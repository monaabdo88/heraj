<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([

            'site_name_ar'          => 'حراج', 
            'site_name_en'          => 'Heraj', 
            'site_email'            => 'heraj@heraj_ksa.com', 
            'site_phone'            => '00968542361', 
            'site_description_ar'   => 'موقع مخصص لعرض وبيع واستبدال جميع انواع السلع', 
            'site_description_en'   => 'A web site dedicated to displaying, selling and exchanging all kinds of goods', 
            'site_tags_ar'          => 'حراج,سيارات,عقارات', 
            'site_tags_en'          => 'heraj,cars,buildings', 
            'site_status'           => 'open', 
            'site_close_msg_ar'     => 'نعتذر الموقع مغلق للصيانه سنعود قريباً شكراً لكم', 
            'site_close_msg_en'     => 'We apologize. Site is closed for maintenance. We will be back soon. Thank you', 
            'site_copyrights_ar'    => 'جميع حقوق الموقع محفوظة لحراج 2020', 
            'site_copyrights_en'    => 'All copyrights are resrved to Heraj 2020', 
            'site_address_ar'       => 'السعودية,الرياض', 
            'site_address_en'       => 'KSA,Riydha', 
            'logo_ar'               => '1607798120gCpRYWn5ZZpLqyEciOFroOaIAZdB5EZBoccUW8LC.png', 
            'logo_en'               => '1607798120ijbvSro1O0WtjRuvZOUBllfJEP0cmC9DA5ueHENa.png', 
            'watermark'             => '1607798120Fn59B2PM0PCMIMSQEuo659ZQiOBgUxs6rJ6gXwfz.png', 
            'mail_driver'           => 'smtp', 
            'mail_host'             => 'smtp.mailtrap.io', 
            'mail_port'             => 2525, 
            'mail_username'         => '0d5eb387f50d89', 
            'mail_password'         => 'd5331673543bfa', 
            'mail_encrypt'          => 'tls', 
            'sms_server'            => 'dddddddddddd', 
            'sms_key'               => 'ddddddddd', 
            'sms_secret'            => 'dddddddddddd'
            
        ]);
    }
}
