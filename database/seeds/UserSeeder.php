<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\Admin::create([
            'name' => 'monaabdo',
            'email' => 'monaabdo88@gmail.com',
            'password' => bcrypt('12122005'),
            'image'     => 'no-user-image.png'
        ]);
        $user->attachRole('super_admin');
    }
}
