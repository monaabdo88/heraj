<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('site_name_ar');
            $table->string('site_name_en');
            $table->string('site_email');
            $table->string('site_phone');
            $table->text('site_description_ar');
            $table->text('site_description_en');
            $table->text('site_tags_ar');
            $table->text('site_tags_en');
            $table->enum('site_status',['open','close'])->default('open');
            $table->text('site_close_msg_ar');
            $table->text('site_close_msg_en');
            $table->text('site_copyrights_ar');
            $table->text('site_copyrights_en');
            $table->string('site_address_ar');
            $table->string('site_address_en');
            $table->string('mail_driver');
            $table->string('mail_host');
            $table->string('mail_port');
            $table->string('mail_username');
            $table->string('mail_password');
            $table->string('mail_encrypt');
            $table->string('sms_server');
            $table->string('sms_key');
            $table->string('sms_secret');
            $table->string('watermark');
            $table->string('logo_ar');
            $table->string('logo_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
