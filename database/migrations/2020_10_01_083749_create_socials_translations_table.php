<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('social_id');
            $table->string('locale')->index();
            $table->string('title');
            $table->unique(['social_id', 'locale']);
            $table->foreign('social_id')->references('id')->on('socials')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials_translations');
    }
}
