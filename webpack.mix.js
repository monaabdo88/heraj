const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'public/dashboard_files/css/bootstrap.min.css',
        'public/dashboard_files/css/ionicons.min.css',
        'public/dashboard_files/css/skin-blue.min.css'
    ],
    'public/css/generalDashboard.css')
    .styles([
        'public/dashboard_files/css/font-awesome-rtl.min.css',
        'public/dashboard_files/css/AdminLTE-rtl.min.css',
        'public/dashboard_files/fonts/rtlfonts.css',
        'public/dashboard_files/css/bootstrap-rtl.min.css',
        'public/dashboard_files/css/rtl.css'
    ],'public/css/rtlDashboard.css')
    .styles([
        'public/dashboard_files/fonts/ltrFonts.css',
        'public/dashboard_files/css/font-awesome.min.css',
        'public/dashboard_files/css/AdminLTE.min.css'
    ],'public/css/ltrDashboard.css')
    .styles([
        'public/css/admin/admin.css',
        'public/dashboard_files/plugins/noty/noty.css',
        'public/dashboard_files/plugins/morris/morris.css',
        'public/dashboard_files/plugins/icheck/all.css'],
    'public/css/admin.css')
    .styles([
            'public/site_files/css/rating_files/bootstrap.css',
            'public/site_files/css/rating_files/rating.css',
            'public/site_files/css/bootstrap-essentials.min.css',
            'public/site_files/css/jquery.selectBoxIt.css',
            'public/site_files/css/font-awesome.min.css',
            'public/site_files/css/elegant-icons.css',
            'public/site_files/css/jquery-ui.min.css',
            'public/site_files/css/owl.carousel.min.css',
            'public/site_files/css/slicknav.min.css',
            'public/site_files/css/dropzone.min.css',
            'public/site_files/sliders/engine1/style.css'
        ]
        ,'public/css/site.css')
    .sass('resources/sass/datatables.scss', 'public/css/admin')
    .scripts([
        'public/dashboard_files/plugins/noty/noty.min.js',
        'public/dashboard_files/js/bootstrap.min.js',
        'public/dashboard_files/plugins/icheck/icheck.min.js',
        'public/dashboard_files/js/fastclick.js',
        'public/dashboard_files/js/adminlte.min.js',
        'public/dashboard_files/js/jquery.number.min.js',   
        'public/dashboard_files/js/printThis.js',
        'public/dashboard_files/js/raphael-min.js',
        'public/dashboard_files/plugins/morris/morris.min.js',
        'public/dashboard_files/chart.js/Chart.js',
        'public/dashboard_files/js/custom/image_preview.js',
        'public/dashboard_files/js/custom/order.js', 
        
    ],
    'public/js/dashboard.js');
