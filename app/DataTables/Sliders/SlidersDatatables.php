<?php

namespace App\DataTables\Sliders;

use App\SlidersDatatable;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\Models\Slider;
use Illuminate\Support\Facades\DB ;
class SlidersDatatables extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('checkbox', 'dashboard.sliders.btn.checkbox')
            ->addColumn('edit', 'dashboard.sliders.btn.edit')
            ->addColumn('delete', 'dashboard.sliders.btn.delete')
            ->addColumn('image', 'dashboard.sliders.btn.image')
            ->addColumn('status','dashboard.sliders.btn.status')
            ->addColumn('archive','dashboard.sliders.btn.archive')
            ->rawColumns([
                'edit',
                'delete',
                'checkbox',
                'image',
                'status',
                'archive'
            ]);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\SlidersDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Slider $model)
    {
       //return $model->newQuery()->with('translations');
       DB::statement(DB::raw('set @rownum=0'));
       $sliders = Slider::select([
           DB::raw('@rownum  := @rownum  + 1 AS rownum'),
           'id',
           'image',
           'status',
           'created_at',
           'updated_at'])->with('translations');
   
       return $this->applyScopes($sliders);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('slidersdatatables-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'dom'        => 'Blfrtip',
                        'lengthMenu' => [[10, 25, 50, 100], [10, 25, 50, trans('site.all_record')]],
                        'buttons'    => [
                           [
                                'text' => '<i class="fa fa-plus"></i> '.trans('site.create'), 'className' => 'btn btn-info', "action" => "function(){
                                    window.location.href = '".\URL::current()."/create';
                                }"],
                            ['extend' => 'print', 'className' => 'btn btn-primary', 'text' => '<i class="fa fa-print"></i>'],
                            ['extend' => 'csv', 'className' => 'btn btn-info', 'text' => '<i class="fa fa-file"></i> '.trans('site.ex_csv')],
                            ['extend' => 'excel', 'className' => 'btn btn-success', 'text' => '<i class="fa fa-file"></i> '.trans('site.ex_excel')],
                            ['extend' => 'pdf', 'className' => 'btn btn-warning', 'text' => '<i class="fa fa-file"></i> '.trans('site.pdf')],
                            ['extend' => 'reload', 'className' => 'btn btn-default', 'text' => '<i class="fa fa-refresh"></i>'],
                            ['text' => '<i class="fa fa-archive"></i>','className'  => 'btn btn-info archive_all'],
                            ['text' => '<i class="fa fa-trash"></i>', 'className' => 'btn btn-danger confirm_all'],
                        ],
                        'initComplete' => " function () {
                            this.api().columns([2,3,4]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
                        'language' => datatable_lang(),
                    ]);
                    
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name'       => 'checkbox',
                'data'       => 'checkbox',
                'title'      => '<input type="checkbox" class="selectAll" />',
                'exportable' => false,
                'printable'  => false,
                'orderable'  => false,
                'searchable' => false,
             ],
             [
                 'name'     => 'rownum',
                 'data'     => 'rownum',
                 'title'    => 'No',
                 'searchable' => false,
             ],
            [
                'name'  => 'image',
                'data'  => 'image',
                'title' => trans('site.image'),
                'orderable'  => false,
            ],
            [
                'name'  => 'translations.title',
                'data'  => 'title',
                'title' => trans('site.title'),
                'orderable'  => false,
            ],
            [
                'name'  =>'status',
                'data'  => 'status',
                'title' => trans('site.status'),
                'orderable'  => false,
            ],
            [
                'name'  => 'created_at',
                'data'  => 'created_at',
                'title' => trans('site.created_at'),
                'orderable'  => false,
             ], 
             [
                'name'  => 'updated_at',
                'data'  => 'updated_at',
                'title' => trans('site.updated_at'),
                'orderable'  => false,
             ],
             [
                'name'       => 'edit',
                'data'       => 'edit',
                'title'      => trans('site.edit'),
                'exportable' => false,
                'printable'  => false,
                'orderable'  => false,
                'searchable' => false,
             ],
             [
                'name'       => 'archive',
                'data'       => 'archive',
                'title'      => trans('site.archive'),
                'exportable' => false,
                'printable'  => false,
                'orderable'  => false,
                'searchable' => false,
             ],
             [
                'name'       => 'delete',
                'data'       => 'delete',
                'title'      => trans('site.delete'),
                'exportable' => false,
                'printable'  => false,
                'orderable'  => false,
                'searchable' => false,
             ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Slider_' . date('YmdHis');
    }
}
