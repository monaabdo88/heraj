<?php

namespace App\DataTables\Cities;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\Models\City;
use Illuminate\Support\Facades\DB ;

class ArchiveDatatables extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('checkbox', 'dashboard.cities.btn.checkbox')
            ->addColumn('restore','dashboard.cities.btn.restore')
            ->rawColumns([
                'checkbox',
                'restore'
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\City $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(City $model)
    {
        $country_id = request()->segment(4);
        DB::statement(DB::raw('set @rownum=0'));
        if($country_id){
            $cities = City::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'country_id',
            'created_at',
            'updated_at'])->where('country_id',$country_id)->onlyTrashed()->with('translations');
        }else{
            $cities = City::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'country_id',
            'created_at',
            'updated_at'])->onlyTrashed()->with('translations');
       }
       return $this->applyScopes($cities);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('archivedatatables_table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'dom'        => 'Blfrtip',
                        'lengthMenu' => [[10, 25, 50, 100], [10, 25, 50, trans('site.all_record')]],
                        'buttons'    => [
        
                                ['extend' => 'print', 'className' => 'btn btn-primary', 'text' => '<i class="fa fa-print"></i>'],
                                ['extend' => 'csv', 'className' => 'btn btn-info', 'text' => '<i class="fa fa-file"></i> '.trans('site.ex_csv')],
                                ['extend' => 'excel', 'className' => 'btn btn-success', 'text' => '<i class="fa fa-file"></i> '.trans('site.ex_excel')],
                                ['extend' => 'pdf', 'className' => 'btn btn-warning', 'text' => '<i class="fa fa-file"></i> '.trans('site.pdf')],
                                ['extend' => 'reload', 'className' => 'btn btn-default', 'text' => '<i class="fa fa-refresh"></i>'],
                                ['text' => '<i class="fa fa-reply-all"></i>','className'  => 'btn btn-info restore_all'],
                            ],
                        'initComplete' => " function () {
                            this.api().columns([2,3,4]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
                        'language' => datatable_lang(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name'       => 'checkbox',
                'data'       => 'checkbox',
                'title'      => '<input type="checkbox" class="selectAll" />',
                'exportable' => false,
                'printable'  => false,
                'orderable'  => false,
                'searchable' => false,
             ],
             [
                'name'     => 'rownum',
                'data'     => 'rownum',
                'title'    => 'No',
                'searchable' => false,
            ],
            [
                'name'  => 'translations.title',
                'data'  => 'title',
                'title' => trans('site.title'),
                'orderable'  => false,
            ],
            [
                'name'  => 'created_at',
                'data'  => 'created_at',
                'title' => trans('site.created_at'),
                'orderable'  => false,
             ], 
             [
                'name'  => 'updated_at',
                'data'  => 'updated_at',
                'title' => trans('site.updated_at'),
                'orderable'  => false,
             ],
            
             [
                'name'       => 'restore',
                'data'       => 'restore',
                'title'      => trans('site.restore'),
                'exportable' => false,
                'printable'  => false,
                'orderable'  => false,
                'searchable' => false,
             ]
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Cities/ArchiveDatatables_' . date('YmdHis');
    }
}
