<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OperationTranslation extends Model
{
    protected $table = 'operations_translations';
    protected $fillable = ['title','locale','operation_id'];
    public $timestamps = false;
}
