<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laratrust\Traits\LaratrustUserTrait;
class Admin extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable,SoftDeletes;
    protected $fillable = [
        'name', 'email', 'password','image'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
}
