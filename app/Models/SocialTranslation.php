<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialTranslation extends Model
{
    use SoftDeletes;
    protected $table = 'socials_translations';
    protected $fillable = ['title'];
    public $timestamps = false;
}
