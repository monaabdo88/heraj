<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subsribers extends Model
{
    protected $table = 'mails';
    protected $fillable = ['email'];
}
