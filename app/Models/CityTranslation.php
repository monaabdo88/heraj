<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CityTranslation extends Model
{
    use SoftDeletes;
    protected $table = 'cities_translations';
    protected $fillable = ['title'];
    public $timestamps = false;
}
