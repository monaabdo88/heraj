<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Setting extends Model
{ 
    protected $table = 'settings';
    protected $fillable = [
        'site_name_ar',
        'site_name_en',
        'site_email',
        'site_phone',
        'site_address_ar',
        'site_address_en',
        'site_status',
        'site_description_ar',
        'site_description_en',
        'site_tags_ar',
        'site_tags_en',
        'logo_ar',
        'logo_en',
        'watermark',
        'site_close_msg_ar',
        'site_close_msg_en',
        'site_copyrights_en',
        'site_copyrights_ar',
        'mail_driver', 
        'mail_host', 
        'mail_port', 
        'mail_username', 
        'mail_password', 
        'mail_encrypt', 
        'sms_server', 
        'sms_key', 
        'sms_secret'
    ];
}
