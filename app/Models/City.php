<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class City extends Model
{
    use Translatable,SoftDeletes; 
    protected $guarded = ['id'];

    public $translatedAttributes = ['title'];
    protected $fillable = ['country_id'];
    public function country(){
        return $this->belongsTo('App\Models\Country','id','country_id');
    }
    public function getCreatedAtAttribute($value){
        $date = Carbon::parse($value);
        return $date->format('Y-m-d H:i');
    }
    public function getUpdatedAtAttribute($value){
        $date = Carbon::parse($value);
        return $date->format('Y-m-d H:i');
    }
    public function products(){
        return $this->hasMany(Product::class);
    }
}
