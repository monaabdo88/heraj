<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CountryTranslation extends Model
{
    use SoftDeletes;
    protected $table = 'countries_translations';
    protected $fillable = ['title','currency'];
    public $timestamps = false;
}
