<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
class Country extends Model
{
    use Translatable,SoftDeletes; 
    protected $guarded = ['id'];

    public $translatedAttributes = ['title','currency'];
    protected $fillable = ['code','flage'];
    public function cities(){
        return $this->hasMany('App\Models\City');
    }
    public function getCreatedAtAttribute($value){
        $date = Carbon::parse($value);
        return $date->format('Y-m-d H:i');
    }
    public function getUpdatedAtAttribute($value){
        $date = Carbon::parse($value);
        return $date->format('Y-m-d H:i');
    }
    public function users(){
        return $this->hasMany(User::class);
    }
    public function products(){
        return $this->hasMany(Product::class);
    }
}
