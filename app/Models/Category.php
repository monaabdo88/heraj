<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use Translatable,SoftDeletes; 
    protected $guarded = ['id'];
    public $translatedAttributes = ['title','description','tags'];
    protected $fillable = ['parent_id','icon','status'];
    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }
    
    public function getParentsNames() {
    
        $parents = collect([]);
    
        if($this->parent) { 
            $parent = $this->parent;
            while(!is_null($parent)) {
                $parents->push($parent);
                $parent = $parent->parent;
            }
            return $parents;
        } else {
            return $this->title;
        }
    
    }
    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }
    public function subproducts()
    {
        return $this->hasManyThrough(Product::class, self::class, 'parent_id', 'category_id');
    }
    public function products(){
        return $this->hasMany(Product::class,'category_id','id');
    }
}
