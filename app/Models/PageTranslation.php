<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageTranslation extends Model
{
    use SoftDeletes;
    protected $table = 'pages_translations';
    protected $fillable = ['title','content'];
    public $timestamps = false;
}
