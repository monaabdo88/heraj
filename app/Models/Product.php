<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Carbon;
use willvincent\Rateable\Rateable;

class Product extends Model implements HasMedia
{
    use SoftDeletes,Translatable,InteractsWithMedia,Rateable;
    protected $guarded = ['id'];

    public $translatedAttributes = ['title','content'];
    protected $fillable = ['status','user_id','category_id','country_id','city_id','special','phone','lat','lng','main_image','price','product_type','views'];
    public function tags(){
        return $this->belongsToMany('App\Models\Tag');
    }
    public function colors(){
        return $this->belongsToMany('App\Models\Color');
    }
    public function sizes(){
        return $this->belongsToMany('App\Models\Size');
    }
    public function categories() {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function getCreatedAtAttribute($value){
        $date = Carbon::parse($value);
        return $date->format('Y-m-d H:i');
    }
    public function getUpdatedAtAttribute($value){
        $date = Carbon::parse($value);
        return $date->format('Y-m-d H:i');
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function city(){
        return $this->belongsTo(City::class);
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

}
