<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryTranslation extends Model
{
    use SoftDeletes;
    protected $table = 'categories_translations';
    protected $fillable = ['title','description','tags'];
    public $timestamps = false;
}
