<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Tag;
use App\Models\Country;
use App\Models\City;
use App\Models\User;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use App\Models\Size;
use App\Models\Color;
use willvincent\Rateable\Rateable;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $cities = City::all();
        $users = User::all();
        $categories = Category::all();
        $tags = Tag::all();
        $colors = Color::all();
        $sizes = Size::all();
        return view('site.add_product',compact('countries','cities','categories','tags','colors','sizes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [
                $locale . '.title'          => 'required|unique:products_translations,title',
                $locale . '.description'    => 'required|min:10'
            ];
        }//end of  for each
        $rules+=[
            'main_image'    => 'image|mimes:jpeg,png,jpg,gif,svg',
            'category_id'   => 'required',
        ];
        $request->validate($rules);
        $request_data = $request->except('_token');
        foreach (config('translatable.locales') as $locale) {
            $request_data[$locale]['slug'] = Str::slug($request_data[$locale]['title'],'-');
        }
        $watermark = public_path('uploads/images/'.settings()->watermark);
        if($request->hasFile('main_image')){
            $main_image = Image::make($request->main_image)
                ->resize(500, 850, function ($constraint) {
                    $constraint->aspectRatio();
                });
            $main_image->insert($watermark,'bottom-left', 5, 5)    
                ->save(public_path('uploads/products/' .$request->main_image->hashName()));
            $request_data['main_image'] = $request->main_image->hashName();
        }
        $product = Product::create($request_data);
        $product->tags()->attach($request->tag_id);
        $product->colors()->attach($request->color_id);
        $product->sizes()->attach($request->size_id);
        foreach ($request->input('document', []) as $file) {
            $product->addMedia(storage_path('tmp/uploads/'.$file))->toMediaCollection('document');
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //$product = Product::where('slug',$id)->first();
        $product = Product::whereHas('translations', function ($query) use($slug) {
            $query
            //->where('locale', app()->getLocale())
            ->where('slug', $slug);
            })->first();
            $media = $product->getMedia('document');
            $views = $product->views+1;
            $related_products = Product::where('id','!=',$product->id)->where('category_id',$product->category_id)->orderBy('id','desc')->limit(10)->get();
            Product::where('id', $product->id)->update(array('views' => $views));
               
            return view('site.product',compact('product','media','related_products'));
    }
    public function rateProduct(Request $request){
        request()->validate(['rate' => 'required']);
        $post = Product::find($request->id);
        $rating = new \willvincent\Rateable\Rating;

        $rating->rating = $request->rate;
        $rating->user_id = $request->user_id;
        $post->ratings()->save($rating);
        toastr()->success(__('site.added_successfully'));

        return redirect()->back();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if($product->user_id != auth()->user()->id)
            return redirect()->back();

        $countries = Country::all();
        $cities = City::all();
        $users = User::all();
        $categories = Category::all();
        $tags = Tag::all(); 
        $colors = Color::all();
        $sizes = Size::all();
        $media = $product->getMedia('document');   
        return view('site.editProduct',compact('product','countries','cities','categories','tags','media','sizes','colors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request_data = $request->except(['_token','_method']);
        $watermark = public_path('uploads/images/'.settings()->watermark);
        
        if($request->main_image){
            Storage::disk('public_uploads')->delete('/products/' . $product->main_image);
            $main_image = Image::make($request->main_image)
                ->resize(500, 850, function ($constraint) {
                    $constraint->aspectRatio();
                });
            $main_image->insert($watermark,'bottom-left', 5, 5)    
                ->save(public_path('uploads/products/' .$request->main_image->hashName()));
            $request_data['main_image'] = $request->main_image->hashName();
        }
        if (count($product->getMedia('document')) > 0) {
            foreach ($product->getMedia('document') as $media) {
                if (!in_array($media->file_name, $request->input('document', []))) {
                    $media->delete();
                }
            }
        }
    
        $media = $product->getMedia('document')->pluck('file_name')->toArray();
    
        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $product->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('document');
            }
        }
    
        $product->update($request_data);
        if (! $product->tags->contains($request->tag_id)) {
            $product->tags()->sync($request->tag_id);
         }
         if (! $product->colors->contains($request->color_id)) {
             $product->colors()->sync($request->color_id);
         }
         if (! $product->sizes->contains($request->size_id)) {
             $product->sizes()->sync($request->size_id);
         }
        toastr()->success(__('site.updated_successfully'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $del = $product->delete();     
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    public function storeMedia(Request $request)
    {
       $path = storage_path('tmp/uploads');
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        } 
       $file = $request->file('file');
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $watermark = public_path('uploads/images/'.settings()->watermark);
        Image::make($file)
                ->resize(500, 850, function ($constraint) {
                    $constraint->aspectRatio();
                })->insert($watermark,'bottom-left', 5, 5)    
                ->save($path.'/'.$name);
        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
    public function del_image($name){
        $path = 'tmp/uploads';
        $filename = $path.'/'.$name;
        if($filename)
            unlink(storage_path($filename));
    }

}
