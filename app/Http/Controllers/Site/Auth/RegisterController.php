<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function newUser(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users', 
            'password' => 'required|confirmed|min:8',
            'phone'=>'required|min:11|unique:users'
        ]);
        $request_data = $request->except('_token');
        $request_data['password'] = bcrypt($request->password);
        $request_data['status'] = '0';
        $request_data['phone'] = $request->country_code.$request->phone;
        if($request->hasFile('user_img')){
            Image::make($request->user_img)
                ->resize(150, 150)
                ->save(public_path('uploads/users/' .$request->user_img->hashName()));
            $request_data['user_img'] = $request->user_img->hashName();
        }
        $user_add = User::create($request_data);
        if(! $user_add){
            toastr()->error(__('site.wrong_password'));
            return redirect()->back();
        }else{
            $user_add->sendEmailVerificationNotification();
            Auth::login($user_add);
            toastr()->success(__('site.success_signup'));
            return redirect()->back();
        }
    }
}
