<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);
        if (auth()->validate(['email' => $request->email, 'password' => $request->password, 'status' => 0])) {
            toastr()->error(__('site.not_verified_account'));
            return redirect()->back();
        }
        $credentials  = array('email' => $request->email, 'password' => $request->password);
        if (auth()->attempt($credentials, $request->has('remember'))){
                toastr()->success(__('site.welcome_back').auth()->user()->name);
                return redirect()->back();
        }
        toastr()->error(__('site.wrong_password'));
        return redirect()->back();
    }
    public function userLogout(Request $request){
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        return redirect()->back();
    }
}
