<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Country;
use App\Models\Category;
use App\Models\Slider;
use App\Models\Product;
use Mail;
use App\Models\Page;
use App\Models\Subsribers;

class HomeController extends Controller
{
    // Main Page For the site
    public function index($type = null){
        $main_cats = Category::orderBy('id','desc')->where('parent_id',NULL)->limit(3)->get();
        $all_cats = Category::orderBy('id','desc')->limit(8)->get();
        $sliders = Slider::orderBy('id','desc')->where('status','1')->get();
        $products_type = Product::where(['status'=>'1','special'=> '1'])->orderBy('id','desc')->limit(8)->get();
        $last_products = Product::orderBy('id','desc')->where('status','1')->limit(6)->get();
        $viewed_products = Product::orderBy('views','desc')->where('status','1')->limit(6)->get();
        $commented_products = Product::withCount('comments')
        ->where('status','1')->orderBy('comments_count', 'desc')->limit(6)
        ->get();
        return view('site.index',compact(
            [
                'main_cats',
                'sliders',
                'all_cats',
                'products_type',
                'last_products',
                'viewed_products',
                'commented_products'
            ]));
    }
    // Get All cities for one country
    public function get_cities($country_id,$lang){
        $cities = City::where('country_id',$country_id)->get();
        foreach($cities as $city){
            echo '<option value="'.$city->id.'">'.$city->translate($lang)->title.'</option>';
        }
    }
    //Get All Products
    public function all_products(){
        $products = Product::where('status','1')->orderBy('id','desc')->paginate(10);
        return view('site.all_products',compact('products'));
    }
    //Contact Us Page
    public function contact(){
        return view('site.contact');
    }
    //Send New Message via contact page
    public function contactMsg(Request $request){
        $this->validate($request,[
            'name'      => 'required|string',
            'email'     => 'required|email',
            'subject'   => 'required',
            'message'   => 'required'
        ]);
        $data = array(
          'name' => $request->name,
          'email'=>$request->email,
          'msg'=>$request->message,
          'subject'=>$request->subject
        );
        \Mail::send('contact_template',$data, function($message) use ($request)
               {
                  $message->from($request->email,$request->name);
                   $message->subject($request->subject);
                  $message->to(settings()->site_email);
               });
        if (Mail::failures()) {
           return new Error(Mail::failures()); 
        }
        toastr()->success(__('site.msg_send'));
        return redirect()->back();   
    }
    //Function show page details
    public function get_page($id){
        $page = Page::findOrFail($id);
        return view('site.page',compact('page'));
    }
    //All Categories
    public function all_cats(){
        $cats = Category::orderBy('id','desc')->where('parent_id',NULL)->get();
        return view('site.all_cats',compact('cats'));
    }
    public function subscribe(Request $request){
        $this->validate($request,[
            'email'    => 'required|email'
            ]);
        $check_email = Subsribers::where('email',$request->email)->get();
        if(count($check_email)>0){
            toastr()->error('This Email is already subscriber');
            return redirect()->back();
        }else{
            Subsribers::create(['email' => $request->email]);
            toastr()->success('Subscribe Had Added Successfully');
            return redirect()->back();
        }
    }
    // upload images via ckeditor
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('uploads/products'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('uploads/products/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            return $response;
        
        }
    }
}
