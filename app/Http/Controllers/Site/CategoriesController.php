<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class CategoriesController extends Controller
{
    public function index(){
        $cats = Category::where(['status'=>'1','parent_id'=> NULL])->orderBy('id','desc')->paginate(10);
        return view('site.categories',compact('cats'));
    }
    public function show($id){
        $category = Category::findOrFail($id);
        $cat = Category::with(['products', 'subproducts'])->where('id', $id)->first();
        $childs = $cat->products->merge($cat->subproducts);
        return view('site.category',compact('childs','category'));
    }
}
