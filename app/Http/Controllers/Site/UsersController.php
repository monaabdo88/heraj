<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\City;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
class UsersController extends Controller
{
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('site.profile',compact('user'));
    }
    public function edit($id)
    {
        $user = User::findOrFail($id);
        if(auth()->user()->id != $id){
            toastr()->error('you are not allowed to open this page');
            return redirect()->back();
        }
        $countries = Country::all();
        $cities = City::all();
        return view('site.edit_profile',compact('user','countries','cities'));
    }
    public function update(Request $request, User $user)
    {
        $this->validate($request,[
            'name'          => 'required|string|max:255',
            'email'         => 'required|email|unique:users,email,'.$user->id,
            'password'      => 'sometimes|nullable|min:8',
            'phone'         => 'required|min:11|unique:users,phone,'.$user->id,
            'city_id'       => 'required'
        ]);
        $request_data = $request->except(['_token','_method']);
        
        if($request->user_img){
            if($user->user_img != 'no-user-image.png')
                Storage::disk('public_uploads')->delete('/users/' . $user->user_img);
            Image::make($request->user_img)
            ->resize(150, 150, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save(public_path('uploads/users/' .$request->user_img->hashName()));
            $request_data['user_img'] = $request->user_img->hashName();
        }
        if($request->password){
            $request_data['password'] = bcrypt($request->password);
        }else{
            $request_data['password'] = $user->password;
        }
        $user->update($request_data);
        toastr()->success(__('site.updated_successfully'));
        return redirect()->back();
    }
    public function userProducts($id){
        //$products = DB::table('products')->where('user_id',$id)->orderBy('id','desc')->paginate(10);
        $products = Product::where('user_id',$id)->orderBy('id','desc')->paginate(10);
        $user = User::findOrFail($id);
        return view('site.userProducts',compact('products','user'));

    }
    public function myFavs($id){

    }
    public function myMsgs($id){

    }
}
