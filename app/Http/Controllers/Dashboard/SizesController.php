<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Size;
use App\Models\Operation;
use App\DataTables\Sizes\SizesDatatables;
use App\DataTables\Sizes\ArchiveDatatables;
use App\Models\OperationTranslation;

class SizesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_sizes'])->only('index');      
        $this->middleware(['permission:create_sizes'])->only(['create','store']);      
        $this->middleware(['permission:update_sizes'])->only(['edit','update']);      
        $this->middleware(['permission:delete_sizes'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_sizes'])->only(['archiveAll','sizes_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SizesDatatables $sizes)
    {
        return $sizes->render('dashboard.sizes.index');
    }
    /* Get All Trashed sizes */
    public function sizes_archive(ArchiveDatatables $sizes){
        return $sizes->render('dashboard.sizes.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.sizes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['title'=> 'required|unique:sizes']);
        $request_data = $request->except('_token');

        $size = Size::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $size->id,
            'section'   => 'sizes',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] =$size->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Size $size)
    {
        return view('dashboard.sizes.edit',compact('size'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Size $size)
    {
        $this->validate($request,['title'   => 'required|unique:sizes,title,'.$size->id]);
        $request_data = $request->except(['_token','_method']);
        $size->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $size->id,
            'section'   => 'sizes',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = $size->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/sizes'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $size = Size::findOrFail($id);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'sizes',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = $size->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $size->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    public function archiveAll($ids){
        $sizes_id = explode(',',$ids);
        foreach($sizes_id as $id){
            $size = Size::findOrFail($id);
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'sizes',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $size->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $size->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted Colors */
    public function restoreAll($ids)
    {
        $sizes_id = explode(',',$ids);
        foreach($sizes_id as $id){
            
            $restore = Size::where('id',$id)->restore();
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'sizes',
                'admin_id'  => auth()->user()->id
            ]);
            $size = Size::findOrFail($id);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] =$size->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    public function delAll($ids){
        $sizes_id = explode(',',$ids);
        foreach($sizes_id as $id){
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'sizes',
                'admin_id'  => auth()->user()->id
            ]);
            $size = Size::findOrFail($id);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $size->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $size->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
}
