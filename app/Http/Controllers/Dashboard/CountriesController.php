<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\DataTables\Countries\CountriesDatatables;
use App\DataTables\Countries\ArchiveDatatables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\Models\Operation;
use App\Models\OperationTranslation;

class CountriesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_countries'])->only('index');      
        $this->middleware(['permission:create_countries'])->only(['create','store']);      
        $this->middleware(['permission:update_countries'])->only(['edit','update']);      
        $this->middleware(['permission:delete_countries'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_countries'])->only(['archiveAll','countries_archive','restoreAll']);    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CountriesDatatables $countries)
    {
        return $countries->render('dashboard.countries.index');
    }
    /* Get All Trashed Countries */
    public function countries_archive(ArchiveDatatables $countries){
        return $countries->render('dashboard.countries.index');
    }   
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.title' => 'required|unique:countries_translations,title'];
        }//end of  for each
        $rules+=[
            'flage'    => 'image|mimes:jpeg,png,jpg,gif,svg',
            'code'    => 'required'
        ];
        $request->validate($rules);
        $request_data = $request->except('_token');
        
        if($request->hasFile('flage')){
            Image::make($request->flage)
                ->resize(150, 75, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/flags/' .$request->flage->hashName()));
            $request_data['flage'] = $request->flage->hashName();
        }
        $country = Country::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $country->id,
            'section'   => 'countries',
            'admin_id'  => auth()->user()->id,
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CountryTranslation::where(['country_id'=>$country->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        return view('dashboard.countries.edit',compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        $request_data = $request->except(['_token','_method']);
        if($request->flage){
            Storage::disk('public_uploads')->delete('/flags/' . $country->flage);
            Image::make($request->flage)
            ->resize(150, 75, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save(public_path('uploads/flags/' .$request->flage->hashName()));
            $request_data['flage'] = $request->flage->hashName();
        }
        $country->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $country->id,
            'section'   => 'countries',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CountryTranslation::where(['country_id'=>$country->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect()->back(); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        Storage::disk('public_uploads')->delete('/flags/' . $country->flage);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'countries',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CountryTranslation::where(['country_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $country->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    /* Archive All Countries */
    public function archiveAll($ids){
        $countries_id = explode(',',$ids);
        foreach($countries_id as $id){
            $country = Country::findOrFail($id);
            $operation=Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'countries',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\CountryTranslation::where(['country_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $country->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted countries */
    public function restoreAll($ids)
    {
        $countries_id = explode(',',$ids);
        foreach($countries_id as $id){
            $restore = Country::where('id',$id)->restore();
            $operation =Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'countries',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\CountryTranslation::where(['country_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    /* Delete All Selected records */
    public function delAll($ids){
        $countries_id = explode(',',$ids);
        foreach($countries_id as $id){
            $country = Country::findOrFail($id);
            $operation =Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'countries',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\CountryTranslation::where(['country_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            Storage::disk('public_uploads')->delete('/flags/' . $country->flage);
            $del = $country->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
}
