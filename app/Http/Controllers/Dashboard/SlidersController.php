<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Operation;
use App\DataTables\Sliders\SlidersDatatables;
use App\DataTables\sliders\ArchiveDatatables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\Models\OperationTranslation;
class SlidersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_sliders'])->only('index');      
        $this->middleware(['permission:create_sliders'])->only(['create','store']);      
        $this->middleware(['permission:update_sliders'])->only(['edit','update']);      
        $this->middleware(['permission:delete_sliders'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_sliders'])->only(['archiveAll','sliders_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SlidersDatatables $sliders)
    {
        return $sliders->render('dashboard.sliders.index');

    }
    /* Get All Trashed sliders */
    public function sliders_archive(ArchiveDatatables $sliders){
        return $sliders->render('dashboard.sliders.index');
    }   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.title' => 'required|unique:sliders_translations,title'];
        }//end of  for each
        $rules+=[
            'image'    => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ];
        $request->validate($rules);
        $request_data = $request->except('_token');
        
        if($request->hasFile('image')){
            Image::make($request->image)
            ->resize(847.5, 545)->save(public_path('uploads/sliders/' .$request->image->hashName()));
            $request_data['image'] = $request->image->hashName();
        }
        $slider = Slider::create($request_data);
        $operation=Operation::create([
            'type'      => 'add',
            'row_id'    => $slider->id,
            'section'   => 'sliders',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\SliderTranslation::where(['slider_id'=>$slider->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('dashboard.sliders.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $request_data = $request->except(['_token','_method']);
        if($request->image){
            Storage::disk('public_uploads')->delete('/sliders/' . $slider->image);
            Image::make($request->image)
            ->resize(847.5, 545)->save(public_path('uploads/sliders/' .$request->image->hashName()));
            $request_data['image'] = $request->image->hashName();
        }
        $slider->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $slider->id,
            'section'   => 'sliders',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\SliderTranslation::where(['slider_id'=>$slider->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        Storage::disk('public_uploads')->delete('/sliders/' . $slider->image);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'sliders',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\SliderTranslation::where(['slider_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $slider->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    //Archive All Selected Records
    public function archiveAll($ids){
        $sliders_id = explode(',',$ids);
        foreach($sliders_id as $id){
            $slider = Slider::findOrFail($id);
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'sliders',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\SliderTranslation::where(['slider_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
             $del = $slider->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted countries */
    public function restoreAll($ids)
    {
        $sliders_id = explode(',',$ids);
        foreach($sliders_id as $id){
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'sliders',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\SliderTranslation::where(['slider_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = Slider::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    //Delete All Selected Records
    public function delAll($ids){
        $sliders_id = explode(',',$ids);
        foreach($sliders_id as $id){
            $slider = Slider::findOrFail($id);
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'sliders',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\SliderTranslation::where(['slider_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            Storage::disk('public_uploads')->delete('/sliders/' . $slider->image);
            $del = $slider->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    public function changeStatus(Request $request)
    {
        $operation = Operation::create([
            'type'      => 'changeStatus',
            'row_id'    => $request->id,
            'section'   => 'sliders',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\SliderTranslation::where(['slider_id'=>$request->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        Slider::where('id',$request->id)->update(['status'=> $request->status]);
        return response()->json(['success'=> __('site.update_status_successfully')]);

    }
}
