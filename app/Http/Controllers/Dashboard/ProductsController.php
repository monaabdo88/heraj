<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataTables\Products\ProductsDatatables;
use App\DataTables\Products\ArchiveDatatables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\Models\Product;
use App\Models\Category;
use App\Models\User;
use App\Models\Country;
use App\Models\City;
use App\Models\Tag;
use App\Models\Color;
use App\Models\Size;
use App\Models\Operation;
use Illuminate\Support\Str;
use App\Models\OperationTranslation;
class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_products'])->only('index');      
        $this->middleware(['permission:create_products'])->only(['create','store']);      
        $this->middleware(['permission:update_products'])->only(['edit','update']);      
        $this->middleware(['permission:delete_products'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_products'])->only(['archiveAll','products_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductsDatatables $products)
    {
        return $products->render('dashboard.products.index');
    }
    public function getUserProducts(User $user,ProductsDatatables $products){
        return $products->render('dashboard.products.index');
    }
    /* Get All Trashed products */
    public function products_archive(ArchiveDatatables $products){
        return $products->render('dashboard.products.index');
    }   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $cities = City::all();
        $users = User::all();
        $categories = Category::all();
        $tags = Tag::all();
        $colors = Color::all();
        $sizes = Size::all();
        return view('dashboard.products.create',
        compact('countries','cities','users','categories','tags','colors','sizes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [
                $locale . '.title'          => 'required|unique:products_translations,title',
                $locale . '.description'    => 'required|min:10'
            ];
        }//end of  for each
        $rules+=[
            'main_image'    => 'image|mimes:jpeg,png,jpg,gif,svg',
            'category_id'   => 'required',
        ];
        $request->validate($rules);
        $request_data = $request->except('_token');
        foreach (config('translatable.locales') as $locale) {
            $request_data[$locale]['slug'] = Str::slug($request_data[$locale]['title'],'-');
        }
        $watermark = public_path('uploads/images/'.settings()->watermark);
            
        if($request->hasFile('main_image')){
            $main_image = Image::make($request->main_image)
                ->resize(500, 850, function ($constraint) {
                    $constraint->aspectRatio();
                });
            $main_image->insert($watermark,'bottom-left', 5, 5)    
                ->save(public_path('uploads/products/' .$request->main_image->hashName()));
            $request_data['main_image'] = $request->main_image->hashName();
        }
        $product = Product::create($request_data);
        $operation=Operation::create([
            'type'      => 'add',
            'row_id'    => $product->id,
            'section'   => 'products',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\ProductTranslation::where(['product_id'=>$product->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $product->tags()->attach($request->tag_id);
        $product->colors()->attach($request->color_id);
        $product->sizes()->attach($request->size_id);

        foreach ($request->input('document', []) as $file) {
            $product->addMedia(storage_path('tmp/uploads/'.$file))->toMediaCollection('document');
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $countries = Country::all();
        $cities = City::all();
        $users = User::all();
        $categories = Category::all();
        $tags = Tag::all();
        $colors = Color::all();
        $sizes = Size::all();
        $media = $product->getMedia('document');
        return view('dashboard.products.edit',
        compact('product','countries','cities','users','categories','tags','media','colors','sizes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request_data = $request->except(['_token','_method']);
        $watermark = public_path('uploads/images/'.settings()->watermark);
        
        if($request->main_image){
            Storage::disk('public_uploads')->delete('/products/' . $product->main_image);
            $main_image = Image::make($request->main_image)
                ->resize(500, 850, function ($constraint) {
                    $constraint->aspectRatio();
                });
            $main_image->insert($watermark,'bottom-left', 5, 5)    
                ->save(public_path('uploads/products/' .$request->main_image->hashName()));
            $request_data['main_image'] = $request->main_image->hashName();
        }
        if (count($product->getMedia('document')) > 0) {
            foreach ($product->getMedia('document') as $media) {
                if (!in_array($media->file_name, $request->input('document', []))) {
                    $media->delete();
                }
            }
        }
    
        $media = $product->getMedia('document')->pluck('file_name')->toArray();
    
        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $product->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('document');
            }
        }
    
        $product->update($request_data);
        if (! $product->tags->contains($request->tag_id)) {
           $product->tags()->sync($request->tag_id);
        }
        if (! $product->colors->contains($request->color_id)) {
            $product->colors()->sync($request->color_id);
        }
        if (! $product->sizes->contains($request->size_id)) {
            $product->sizes()->sync($request->size_id);
        }
        $operation =Operation::create([
            'type'      => 'update',
            'row_id'    => $product->id,
            'section'   => 'products',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\ProductTranslation::where(['product_id'=>$product->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/products');
        
    }

    public function changeStatus(Request $request)
    {
        $operation =Operation::create([
            'type'      => 'changeStatus',
            'row_id'    => $request->id,
            'section'   => 'products',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\ProductTranslation::where(['product_id'=>$request->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        Product::where('id',$request->id)->update(['status'=> $request->status]);
        return response()->json(['success'=> __('site.update_status_successfully')]);

    }
    public function changeType(Request $request)
    {
        $operation = Operation::create([
            'type'      => 'changeType',
            'row_id'    => $request->id,
            'section'   => 'products',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\ProductTranslation::where(['product_id'=>$request->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        Product::where('id',$request->id)->update(['special'=> $request->type]);
        return response()->json(['success'=> __('site.update_status_successfully')]);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'products',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\ProductTranslation::where(['product_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        Storage::disk('public_uploads')->delete('/products/' . $product->main_image);
        $product->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    public function archiveAll($ids){
        $products_id = explode(',',$ids);
        foreach($products_id as $id){
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'products',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\ProductTranslation::where(['product_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $product = Product::findOrFail($id);
            $del = $product->delete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted products */
    public function restoreAll($ids)
    {
        $products_id = explode(',',$ids);
        foreach($products_id as $id){
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'products',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\ProductTranslation::where(['product_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = Product::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    public function delAll($ids){
        $products_id = explode(',',$ids);
        foreach($products_id as $id){
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'products',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\ProductTranslation::where(['product_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $product = Product::findOrFail($id);
            Storage::disk('public_uploads')->delete('/products/' . $product->main_image);
            
            $del = $product->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    public function storeMedia(Request $request)
    {
       $path = storage_path('tmp/uploads');
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        } 
       $file = $request->file('file');
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $watermark = public_path('uploads/images/'.settings()->watermark);
        Image::make($file)
                ->resize(500, 850, function ($constraint) {
                    $constraint->aspectRatio();
                })->insert($watermark,'bottom-left', 5, 5)    
                ->save($path.'/'.$name);
        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }
    public function del_image($name){
        $path = 'tmp/uploads';
        $filename = $path.'/'.$name;
        if($filename)
            unlink(storage_path($filename));
    }
}
