<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataTables\Cities\CitiesDatatables;
use App\Models\City;
use App\Models\Country;
use App\Models\Operation;
use App\Models\OperationTranslation;
use App\DataTables\Cities\ArchiveDatatables;
class CitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_cities'])->only('show');
        $this->middleware(['permission:read_cities'])->only('index');      
        $this->middleware(['permission:create_cities'])->only(['create','store']);      
        $this->middleware(['permission:update_cities'])->only(['edit','update']);      
        $this->middleware(['permission:delete_cities'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_cities'])->only(['archiveAll','cities_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CitiesDatatables $cities)
    {
        return $cities->render('dashboard.cities.index');
    }
    /* Get All Trashed Cities */
    public function cities_archive(ArchiveDatatables $cities){
        return $cities->render('dashboard.cities.index');
    }   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('dashboard.cities.create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.title' => 'required|unique:cities_translations,title'];
        }//end of  for each
        $request->validate($rules);
        $request_data = $request->except('_token');
        $city = City::create($request_data);
        $operation=Operation::create([
            'type'      => 'add',
            'row_id'    => $city->id,
            'section'   => 'cities',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CityTranslation::where(['city_id'=>$city->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country,CitiesDatatables $cities)
    {
        return $cities->render('dashboard.cities.index');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $countries = Country::all();
        
        return view('dashboard.cities.edit',compact('city','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $request_data = $request->except(['_token','_method']);
        $city->update($request_data);
        $operation=Operation::create([
            'type'      => 'update',
            'row_id'    => $city->id,
            'section'   => 'cities',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CityTranslation::where(['city_id'=>$city->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect()->back(); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $operation=Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'cities',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CityTranslation::where(['city_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $city->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    public function archiveAll($ids){
        $cities_id = explode(',',$ids);
        foreach($cities_id as $id){
            $city = City::findOrFail($id);
            $operation=Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'cities',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\CityTranslation::where(['city_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $city->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted cities */
    public function restoreAll($ids)
    {
        $cities_id = explode(',',$ids);
        foreach($cities_id as $id){
            $operation=Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'cities',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\CityTranslation::where(['city_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = City::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    /* Delete All Selected records */
    public function delAll($ids){
        $cities_id = explode(',',$ids);
        foreach($cities_id as $id){
            $city = City::findOrFail($id);
            $operation=Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'cities',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\CityTranslation::where(['city_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $city->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
}
