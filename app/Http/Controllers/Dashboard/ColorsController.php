<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Color;
use App\Models\Operation;
use App\DataTables\Colors\ColorsDatatables;
use App\DataTables\Colors\ArchiveDatatables;
use App\Models\OperationTranslation;

class ColorsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_colors'])->only('index');      
        $this->middleware(['permission:create_colors'])->only(['create','store']);      
        $this->middleware(['permission:update_colors'])->only(['edit','update']);      
        $this->middleware(['permission:delete_colors'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_colors'])->only(['archiveAll','colors_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ColorsDatatables $colors)
    {
        return $colors->render('dashboard.colors.index');
    }
    /* Get All Trashed Colors */
    public function colors_archive(ArchiveDatatables $colors){
        return $colors->render('dashboard.colors.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.colors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['title'=> 'required|unique:colors']);
        $request_data = $request->except('_token');

        $color = Color::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $color->id,
            'section'   => 'colors',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] =$color->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {
        return view('dashboard.colors.edit',compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        $this->validate($request,['title'   => 'required|unique:colors,title,'.$color->id]);
        $request_data = $request->except(['_token','_method']);
        $color->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $color->id,
            'section'   => 'colors',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = $color->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/colors'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $color = Color::findOrFail($id);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'Colors',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = $color->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $color->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    public function archiveAll($ids){
        $Colors_id = explode(',',$ids);
        foreach($Colors_id as $id){
            $color = Color::findOrFail($id);
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'colors',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $color->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $color->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted colors */
    public function restoreAll($ids)
    {
        $tags_id = explode(',',$ids);
        foreach($tags_id as $id){
            
            $restore = Color::where('id',$id)->restore();
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'colors',
                'admin_id'  => auth()->user()->id
            ]);
            $color = Color::findOrFail($id);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $color->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    public function delAll($ids){
        $Colors_id = explode(',',$ids);
        foreach($Colors_id as $id){
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'colors',
                'admin_id'  => auth()->user()->id
            ]);
            $color = Color::findOrFail($id);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $color->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $color->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
}
