<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Operation;
use App\DataTables\Pages\PagesDatatables;
use App\DataTables\Pages\ArchiveDatatables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\Models\OperationTranslation;
class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_pages'])->only('index');      
        $this->middleware(['permission:create_pages'])->only(['create','store']);      
        $this->middleware(['permission:update_pages'])->only(['edit','update']);      
        $this->middleware(['permission:delete_pages'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_pages'])->only(['archiveAll','pages_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PagesDatatables $pages)
    {
        return $pages->render('dashboard.pages.index');
    }
    /* Get All Trashed Pages */
    public function pages_archive(ArchiveDatatables $pages){
        return $pages->render('dashboard.pages.index');
    }   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [
                        $locale . '.title'      => 'required|unique:pages_translations,title',
                        $locale . '.content' => 'required'
                    ];
        }//end of  for each
        $request->validate($rules);
        $request_data = $request->except('_token');
        $page = Page::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $page->id,
            'section'   => 'pages',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\PageTranslation::where(['page_id'=>$page->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('dashboard.pages.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $request_data = $request->except(['_token','_method']);
        $page->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $page->id,
            'section'   => 'pages',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\PageTranslation::where(['page_id'=>$page->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/pages'); 
    }
    public function changeStatus(Request $request)
    {
        $operation = Operation::create([
            'type'      => 'changeStatus',
            'row_id'    => $request->id,
            'section'   => 'pages',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\PageTranslation::where(['page_id'=>$request->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        Page::where('id',$request->id)->update(['status'=> $request->status]);
        return response()->json(['success'=> __('site.update_status_successfully')]);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'pages',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\PageTranslation::where(['page_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $page->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    public function archiveAll($ids){
        $pages_id = explode(',',$ids);
        foreach($pages_id as $id){
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'pages',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\PageTranslation::where(['page_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $page = Page::findOrFail($id);
            $del = $page->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted cities */
    public function restoreAll($ids)
    {
        $pages_id = explode(',',$ids);
        foreach($pages_id as $id){
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'pages',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\PageTranslation::where(['page_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = Page::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    public function delAll($ids){
        $pages_id = explode(',',$ids);
        foreach($pages_id as $id){
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'pages',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\PageTranslation::where(['page_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $page = Page::findOrFail($id);
            $del = $page->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
}
