<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataTables\Admins\AdminsDatatables;
use App\DataTables\Admins\ArchiveDatatables;
use App\Models\Admin;
use App\Models\Operation;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Models\OperationTranslation;
class AdminsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_admins'])->only('index');      
        $this->middleware(['permission:create_admins'])->only(['create','store']);      
        $this->middleware(['permission:update_admins'])->only(['edit','update']);      
        $this->middleware(['permission:delete_admins'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_admins'])->only(['archiveAll','admins_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdminsDatatables $admins)
    {
        return $admins->render('dashboard.admins.index');
    }
    /* Get All Trashed products */
    public function admins_archive(ArchiveDatatables $admins){
        return $admins->render('dashboard.admins.index');
    } 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:admins',
            'image' => 'image',
            'password' => 'required|confirmed',
            'permissions' => 'required|min:1'
        ]);
        $request_data = $request->except(['password', 'password_confirmation', 'permissions', 'image']);
        $request_data['password'] = bcrypt($request->password);
        if ($request->image) {

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/admins/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

        }//end of if
        $admin = Admin::create($request_data);
        $admin->attachRole('admin');
        $admin->syncPermissions($request->permissions);
        $operation =Operation::create([
            'type'      => 'add',
            'row_id'    => $admin->id,
            'section'   => 'admins',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] =  $admin->name;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        $user_id = Auth::user()->id;
        if($admin->id == 1 && $user_id != '1'){
            toastr()->error(__('site.noPermissions'));
            return redirect('/cp');
        }else{
            return view('dashboard.admins.edit',compact('admin','user_id'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $request->validate([
            'name' => 'required',
            'email' => ['required', Rule::unique('admins')->ignore($admin->id),],
            'image' => 'image',
            'permissions' => 'required|min:1'
        ]);

        $request_data = $request->except(['permissions', 'image']);
        if($request->password){
            $request_data['password'] = bcrypt($request->password);
        }else{
            $request_data['password'] = $admin->password;
        }
        if ($request->image) {

            if ($admin->image != 'no-user-image.png') {

                Storage::disk('public_uploads')->delete('/admins/' . $admin->image);

            }//end of inner if

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/admins/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

        }//end of external if

        $admin->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $admin->id,
            'section'   => 'admins',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] =  $admin->name;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $admin->syncPermissions($request->permissions);
        toastr()->success( __('site.updated_successfully'));
        return redirect('cp/admins');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Admin::findOrFail($id);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'admins',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] =  $user->name;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        if($id != 1){
            if($user->image != 'no-user-image.png')
                Storage::disk('public_uploads')->delete('/admins/' . $user->image);
            $user->forceDelete();
        }
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    public function archiveAll($ids){
        $admins_id = explode(',',$ids);
        foreach($admins_id as $id){
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'admins',
                'admin_id'  => auth()->user()->id
            ]);
            $admin = Admin::findOrFail($id);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] =  $admin->name;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            if($id != 1)
                $del = $admin->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted products */
    public function restoreAll($ids)
    {
        $admins_id = explode(',',$ids);
        foreach($admins_id as $id){
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'admins',
                'admin_id'  => auth()->user()->id
            ]);
            $admin = Admin::findOrFail($id);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] =  $admin->name;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = Admin::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    //Delete All selected User
    public function delAll($ids){
        $admins_id = explode(',',$ids);
        foreach($admins_id as $id){
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'admins',
                'admin_id'  => auth()->user()->id
            ]);
            $admin = Admin::findOrFail($id);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] =  $admin->name;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            if($id !=1){
                if($admin->image != 'no-user-image.png')
                    Storage::disk('public_uploads')->delete('/admins/' . $admin->image);
                $del = $admin->forceDelete();
            }
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
}
