<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\DataTables\Users\UsersDatatables;
use App\Models\Country;
use App\Models\City;
use App\Models\Operation;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\DataTables\Users\ArchiveDatatables;
use App\Models\OperationTranslation;
class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_users'])->only('index');      
        $this->middleware(['permission:create_users'])->only(['create','store']);      
        $this->middleware(['permission:update_users'])->only(['edit','update']);      
        $this->middleware(['permission:delete_users'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_users'])->only(['archiveAll','users_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDatatables $users)
    {
        return $users->render('dashboard.users.index');
    }
     /* Get All Trashed Users */
    public function users_archive(ArchiveDatatables $users){
        return $users->render('dashboard.users.index');
    } 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('dashboard.users.create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'          => 'required|string|max:255',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|confirmed|min:8',
            'phone'         => 'required|min:11|unique:users',
            'city_id'       => 'required'
        ]);
        $request_data = $request->except('_token');
        $request_data['password'] = bcrypt($request->password);
        if($request->hasFile('user_img')){
            Image::make($request->user_img)
                ->resize(150, 150)
                ->save(public_path('uploads/users/' .$request->user_img->hashName()));
            $request_data['user_img'] = $request->user_img->hashName();
        }
        $user = User::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $user->id,
            'section'   => 'users',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = $user->name;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect('cp/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $countries = Country::all();
        $cities = City::all();
        return view('dashboard.users.edit',compact('user','countries','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request,[
            'name'          => 'required|string|max:255',
            'email'         => 'required|email|unique:users,email,'.$user->id,
            'password'      => 'sometimes|nullable|min:8',
            'phone'         => 'required|min:11|unique:users,phone,'.$user->id,
            'city_id'       => 'required'
        ]);
        $request_data = $request->except(['_token','_method']);
        
        if($request->user_img){
            if($user->user_img != 'no-user-image.png')
                Storage::disk('public_uploads')->delete('/users/' . $user->user_img);
            Image::make($request->user_img)
            ->resize(150, 150, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save(public_path('uploads/users/' .$request->user_img->hashName()));
            $request_data['user_img'] = $request->user_img->hashName();
        }
        if($request->password){
            $request_data['password'] = bcrypt($request->password);
        }else{
            $request_data['password'] = $user->password;
        }
        $user->update($request_data);
        $operation=Operation::create([
            'type'      => 'update',
            'row_id'    => $user->id,
            'section'   => 'users',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = $user->name;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($user->user_img != 'no-user-image.png')
            Storage::disk('public_uploads')->delete('/users/' . $user->user_img);
            
            $operation=Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'users',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $user->name;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
        $user->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    //Archived All selected user
    public function archiveAll($ids){
        $users_id = explode(',',$ids);
        foreach($users_id as $id){
            $user = User::findOrFail($id);
            $operation=Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'users',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $user->name;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $user->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted cities */
    public function restoreAll($ids)
    {
        $users_id = explode(',',$ids);
        foreach($users_id as $id){
            $operation=Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'users',
                'admin_id'  => auth()->user()->id
            ]);
            $user = User::findOrFail($id);
            $restore = User::where('id',$id)->restore();
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $user->name;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    //Delete All selected User
    public function delAll($ids){
        $users_id = explode(',',$ids);
        foreach($users_id as $id){
            $user = User::findOrFail($id);
            $operation=Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'users',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = $user->name;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            if($user->user_img != 'no-user-image.png')
                Storage::disk('public_uploads')->delete('/users/' . $user->user_img);
            $del = $user->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    public function changeStatus(Request $request)
    {
        $operation=Operation::create([
            'type'      => 'changeStatus',
            'row_id'    => $request->id,
            'section'   => 'users',
            'admin_id'  => auth()->user()->id
        ]);
        $user = User::findOrFail($request->id);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = $user->name;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        User::where('id',$request->id)->update(['status'=> $request->status]);
        return response()->json(['success'=> __('site.update_status_successfully')]);

    }
    public function changeType(Request $request)
    {
        $operation=Operation::create([
            'type'      => 'changeType',
            'row_id'    => $request->id,
            'section'   => 'users',
            'admin_id'  => auth()->user()->id
        ]);
        $user = User::findOrFail($request->id);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = $user->name;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        User::where('id',$request->id)->update(['special'=> $request->type]);
        return response()->json(['success'=> __('site.update_status_successfully')]);

    }
}
