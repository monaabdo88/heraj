<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Operation;
use App\DataTables\Tags\TagsDatatables;
use App\DataTables\Tags\ArchiveDatatables;
use Illuminate\Support\Str;
use App\Models\OperationTranslation;
class TagsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_tags'])->only('index');      
        $this->middleware(['permission:create_tags'])->only(['create','store']);      
        $this->middleware(['permission:update_tags'])->only(['edit','update']);      
        $this->middleware(['permission:delete_tags'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_tags'])->only(['archiveAll','tags_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TagsDatatables $tags)
    {
        return $tags->render('dashboard.tags.index');
    }
    /* Get All Trashed Tags */
    public function tags_archive(ArchiveDatatables $tags){
        return $tags->render('dashboard.tags.index');
    }   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.title' => 'required|unique:tags_translations,title'];
        }//end of  for each
        $request->validate($rules);
        $request_data = $request->except('_token');
        foreach (config('translatable.locales') as $locale) {
            $request_data[$locale]['title'] =preg_replace('/\s+/', '-', $request_data[$locale]['title']);
        }
        $tag = Tag::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $tag->id,
            'section'   => 'tags',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\TagTranslation::where(['tag_id'=>$tag->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('dashboard.tags.edit',compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $request_data = $request->except(['_token','_method']);
        foreach (config('translatable.locales') as $locale) {
            $request_data[$locale]['title'] =preg_replace('/\s+/', '-', $request_data[$locale]['title']);
        }
        $tag->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $tag->id,
            'section'   => 'tags',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\TagTranslation::where(['tag_id'=>$tag->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/tags'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'tags',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\TagTranslation::where(['tag_id'=>$tag->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $tag->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    public function archiveAll($ids){
        $tags_id = explode(',',$ids);
        foreach($tags_id as $id){
            $tag = Tag::findOrFail($id);
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'tags',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\TagTranslation::where(['tag_id'=>$tag->id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $tag->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted tags */
    public function restoreAll($ids)
    {
        $tags_id = explode(',',$ids);
        foreach($tags_id as $id){
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'tags',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\TagTranslation::where(['tag_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = Tag::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    public function delAll($ids){
        $tags_id = explode(',',$ids);
        foreach($tags_id as $id){
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'tags',
                'admin_id'  => auth()->user()->id
            ]);
            $tag = Tag::findOrFail($id);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\TagTranslation::where(['tag_id'=>$tag->id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $del = $tag->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
}
