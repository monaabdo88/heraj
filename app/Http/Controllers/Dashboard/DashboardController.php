<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Intervention\Image\ImageManager;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Session;
use App\Models\Product;
use App\Models\Operation;
use Illuminate\Support\Facades\DB;
class DashboardController extends Controller
{
    public function index(){
        /*$products = Product::select(
            DB::raw('YEAR(created_at) as year'),
            DB::raw('MONTH(created_at) as month'),
        )->groupBy('month')->get(); */
        $products = Product::all();
        return view('dashboard.index',compact('products'));
    }
    public function noPermission(){
        toastr()->error(__('site.noPermissions'));
        return redirect('/cp');
    }
    public function operations(){
        if(auth()->user()->id == 1){
            $operations = Operation::orderBy('id','desc')->get();
            return view('dashboard.operation',compact('operations'));
        }else{
            toastr()->error(__('site.noPermissions'));
            return redirect('/cp');
        }
    }
    public function showAdminOperations($admin){
        if(auth()->user()->id == 1){
            $operations = Operation::orderBy('id','desc')->where('admin_id',$admin)->get();
            return view('dashboard.operation',compact('operations'));
        }else{
            toastr()->error(__('site.noPermissions'));
            return redirect('/cp');
        }
    }
    public function showOperations($section){
        if(auth()->user()->id == 1){
            $operations = Operation::orderBy('id','desc')->where('section',$section)->get();
            return view('dashboard.operation',compact('operations'));
        }else{
            toastr()->error(__('site.noPermissions'));
            return redirect('/cp');
        }
    }
    public function delOperation($ids){
        if(auth()->user()->id == 1){
            $operations_id = explode(',',$ids);
            foreach($operations_id as $op_id){
                $operation = Operation::where('id',$op_id)->delete();
                
            }
            if($operation){
                toastr()->success(__('site.deleted_successfully'));
                return redirect()->back();
            }
        }else{
            toastr()->error(__('site.noPermissions'));
            return redirect('/cp');
        }
    }
    // upload images via ckeditor
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('uploads/products'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('uploads/products/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            return $response;
        
        }
    }
}
