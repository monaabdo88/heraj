<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AlertController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_alerts'])->only('index');      
        $this->middleware(['permission:delete_alerts'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_alerts'])->only('archiveAll');      
    }
    public function index(){
        
    }
}
