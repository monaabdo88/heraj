<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Operation;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\Models\OperationTranslation;
class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_categories'])->only('index');      
        $this->middleware(['permission:create_categories'])->only(['create','store']);      
        $this->middleware(['permission:update_categories'])->only(['edit','update']);      
        $this->middleware(['permission:delete_categories'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_categories'])->only(['archiveAll','categories_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.categories.index');
    }
    public function categories_archive(){
        return view('dashboard.categories.archive');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.title' => 'required|unique:categories_translations,title'];
        }//end of  for each
        $rules+=[
            'icon'    => 'image|mimes:jpeg,png,jpg,gif,svg',
        ];
        $request->validate($rules);
        $request_data = $request->except('_token');
        
        if($request->hasFile('icon')){
            Image::make($request->icon)
                ->resize(265, 270)
                ->save(public_path('uploads/categories/' .$request->icon->hashName()));
            $request_data['icon'] = $request->icon->hashName();
        }
        $cat = Category::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $cat->id,
            'section'   => 'categories',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CategoryTranslation::where(['category_id'=>$cat->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('dashboard.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request_data = $request->except(['_token','_method']);
        if($request->icon){
            Storage::disk('public_uploads')->delete('/categories/' . $category->icon);
            Image::make($request->icon)
            ->resize(265, 270)
            ->save(public_path('uploads/categories/' .$request->icon->hashName()));
            $request_data['icon'] = $request->icon->hashName();
        }
        $category->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $category->id,
            'section'   => 'categories',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CategoryTranslation::where(['category_id'=>$category->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/categories'); 
    }
    public function changeStatus($id,$status)
    {
        $operation = Operation::create([
            'type'      => 'changeStatus',
            'row_id'    => $id,
            'section'   => 'categories',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CategoryTranslation::where(['category_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        Category::where('id',$id)->update(['status'=> $status]);
        return response()->json(['success'=> __('site.update_status_successfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /* Restore All deleted categories */
    public function restoreAll($ids)
    {
        $categories_id = explode(',',$ids);
        foreach($categories_id as $id){
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'categories',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\CategoryTranslation::where(['category_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = Category::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    public function archive($id)
    {
        $category = Category::findOrFail($id);
        $operation = Operation::create([
            'type'      => 'archive',
            'row_id'    => $id,
            'section'   => 'categories',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CategoryTranslation::where(['category_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $category->delete();
        toastr()->success(__('site.archived_successfully'));
        return redirect()->back();
    }
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $operation =Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'categories',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\CategoryTranslation::where(['category_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        Storage::disk('public_uploads')->delete('/categories/' . $category->icon);
        $category->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    
}
