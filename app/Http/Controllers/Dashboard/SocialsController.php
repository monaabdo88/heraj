<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Social;
use App\Models\Operation;
use App\DataTables\Socials\SocialsDatatables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\DataTables\Socials\ArchiveDatatables;
use App\Models\OperationTranslation;
class SocialsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_socials'])->only('index');      
        $this->middleware(['permission:create_socials'])->only(['create','store']);      
        $this->middleware(['permission:update_socials'])->only(['edit','update']);      
        $this->middleware(['permission:delete_socials'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_socials'])->only(['archiveAll','socials_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SocialsDatatables $socials)
    {
        return $socials->render('dashboard.socials.index');
    }
    /* Get All Trashed Socials */
    public function socials_archive(ArchiveDatatables $socials){
        return $socials->render('dashboard.socials.index');
    }   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.socials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.title' => 'required|unique:socials_translations,title'];
        }//end of  for each
        $rules+=[
            'icon'    => 'image|mimes:jpeg,png,jpg,gif,svg',
            'link'    => 'required'
        ];
        $request->validate($rules);
        $request_data = $request->except('_token');
        
        if($request->hasFile('icon')){
            Image::make($request->icon)
                ->resize(25, 25)
                ->save(public_path('uploads/icons/' .$request->icon->hashName()));
            $request_data['icon'] = $request->icon->hashName();
        }
        $social =Social::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $social->id,
            'section'   => 'socials',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\SocialTranslation::where(['social_id'=>$social->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Social $social)
    {
        return view('dashboard.socials.edit',compact('social'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Social $social)
    {
        $request_data = $request->except(['_token','_method']);
        if($request->icon){
            Storage::disk('public_uploads')->delete('/icons/' . $social->icon);
            Image::make($request->icon)
            ->resize(25, 25)
            ->save(public_path('uploads/icons/' .$request->icon->hashName()));
            $request_data['icon'] = $request->icon->hashName();
        }
        $social->update($request_data);
        $operation = Operation::create([
            'type'      => 'update',
            'row_id'    => $social->id,
            'section'   => 'socials',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\SocialTranslation::where(['social_id'=>$social->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/socials'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $social = Social::findOrFail($id);
        Storage::disk('public_uploads')->delete('/icons/' . $social->icon);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'socials',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\SocialTranslation::where(['social_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $social->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    public function archiveAll($ids){
        $socials_id = explode(',',$ids);
        foreach($socials_id as $id){
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'socials',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\SocialTranslation::where(['social_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $social = Social::findOrFail($id);
            $del = $social->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted countries */
    public function restoreAll($ids)
    {
        $socials_id = explode(',',$ids);
        foreach($socials_id as $id){
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'socials',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\SocialTranslation::where(['social_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = Social::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    public function delAll($ids){
        $socials_id = explode(',',$ids);
        foreach($socials_id as $id){
            $social = Social::findOrFail($id);
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'socials',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\SocialTranslation::where(['social_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            Storage::disk('public_uploads')->delete('/icons/' . $social->icon);
            $del = $social->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
}
