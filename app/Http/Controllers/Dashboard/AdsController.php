<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ad;
use App\Models\Operation;
use App\DataTables\Ads\AdsDatatables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use App\DataTables\Ads\ArchiveDatatables;
use App\Models\OperationTranslation;
class AdsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:read_google_ads'])->only('index');      
        $this->middleware(['permission:create_google_ads'])->only(['create','store']);      
        $this->middleware(['permission:update_google_ads'])->only(['edit','update']);      
        $this->middleware(['permission:delete_google_ads'])->only(['destroy','delAll']);      
        $this->middleware(['permission:archive_google_ads'])->only(['archiveAll','ads_archive','restoreAll']);      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdsDatatables $ads)
    {
        return $ads->render('dashboard.ads.index');   
    }
    /* Get All Trashed ads */
    public function ads_archive(ArchiveDatatables $ads){
        return $ads->render('dashboard.ads.index');
    }   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.title' => 'required|unique:ads_translations,title'];
        }//end of  for each
        $rules+=[
            'image'    => 'image|mimes:jpeg,png,jpg,gif,svg'
        ];
        $request->validate($rules);
        $request_data = $request->except('_token');
        
        if($request->hasFile('image')){
            if($request->position == 'inside-home'){
                $width= 555;
                $height=200;
            }else{
                $width= 262;
                $height = '';
            }
            Image::make($request->image)
                ->resize($width, $height)
                ->save(public_path('uploads/googleImages/' .$request->image->hashName()));
            $request_data['image'] = $request->image->hashName();
        }
        $ad=Ad::create($request_data);
        $operation = Operation::create([
            'type'      => 'add',
            'row_id'    => $ad->id,
            'section'   => 'ads',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\AdTranslation::where(['ad_id'=>$ad->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.added_successfully'));
		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        return view('dashboard.ads.edit',compact('ad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        $request_data = $request->except(['_token','_method']);
        if($request->image){
            if($request->position == 'inside-home'){
                $width= 555;
                $height=200;
            }else{
                $width= 262;
                $height = '';
            }
            Storage::disk('public_uploads')->delete('/googleImages/' . $ad->image);
            Image::make($request->image)
            ->resize($width, $height)
            ->save(public_path('uploads/googleImages/' .$request->image->hashName()));
            $request_data['image'] = $request->image->hashName();
        }
        $ad->update($request_data);
        $operation=Operation::create([
            'type'      => 'update',
            'row_id'    => $ad->id,
            'section'   => 'ads',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\AdTranslation::where(['ad_id'=>$ad->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        toastr()->success(__('site.updated_successfully'));
        return redirect('cp/ads');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Ad::findOrFail($id);
        Storage::disk('public_uploads')->delete('/googleImages/' . $ad->image);
        $operation = Operation::create([
            'type'      => 'delete',
            'row_id'    => $id,
            'section'   => 'ads',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\AdTranslation::where(['ad_id'=>$id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        $ad->forceDelete();
        toastr()->success(__('site.deleted_successfully'));
        return redirect()->back();
    }
    //Archive All Selected Records
    public function archiveAll($ids){
        $ads_id = explode(',',$ids);
        foreach($ads_id as $id){
            $ad = Ad::findOrFail($id);
            $operation = Operation::create([
                'type'      => 'archive',
                'row_id'    => $id,
                'section'   => 'ads',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\AdTranslation::where(['ad_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
             $del = $ad->delete();
        }       
        if($del)
        toastr()->success(__('site.archived_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    /* Restore All deleted ads */
    public function restoreAll($ids)
    {
        $ads_id = explode(',',$ids);
        foreach($ads_id as $id){
            $operation = Operation::create([
                'type'      => 'restore',
                'row_id'    => $id,
                'section'   => 'ads',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\AdTranslation::where(['ad_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            $restore = Ad::where('id',$id)->restore();
        }       
        if($restore)
            toastr()->success(__('site.restore_successfully'));
        else
            toastr()->error('Somthing Wrong Please Try again later');
        return redirect()->back();
       
       
    }
    public function delAll($ids){
        $ads_id = explode(',',$ids);
        foreach($ads_id as $id){
            $ad = Ad::findOrFail($id);
            $operation = Operation::create([
                'type'      => 'delete',
                'row_id'    => $id,
                'section'   => 'ads',
                'admin_id'  => auth()->user()->id
            ]);
            foreach (config('translatable.locales') as $locale) {
                $data['locale'] = $locale;
                $data['title'] = \App\Models\AdTranslation::where(['ad_id'=>$id,'locale'=> $locale])->first()->title;
                $data['operation_id'] = $operation->id;
                OperationTranslation::create($data);
            }
            Storage::disk('public_uploads')->delete('/googleImages/' . $ad->image);
            $del = $ad->forceDelete();
        }       
        if($del)
        toastr()->success(__('site.deleted_successfully'));
        else
        toastr()->error('Somthing Wrong Please Try again later');

		return redirect()->back();
    }
    public function changeStatus(Request $request)
    {
        $operation = Operation::create([
            'type'      => 'changeStatus',
            'row_id'    => $request->id,
            'section'   => 'ads',
            'admin_id'  => auth()->user()->id
        ]);
        foreach (config('translatable.locales') as $locale) {
            $data['locale'] = $locale;
            $data['title'] = \App\Models\AdTranslation::where(['ad_id'=>$request->id,'locale'=> $locale])->first()->title;
            $data['operation_id'] = $operation->id;
            OperationTranslation::create($data);
        }
        Ad::where('id',$request->id)->update(['status'=> $request->status]);
        return response()->json(['success'=> __('site.update_status_successfully')]);

    }
}
