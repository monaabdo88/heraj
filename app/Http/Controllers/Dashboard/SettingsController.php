<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Operation;
use Illuminate\Http\Request;
use App\Models\Setting;
use Intervention\Image\ImageManager;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Validation\Rule;
class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:update_settings'])->only(['getSettings','updateSettings']);      
    }
    public function getSettings(){
        return view('dashboard.settings');
    }
    public function updateSettings(Request $request){
        $this->validate($request,[
            'site_name_ar'          => 'required|min:3',
            'site_name_en'          => 'required|min:3',
            'site_email'            => 'required|email',
            'site_phone'            => 'required|min:3|max:14',
            'site_address_ar'       => 'required|min:3',
            'site_address_en'       => 'required|min:3',
            'site_description_ar'   => 'required|min:3',
            'site_description_en'   => 'required|min:3',
            'site_tags_ar'          => 'required|min:3',
            'site_tags_en'          => 'required|min:3',
            'site_copyrights_ar'    => 'required|min:3',
            'site_copyrights_en'    => 'required|min:3',
            'site_close_msg_ar'     => 'required|min:3',
            'site_close_msg_en'     => 'required|min:3',
            'logo_ar'               => 'mimes:jpeg,bmp,png',
            'logo_en'               => 'mimes:jpeg,bmp,png',
            'watermark'             => 'mimes:jpeg,bmp,png' 
        ]);
        $request_data = $request->except(['_token','title']);
        $request_data['logo_ar'] = settings()->logo_ar;
        $request_data['logo_en'] = settings()->logo_en;
        $request_data['watermark']  = settings()->watermark;

        if($request->hasFile('logo_ar')){
            if(settings()->logo_ar){
                unlink(public_path('uploads/images/') .settings()->logo_ar);
            }
            $logo_name_ar = time().$request->logo_ar->hashName();
            Image::make($request->logo_ar)
                ->resize(300, 75)
                ->save(public_path('uploads/images/' .$logo_name_ar));
            $request_data['logo_ar'] = $logo_name_ar;
        }

        if($request->hasFile('logo_en')){
            if(settings()->logo_en){
                unlink(public_path('uploads/images/') .settings()->logo_en);
            }
            $logo_name_en = time().$request->logo_en->hashName();
            Image::make($request->logo_en)
                ->resize(300, 75)
                ->save(public_path('uploads/images/' .$logo_name_en));
            $request_data['logo_en'] = $logo_name_en;
        }

        if($request->hasFile('watermark')){
            if(settings()->watermark){
                unlink(public_path('uploads/images/') .settings()->watermark);
            }
            $watermark = time().$request->watermark->hashName();
            Image::make($request->watermark)
                ->resize(300, 75)
                ->save(public_path('uploads/images/' .$watermark));
            $request_data['watermark'] = $watermark;
        }

        Setting::where('id',1)->update($request_data);
        Operation::create([
            'type'      => 'update',
            'row_id'    => '1',
            'section'   => 'settings',
            'admin_id'  => auth()->user()->id,
        ]);
        toastr()->success(__('site.updated_successfully'));
		return redirect()->back();
    }
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('uploads/images'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('uploads/images/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            return $response;
        
        }
    }
}
