<?php
//Active Link in sidebar menu function
if(! function_exists('active_link')){
    function active_link($path){
        $path = explode('.', $path);
        $segment = 3;
        foreach($path as $p) {
            if((request()->segment($segment) == $p) == false) {
                return '';
            }
                $segment++;
            }
        return 'active';
    }
    
}
//Function to get all settings for the site
if(! function_exists('settings')){
    function settings(){
        $settings = \App\Models\Setting::orderBy('id','desc')->first();
        return $settings;
    }
}
// Function to get All Socials link
if(! function_exists('get_socials')){
    function get_socials(){
        $socials =  \App\Models\Social::orderBy('id','desc')->get();
        return $socials;
    }
}
//function to get categories&childs
if(! function_exists('cats_nav')){
    function cats_nav(){
        $categories = \App\Models\Category::whereNull('parent_id')->with('childs')->get();
        return $categories;
    }
}
// Function to get All Pages in the site
if(! function_exists('get_pages')){
    function get_pages($place,$limit){
        $pages = \App\Models\Page::orderBy('id','desc')->where(['place'=>$place,'status' => '1'])->limit($limit)->get();
        return $pages;
    }
}
// Function to get All countries 
if(! function_exists('get_countries')){
    function get_countries(){
        $countries = \App\Models\Country::orderBy('id','desc')->get();
        return $countries;
    }
}
//get products by type
if(! function_exists('products_type')){
    function products_type($type){
        $products_type = \App\Models\Product::where(['product_type'=>$type,'status'=> '1'])->orderBy('id','desc')->limit(8)->get();
        return $products_type;
    }
}
// function to get the count of rows in any table
if(! function_exists('get_count')){
    function get_count($tbl_name){
        $count = \DB::table($tbl_name)->where('deleted_at',null)->get()->count();
        return $count;
    }
}
//function to show ads in the pages
if(!function_exists('show_ads')){
    function show_ads($position,$limit){
        $ads = \App\Models\Ad::orderBy('id','desc')->where(['status'=> '1','position' => $position])->limit($limit)->get();
        return $ads;
    }
}
//function to translate the data in yajra datatables
if (!function_exists('datatable_lang')) {
    function datatable_lang() {
        return ['sProcessing' => trans('site.sProcessing'),
            'sLengthMenu'        => trans('site.sLengthMenu'),
            'sZeroRecords'       => trans('site.sZeroRecords'),
            'sEmptyTable'        => trans('site.sEmptyTable'),
            'sInfo'              => trans('site.sInfo'),
            'sInfoEmpty'         => trans('site.sInfoEmpty'),
            'sInfoFiltered'      => trans('site.sInfoFiltered'),
            'sInfoPostFix'       => trans('site.sInfoPostFix'),
            'sSearch'            => trans('site.sSearch'),
            'sUrl'               => trans('site.sUrl'),
            'sInfoThousands'     => trans('site.sInfoThousands'),
            'sLoadingRecords'    => trans('site.sLoadingRecords'),
            'oPaginate'          => [
                'sFirst'            => trans('site.sFirst'),
                'sLast'             => trans('site.sLast'),
                'sNext'             => trans('site.sNext'),
                'sPrevious'         => trans('site.sPrevious'),
            ],
            'oAria'            => [
                'sSortAscending'  => trans('site.sSortAscending'),
                'sSortDescending' => trans('site.sSortDescending'),
            ],
        ];
    }
}
//function to cp 
if(! function_exists('cp_url')){
    function cp_url($url = null){
        return url('cp/'.$url);
    }
}
//function to get all categories 
if(! function_exists('load_cats')){
    function load_cats($select = null,$cat_id = null){
        $cats = \App\Models\Category::all();
        $deps = [];
        foreach ($cats as $cat){
            $list_arr = [];
            $list_arr['icon']       = '';
            $list_arr['li_attr']    = '';
            $list_arr['a_attr']     = '';
            $list_arr['children']   ='';
            if($select !== null && $select == $cat->id){
                $list_arr['state']      = [
                    'opened'    => true,
                    'selected'  => true,
                    'disabled'  => false,
                ];

            }
            if($cat_id !== null && $cat_id == $cat->id){
                $list_arr['state']      = [
                    'opened'    => false,
                    'selected'  => false,
                    'disabled'  => true,
                    'hidden'    => true,
                ];

            }
            $list_arr['id']     = $cat->id;
            $list_arr['parent'] = $cat->parent_id == null ? '#' : $cat->parent_id;
            $list_arr['text']   = $cat->title;
            array_push($deps,$list_arr);
        }
        return json_encode($deps,JSON_UNESCAPED_UNICODE);
    }
}
//function to get all archived categories 
if(! function_exists('load_archived_cats')){
    function load_archived_cats($select = null,$cat_id = null){
        $cats = \App\Models\Category::onlyTrashed()->get();
        $deps = [];
        foreach ($cats as $cat){
            $list_arr = [];
            $list_arr['icon']       = '';
            $list_arr['li_attr']    = '';
            $list_arr['a_attr']     = '';
            $list_arr['children']   ='';
            if($select !== null && $select == $cat->id){
                $list_arr['state']      = [
                    'opened'    => true,
                    'selected'  => true,
                    'disabled'  => false,
                ];

            }
            if($cat_id !== null && $cat_id == $cat->id){
                $list_arr['state']      = [
                    'opened'    => false,
                    'selected'  => false,
                    'disabled'  => true,
                    'hidden'    => true,
                ];

            }
            $list_arr['id']     = $cat->id;
            $list_arr['parent'] = $cat->parent_id == null ? '#' : $cat->parent_id;
            $list_arr['text']   = $cat->title;
            array_push($deps,$list_arr);
        }
        return json_encode($deps,JSON_UNESCAPED_UNICODE);
    }
}