<?php

namespace App\Providers;
use Config;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $config = array(
            'driver'  => settings()->mail_driver,
            'host'       => settings()->mail_host,
            'port'       => settings()->mail_port,
            'from'       => array('address' => settings()->site_email, 'name' => settings()->site_name_en),
            'encryption' => settings()->mail_encrypt,
            'username'   => settings()->mail_username,
            'password'   => settings()->mail_password,
            'sendmail'   => '/usr/sbin/sendmail -bs',
            'pretend'    => false,
        );
        Config::set('mail', $config);
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    
    
}
