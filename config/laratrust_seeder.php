<?php
return [
    'role_structure' => [
        'super_admin' => [
            'settings'  => 'u,r',
            'countries' => 'a,c,r,u,d',
            'cities'    => 'a,c,r,u,d',
            'google_ads'    => 'a,c,r,u,d',
            'pages'     => 'a,c,r,u,d',
            'tags'      => 'a,c,r,u,d',
            'alerts'    => 'r,d',
            'customer_support_msg'  => 'r,re,d',
            'mail_list' => 'r,d',
            'archive'   =>'r,s,d',
            'colors'    => 'a,c,r,u,d',
            'sizes'     => 'a,c,r,u,d',
            'categories' => 'a,c,r,u,d',
            'products' => 'a,c,r,u,d',
            'sliders' => 'a,c,r,u,d',
            'socials' => 'a,c,r,u,d',
            'users' => 'a,c,r,u,d',
            'admins'    => 'a,c,r,u,d'
        ],
        'admin' => []
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete',
        'a' => 'archive',
        're' => 'replay',
        's' =>'restore'
    ]
];
