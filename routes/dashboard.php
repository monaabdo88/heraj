<?php
Route::group(['prefix'=>'cp','namespace'=>'Auth'],function(){   
    //Login Routes
    Route::get('login','LoginController@showLoginForm')->name('cpLogin');
    Route::post('login','LoginController@adminLogin')->name('adminLogin');
    Route::post('adminLogout','LoginController@adminLogout')->name('adminLogout');
    
});
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){ 
        Route::group(['prefix' => 'cp','middleware'=>['auth:admin']], function () {
            Route::get('/','DashboardController@index')->name('cp.index');
            Route::get('/NoPermission','DashboardController@noPermission');
            //Operation route
            Route::get('operations','DashboardController@operations');
            Route::get('showOperations/{section}','DashboardController@showOperations');
            Route::get('showAdminOperations/{admin}','DashboardController@showAdminOperations');
            Route::get('delOperation/{id}','DashboardController@delOperation');
            //Settings Routes
            Route::get('settings','SettingsController@getSettings')->name('cp.getSettings');
            Route::post('settings','SettingsController@updateSettings')->name('cp.updateSettings');
            //Slider Routes
            Route::resource('sliders','SlidersController')->except('show');
            Route::get('sliders/delSlider/{id}','SlidersController@destroy');
            Route::get('sliders/archiveAll/{ids}','SlidersController@archiveAll');
            Route::get('sliders/delAll/{ids}','SlidersController@delAll');
            Route::get('sliders/changeStatus','SlidersController@changeStatus');
            Route::get('sliders_archive','SlidersController@sliders_archive');
            Route::get('sliders/restoreAll/{ids}','SlidersController@restoreAll');
            //Ads Routes
            Route::resource('ads','AdsController')->except('show');
            Route::get('ads/delAd/{id}','AdsController@destroy');
            Route::get('ads/archiveAll/{ids}','AdsController@archiveAll');
            Route::get('ads/delAll/{ids}','AdsController@delAll');
            Route::get('ads/changeStatus','AdsController@changeStatus');
            Route::get('ads_archive','AdsController@ads_archive');
            Route::get('ads/restoreAll/{ids}','AdsController@restoreAll');
            //Tags Routes
            Route::resource('tags','TagsController')->except('show');
            Route::get('tags/delTag/{id}','TagsController@destroy');
            Route::get('tags/archiveAll/{ids}','TagsController@archiveAll');
            Route::get('tags/delAll/{ids}','TagsController@delAll');
            Route::get('tags_archive','TagsController@tags_archive');
            Route::get('tags/restoreAll/{ids}','TagsController@restoreAll');
            //Pages Routes
            Route::resource('pages','PagesController')->except('show');
            Route::get('pages/delPage/{id}','PagesController@destroy');
            Route::get('pages/archiveAll/{ids}','PagesController@archiveAll');
            Route::get('pages/delAll/{ids}','PagesController@delAll');
            Route::get('pages/changeStatus','PagesController@changeStatus');
            Route::get('pages_archive','PagesController@pages_archive');
            Route::get('pages/restoreAll/{ids}','PagesController@restoreAll');
            //Countries Routes
            Route::resource('countries','CountriesController')->except('show');
            Route::get('countries/delCountry/{id}','CountriesController@destroy');
            Route::get('countries/archiveAll/{ids}','CountriesController@archiveAll');
            Route::get('countries/delAll/{ids}','CountriesController@delAll');
            Route::get('countries_archive','CountriesController@countries_archive');
            Route::get('countries/restoreAll/{ids}','CountriesController@restoreAll');
            //Cities Routes
            Route::resource('cities','CitiesController');
            Route::get('cities/delCity/{id}','CitiesController@destroy');
            Route::get('cities/archiveAll/{ids}','CitiesController@archiveAll');
            Route::get('cities/delAll/{ids}','CitiesController@delAll');
            Route::get('cities_archive','CitiesController@cities_archive');
            Route::get('cities/restoreAll/{ids}','CitiesController@restoreAll');
            //Socials Routes
            Route::resource('socials','SocialsController')->except('show');
            Route::get('socials/delSocial/{id}','SocialsController@destroy');
            Route::get('socials/archiveAll/{ids}','SocialsController@archiveAll');
            Route::get('socials/delAll/{ids}','SocialsController@delAll');
            Route::get('socials_archive','SocialsController@socials_archive');
            Route::get('socials/restoreAll/{ids}','SocialsController@restoreAll');
            //Colors Routes
            Route::resource('colors','ColorsController')->except('show');
            Route::get('colors/delColor/{id}','ColorsController@destroy');
            Route::get('colors/archiveAll/{ids}','ColorsController@archiveAll');
            Route::get('colors/delAll/{ids}','ColorsController@delAll');
            Route::get('colors_archive','ColorsController@colors_archive');
            Route::get('colors/restoreAll/{ids}','ColorsController@restoreAll');
            //Sizes Routes
            Route::resource('sizes','SizesController')->except('show');
            Route::get('sizes/archiveSize/{id}','SizesController@archive');
            Route::get('sizes/delSize/{id}','SizesController@destroy');
            Route::get('sizes/archiveAll/{ids}','SizesController@archiveAll');
            Route::get('sizes/delAll/{ids}','SizesController@delAll');
            Route::get('sizes_archive','SizesController@sizes_archive');
            Route::get('sizes/restoreAll/{ids}','SizesController@restoreAll');
            //Categories Routes
            Route::resource('categories','CategoriesController')->except('show');
            Route::get('categories/archiveCategory/{id}','CategoriesController@archive');
            Route::get('categories/delCategory/{id}','CategoriesController@destroy');
            Route::get('categories_archive','CategoriesController@categories_archive');
            Route::get('categories/restoreAll/{ids}','CategoriesController@restoreAll');
            //Products Routes
            Route::resource('products','ProductsController')->except('show');
            Route::get('products/delProduct/{id}','ProductsController@destroy');
            Route::get('products/delAll/{ids}','ProductsController@delAll');
            Route::get('products/changeStatus','ProductsController@changeStatus');
            Route::post('products/media','ProductsController@storeMedia')->name('products.storeMedia');;
            Route::get('products/changeType','ProductsController@changeType');
            Route::get('products_archive','ProductsController@products_archive');
            Route::get('products/restoreAll/{ids}','ProductsController@restoreAll');
            Route::get('getUserProducts/{id}','ProductsController@getUserProducts');
            //delete image from server and remove it from dropzone box
            Route::get('products/deleteImag/{name}','ProductsController@del_image');
            //Users Routes
            Route::resource('users','UsersController')->except('show');
            Route::get('users/delUser/{id}','UsersController@destroy');
            Route::get('users/archiveAll/{ids}','UsersController@archiveAll');
            Route::get('users/delAll/{ids}','UsersController@delAll');
            Route::get('users/changeStatus','UsersController@changeStatus');
            Route::get('users/changeType','UsersController@changeType');
            Route::get('users_archive','UsersController@users_archive');
            Route::get('users/restoreAll/{ids}','UsersController@restoreAll');
            //Admins Routes
            Route::resource('admins','AdminsController')->except('show');
            Route::get('admins/archiveAll/{ids}','AdminsController@archiveAll');
            Route::get('admins/delAll/{ids}','AdminsController@delAll');
            Route::get('admins_archive','AdminsController@admins_archive');
            Route::get('admin/restoreAll/{ids}','AdminsController@restoreAll');
            
           //Alert Routes
            Route::get('alerts','AlertController@index');
            //ckeditor
            Route::post('ckeditor/upload', 'DashboardController@upload')->name('ckeditor.upload');
        });
    });