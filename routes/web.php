<?php
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){ 
        Route::group(['namespace'=> 'Site'],function(){
            Route::get('/','HomeController@index');
            //Login Route
            Route::post('postLogin','Auth\LoginController@postLogin')->name('postLogin');
            Route::post('userLogout','Auth\LoginController@userLogout')->name('userLogout');
            //Resigster Route
            Route::post('postSignup','Auth\RegisterController@newUser')->name('signup');
            //Socials Route
            Route::get('/auth/redirect/{provider}', 'Auth\SocialController@redirect');
            Route::get('/callback/{provider}', 'Auth\SocialController@callback');
            //User Routes
            Route::group(['middleware'=>'verified'],function(){
                Route::resource('user','UsersController')->except(['index','create','store','destroy']);
                //Route::get('profile/{id}','UserController@getProfile');
                Route::get('userProducts/{id}','UsersController@userProducts');
                Route::get('addProduct','UsersController@new_product');
                Route::get('myFavs/{id}','UsersController@myFavs');
                Route::get('myMsgs/{id}','UsersController@myMsgs');
                Route::resource('product','ProductsController')->except(['index']);
                Route::get('delProduct/{id}','ProductsController@destroy');
                //Products Dropzone media
                Route::post('product/media','ProductsController@storeMedia')->name('product.storeMedia');;
                Route::get('productImg/deleteImag/{name}','ProductsController@del_image');
            });
            Route::post('rateProduct','ProductsController@rateProduct')->name('product.rate');
            //categories routes
            Route::get('categories','CategoriesController@index')->name('categories');
            Route::get('category/{id}','CategoriesController@show')->name('categories.show');
            //products route
            Route::get('allProducts','HomeController@all_products');
            Route::get('showProduct/{slug}','ProductsController@show');
            //subscribe routes
            Route::post('subscribe','HomeController@subscribe')->name('subscribe');
            //Pages Route
            Route::get('getPage/{id}','HomeController@get_page')->name('getPage');
            //Route Contact Page
            Route::get('contact','HomeController@contact');
            Route::post('contactMsg','HomeController@contactMsg')->name('contactMessage');
            //Route To get All cities for one country
            Route::get('get_cities/{country_id}/{lang}','HomeController@get_cities');
            //Route to get all categories
            Route::get('all_categories','HomeController@all_cats');
            //ckeditor
            Route::post('ckeditor/upload', 'HomeController@upload')->name('ckeditor.uploadProduct');
            
        });
        Auth::routes(['verify'=> true]);
    });


Route::get('/home', 'Site\HomeController@index')->name('home');
