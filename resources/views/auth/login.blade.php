@extends('site.layouts.app')

@section('content')
<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>@lang('site.login')</h2>
                    <div class="breadcrumb__option">
                        <a href="{{url('/')}}">@lang('site.home')</a>
                        <span>@lang('site.login')</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->
<section class="contact spad">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('site.login')</div>

                <div class="card-body">
                    <form class="form-login" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">@lang('site.email')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">@lang('site.password')</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        @lang('site.remember_me')
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('site.login')
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                       @lang('site.forget_password')
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                    <div class="text-center text-muted delimiter">@lang('site.social_login')</div>
                <br>
                <br>
                <div class="d-flex justify-content-center social-buttons">
                    <a href="{{ url('/auth/redirect/twitter') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Twitter">
                      <i class="fa fa-twitter"></i>
                    </a>
                    <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Facebook">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a href="{{ url('/auth/redirect/linkedin') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Linkedin">
                      <i class="fa fa-linkedin"></i>
                    </a>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@push('scripts')
      <script>
        // Wait for the DOM to be ready
          $(function() {
              $("#login-submit").click(function(){
                    $(".form-login").validate({
                    rules: {
                      email: {
                        required: true,
                        email: true
                      },
                      password: {
                        required: true,
                        minlength: 5
                      }
                    },
                  messages: {
                      password: {
                        required: "<span class='text-danger'><?=__('site.password_field')?></span>",
                        minlength: "<span class='text-danger'><?=__('site.min_length_field')?></span>"
                      },
                      email: "<span class='text-danger'><?=__('site.email_field')?></span>"
                    },
                    submitHandler: function(form) {
                      form.submit();
                    }
                  });
              });
              //clear 
            $('#loginModal').on('hidden.bs.modal', function() {
                  var $alertas = $('.form-login');
                  $alertas.validate().resetForm();
                  $alertas.find('.error').removeClass('error');
            });
          });
          
      </script>
    @endpush