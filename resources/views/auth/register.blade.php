@extends('site.layouts.app')

@section('content')
<!-- Breadcrumb Section Begin -->
<section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>@lang('site.signup')</h2>
                    <div class="breadcrumb__option">
                        <a href="{{url('/')}}">@lang('site.home')</a>
                        <span>@lang('site.signup')</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->
<section class="contact spad">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('site.signup')</div>

                <div class="card-body">
                    <form class="form-signup" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>@lang('site.name')</label>
                            <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="@lang('site.name')"/>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.email')</label>
                            <input type="email" name="email" class="form-control" placeholder="@lang('site.email')">
                        </div>
                        <div class="form-group">
                            <label>@lang('site.show_email')</label>
                            <select name="show_email" class="form-control">
                                <option value="0">@lang('site.no')</option>
                                <option value="1">@lang('site.yes')</option>
                            </select>
                            
                        </div>
                        <div class="form-group">
                            <label>@lang('site.password')</label>
                          <input type="password" id="password" name="password" class="form-control" placeholder="@lang('site.password')">
                        </div>
                        <div class="form-group">
                            <label>@lang('site.password_confirm')</label>
                            <input type="password" name="password_confirmation" placeholder="@lang('site.password_confirm')" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>@lang('site.phone')</label>
                            <div class="clearfix"></div>
                            <select id="country_code" name="country_code" class="form-control float-left">
                              @foreach (get_countries() as $item)
                                  <option data-iconurl="{{asset('uploads/flags/'.$item->flage)}}" value="{{$item->code}}">{{$item->code}}</option>
                              @endforeach
                            </select>
                              <input type="text" name="phone" class="form-control col-md-8 float-right" placeholder="@lang('site.phone')" value="{{old('phone')}}"/>
                            
                        </div>
                        <div class="form-group">
                            <label>@lang('site.show_phone')</label>
                            <select name="show_phone" class="form-control">
                                <option value="0">@lang('site.no')</option>
                                <option value="1">@lang('site.yes')</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.gender')</label>
                            <select class="form-control" name="gender">
                                <option value="female">@lang('site.female')</option>
                                <option value="male">@lang('site.male')</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.country_name')</label>
                            <select name="country_id" class="form-control" id="country">
                                    <option value="">@lang('site.selecte_country')</option>
                                @foreach (get_countries() as $country)
                                    <option value="{{$country->id}}">{{$country->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.city')</label>
                            <select class="form-control" name="city_id" id="city"></select>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.facebook_link')</label>
                            <input type="text" name="facebook_link" class="form-control" value="{{old('facebook_link')}}"/>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.twitter_link')</label>
                            <input type="text" name="twitter_link" class="form-control" value="{{old('twitter.link')}}"/>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.insatgram_link')</label>
                            <input type="text" name="instagram_link" class="form-control" value="{{old('instagram.link')}}"/>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.website_link')</label>
                            <input type="text" name="website_link" class="form-control" value="{{old('website.link')}}"/>
                        </div>
                        <div class="form-group">
                            <label>@lang('site.image')</label>
                            <input type="file" id="logo_image" name="user_img" class="form-control"/>
                            <br>
                            <img src="#" id="logo-img-tag" style="display: none" width="150" height="150" class="img-responsive img-thumbnail" />
                        
                        </div>
                        <div class="form-group col-md-8 offset-md-2 text-center">
                          <div class="g-recaptcha" data-sitekey="6Lf2kgQaAAAAADChUMt-QCjqL161FETtkG22DLoM" data-callback="enableSignup"></div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   @lang('site.signup')
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="text-center text-muted delimiter">@lang('site.social_login')</div>
                <br>
                <div class="d-flex justify-content-center social-buttons">
                  <a href="{{ url('/auth/redirect/twitter') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Twitter">
                    <i class="fa fa-twitter"></i>
                  </a>
                  <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Facebook">
                    <i class="fa fa-facebook"></i>
                  </a>
                  <a href="{{ url('/auth/redirect/linkedin') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Linkedin">
                    <i class="fa fa-linkedin"></i>
                  </a>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@push('scripts')
  <script>
    // Wait for the DOM to be ready
      $(function() {
          $("#signup-submit").click(function(){
                $(".form-signup").validate({
                rules: {
                  name:'required',
                  show_email:'required',
                  show_phone:'required',
                  phone:'required',
                  country_id:'required',
                  email: {
                    required: true,
                    email: true
                  },
                  password: {
                    required: true,
                    minlength: 5
                  },
                  password_confirmation: {
                    equalTo: "#password",
                    required:true
                }
                },
              messages: {
                  password: {
                        required: "<span class='text-danger'><?=__('site.password_field')?></span>",
                        minlength: "<span class='text-danger'><?=__('site.min_length_field')?></span>"
                  },
                  email: "<span class='text-danger'><?=__('site.email_field')?></span>",
                  name:"<span class='text-danger'><?=__('site.username_field')?></span>",
                  phone:"<span class='text-danger'><?=__('site.phone_field')?></span>",
                  country_id:"<span class='text-danger'><?=__('site.country_id_field')?></span>",
                  password_confirmation:"<span class='text-danger'><?=__('site.password_confirmation_field')?></span>",
                  show_email:"<span class='text-danger'><?=__('site.show_email_field')?></span>",
                  show_phone:"<span class='text-danger'><?=__('site.show_phone_field')?></span>"
                  
                },
                submitHandler: function(form) {
                  form.submit();
                }
              });
          });
            //clear 
            $('#signupModal').on('hidden.bs.modal', function() {
                  var $alertas = $('.form-signup');
                  $alertas.validate().resetForm();
                  $alertas.find('.error').removeClass('error');
            });
      });
      $("#country_code").selectBoxIt();
      function enableSignup(){
                document.getElementById("signup-submit").disabled = false;
            }   
</script>
@endpush
