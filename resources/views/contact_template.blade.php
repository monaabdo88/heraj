<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                    <tr>
                        <td align="center" bgcolor="#f1f1f1" style="padding:20px 0">
                            @if(app()->getLocale() == 'ar')
                                <img src="{{asset('uploads/images/'.settings()->logo_ar)}}" alt="">
                            @else
                                <img src="{{asset('uploads/images/'.settings()->logo_en)}}" alt="">
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                        <b>@lang('site.hello') {{(app()->getLocale() == 'ar')? settings()->site_name_ar : settings()->site_name_en}},</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                        @lang('site.message_top') : {{ $name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td style="padding:5px 0">
                                                                <b>@lang('site.email') : </b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:5px 0">
                                                              {{$email}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td style="padding:5px 0">
                                                              <b>@lang('site.subject') :</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:5px 0">
                                                             {{$subject}}
                                                             
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:5px 0">
                                                              <b>@lang('site.message') :</b>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:5px 0">
                                                             {{$msg}}
                                                             <br>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
    </table>
                   
</body>
</html>