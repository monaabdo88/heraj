@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.admins')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.admins')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('admins.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group col-md-6">
                        <label>@lang('site.name')</label>
                        <input type="text" name="name" class="form-control" value="{{old('name')}}" />
                    </div>
                    <div class="form-group col-md-6">
                        <label>@lang('site.email')</label>
                        <input type="email" name="email" class="form-control" value="{{old('email')}}" />
                    </div>
                    <div class="form-group col-md-6">
                        <label>@lang('site.password')</label>
                        <input type="password" name="password" class="form-control"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>@lang('site.password_confirm')</label>
                        <input type="password" name="password_confirmation" class="form-control" />
                    </div> 
                    <div class="form-group col-md-12">
                        <label>@lang('site.permissions')</label>
                        @php
                                $models = ['settings','countries','cities','users','sliders','google_ads','socials','pages','alerts','customer_support_msg','mail_list','categories', 'products', 'admins', 'archive'];
                                
                        @endphp
                        <ul>
                            @foreach ($models as $index=>$model)
                                <li style="margin:0 10px" class="{{ $index == 0 ? 'active' : '' }}"><b>@lang('site.' . $model)</b>
                                    <ul>
                                        @if ($model == 'settings')
                                            @php
                                                $maps = ['update'];
                                            @endphp
                                        @elseif($model == 'archive')
                                            @php
                                                $maps = ['restore','delete']   
                                            @endphp
                                        @elseif($model == 'customer_support_msg')
                                            @php
                                                $maps = ['replay','delete']
                                            @endphp
                                        @elseif($model == 'mail_list' || $model == 'alerts')
                                            @php
                                                $maps = ['delete'];
                                            @endphp
                                        @else
                                            @php
                                                $maps = ['create', 'read', 'update','archive', 'delete'];                   
                                            @endphp
                                        @endif
                                        @foreach ($maps as $map)
                                        <li><input type="checkbox" name="permissions[]" value="{{ $map . '_' . $model }}"> @lang('site.' . $map)</li>
                                        @endforeach
                                        
                                    </ul>
                                    
                                </li>
                            @endforeach
                        </ul>
                        
                    </div> 
                    <div class="form-group col-md-12">
                        <label>@lang('site.image')</label>
                        <input type="file" id="logo_image" name="image" class="form-control"/>
                        <br>
                        <img src="#" id="logo-img-tag" class="img-responsive img-thumbnail" />
                    
                    </div>
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                    </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
