@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.categories')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.categories')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form id="file-upload-form" class="uploader" action="{{route('categories.update',$category->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                        @foreach (config('translatable.locales') as $locale)
                            <div class="form-group col-md-12">
                                <label>@lang('site.' . $locale . '.title')</label>
                                <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{ $category->translate($locale)->title }}">
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label>@lang('site.' . $locale .'.description')</label>
                            <textarea class="form-control" name="{{ $locale }}[description]">{{$category->translate($locale)->description }}</textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label>@lang('site.' . $locale .'.tags')</label>
                            <textarea class="form-control" name="{{ $locale }}[tags]">{{$category->translate($locale)->tags }}</textarea>
                            </div>
                        @endforeach
                        <div class="form-group col-md-12">
                            <label>@lang('site.category')</label>
                            <div id="jstree"></div>
                            <input type="hidden" class="parent_id" value="{{$category->parent_id}}" name="parent_id">
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.status')</label>
                            <select class="form-control" name="status">
                                <option value="0" {{($category->status == '0') ? 'selected':''}}>@lang('site.not-active')</option>
                                <option value="1" {{($category->status == '1') ? 'selected':''}}>@lang('site.active')</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.icon')</label>
                            <input type="file" id="logo_image" name="icon" class="form-control"/>
                            <br>
                        <img src="{{url('uploads/categories/'.$category->icon)}}" id="logo-img-tag" class="img-responsive img-thumbnail" />
                        
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> @lang('site.update')</button>
                        </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
@push('scripts')
<script>
    //jstree categories
    $(document).ready(function(){
            $('#jstree').jstree({
                "core" : {
                    'data' : {!! load_cats($category->parent_id,$category->id) !!},
                    "themes" : {
                        "variant" : "large"
                    }
                },
                "checkbox" : {
                    "keep_selected_style" : false
                },
                "plugins" : [ "wholerow" ]
                });
            });
            $('#jstree').on('changed.jstree',function (e,data) {
                var i , j ,r = [];
                for(i=0,j = data.selected.length;i < j;i++){
                    r.push(data.instance.get_node(data.selected[i]).id);
                }
                $('.parent_id').val(r.join(', '));
            });
        </script>
   
@endpush
