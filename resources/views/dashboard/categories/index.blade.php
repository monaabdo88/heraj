@extends('dashboard.layouts.app')
@section('content')

<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.categories')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.categories')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
                    @endif  
                    <a href="{{route('categories.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('site.create')</a>
                    <a href="" class="btn btn-info edit_dep showbtn_control hidden" ><i class="fa fa-edit"></i></a>
                    <a href="" class="btn btn-primary showbtn_control archive_confirm hidden"><i class="fa fa-archive"></i></a>
                    <a href="" class="btn btn-danger showbtn_control hidden delete-confirm"><i class="fa fa-trash"></i></a>
                    <br/>
                    <div id="jstree"></div>
                    <input type="hidden" class="parent_id" value="" name="parent_id">
                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
@push('scripts')
        <script type="text/javascript">
            $(document).ready(function(){
                $('#jstree').jstree({
                    "core" : {
                        'data' : {!! load_cats(old('parent_id')) !!},
                        "themes" : {
                            "variant" : "large"
                        }
                    },
                    "checkbox" : {
                        "keep_selected_style" : true
                    },
                    "plugins" : [ "wholerow" ]
                });
            });
            $('#jstree').on('changed.jstree',function (e,data) {
                var i , j ,r = [];
                var name = [];
                for(i=0,j = data.selected.length;i < j;i++){
                    r.push(data.instance.get_node(data.selected[i]).id);
                    name.push(data.instance.get_node(data.selected[i]).text);
                }
                $("#form_Delete_department").attr('action','{{cp_url('categories')}}/'+r.join(', '));
                $("#dep_name").text(name.join(', '));
                $('.parent_id').val(r.join(', '));
                if(r.join(', ') != ''){
                    $('.showbtn_control').removeClass('hidden');
                    $('.edit_dep').attr('href','{{cp_url('categories')}}/'+r.join(', ')+'/edit');
                }
            });
            //confirm archive
            $('.archive_confirm').on('click',function (event) {
                event.preventDefault();
                 var cat_id = $('.parent_id').val()
                 const url = '{{url('cp/categories/archiveCategory')}}/'+cat_id;
                 swal({
                     title: '@lang('site.confirm_archive')',
                     text: '@lang('site.archive_msg')',
                     icon: 'warning',
                     buttons: ["@lang('site.no')", "@lang('site.yes')"],
                 }).then(function(value) {
                     if (value) {
                         window.location.href = url;
                     }
                     
                 });
             });
            // confirm delete
             $('.delete-confirm').on('click',function (event) {
                event.preventDefault();
                 var cat_id = $('.parent_id').val()
                 const url = '{{url('cp/categories/delCategory')}}/'+cat_id;
                 swal({
                     title: '@lang('site.confirm_delete')',
                     text: '@lang('site.delete_msg')',
                     icon: 'warning',
                     buttons: ["@lang('site.no')", "@lang('site.yes')"],
                 }).then(function(value) {
                     if (value) {
                         window.location.href = url;
                     }
                     
                 });
             });
             
         </script>
    @endpush