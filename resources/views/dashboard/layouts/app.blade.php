<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{config('app.name')}} </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {{--<!-- Bootstrap 3.3.7 -->--}}
    <link rel="stylesheet" href="{{asset('css/generalDashboard.css')}}">
    @toastr_css
    @if (app()->getLocale() == 'ar')
        <link rel="stylesheet" href="{{asset('css/rtlDashboard.css')}}">
        <style>
            body, h1, h2, h3, h4, h5, h6 {
                font-family: 'Cairo', sans-serif !important;
            }        
        </style>
    @else
        <link rel="stylesheet" href="{{asset('css/ltrDashboard.css')}}">
    @endif    
    
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

    <style>
        .mr-2{
            margin-right: 5px;
        }       
        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
            }
        }
        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .dt-buttons .btn{
            margin:15px 5px !important;
        }

    </style>
</head>        
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
    <div id="app">
        <!------------Header----------->
        @include('dashboard.inc.header')
        <!------------Sidebar----------->
        @include('dashboard.inc.sidebar')
        <!------------Content----------->
        @yield('content')
        <!------------Footer----------->
        @include('dashboard.inc.footer')
        
    </div>
</div><!-- end of wrapper -->
@jquery
<script src="{{asset('js/dashboard.js')}}"></script>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
@toastr_js
@toastr_render
<script src="{{asset('dashboard_files/jstree/jstree.js')}}"></script>
<script src="{{asset('dashboard_files/jstree/jstree.wholerow.js')}}"></script>
<script src="{{asset('dashboard_files/jstree/jstree.checkbox.js')}}"></script>
<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>

<script type="text/javascript">
            
            // preview image before upload
            function readURL(input) {
                //show image after upload
                $('#logo-img-tag').css('display','block');
                // upload image and preview
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#logo-img-tag').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            function readURLar(input) {
                //show image after upload
                $('#logo-img-tag_ar').css('display','block');
                // upload image and preview
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#logo-img-tag_ar').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            function readURL2(input) {
                //show image after upload
               $('#watermark-img-tag').css('display','block');
                
                // upload image and preview
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#watermark-img-tag').attr('src', e.target.result);    
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            // change and upload new image
            $("#logo_image").change(function(){
                readURL(this);
            });
            $("#logo_image_ar").change(function(){
                readURLar(this);
            });
            $("#watermark_image").change(function(){
                readURL2(this);
            });
            // hide image before upload
            var src = $('#logo-img-tag').attr('src');
            var src2 = $('#watermark-img-tag').attr('src');
            var src3 = $('#logo-img-tag_ar').attr('src');
            if(src==="#"){
                $('#logo-img-tag').css('display','none');
            }
            if(src3==="#"){
                $('#logo-img-tag_ar').css('display','none');
            }
           if(src2 === "#"){
            $('#watermark-img-tag').css('display','none');
           }
      </script>
     
    @stack('scripts')

</body>
</html>
