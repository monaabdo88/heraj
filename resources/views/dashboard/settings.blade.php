@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.settings')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.settings')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
                    @endif  
                    <form enctype="multipart/form-data" action="{{route('cp.updateSettings')}}" method="post">
                        @csrf
                    
                        <div class="form-group col-md-6">
                            <label>@lang('site.site_name_ar')</label>
                        <input type="text" class="form-control" name="site_name_ar" value="{{settings()->site_name_ar}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">@lang('site.site_name_en')</label>
                            <input type="text" class="form-control" name="site_name_en" value="{{settings()->site_name_en}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_email')</label>
                            <input type="text" class="form-control" name="site_email" value="{{settings()->site_email}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.phone')</label>
                            <input type="text" class="form-control" name="site_phone" value="{{settings()->site_phone}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_address_ar')</label>
                            <input type="text" class="form-control" name="site_address_ar" value="{{settings()->site_address_ar}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_address_en')</label>
                            <input type="text" class="form-control" name="site_address_en" value="{{settings()->site_address_en}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_description_ar')</label>
                            <textarea class="form-control" name="site_description_ar">{{settings()->site_description_ar}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_description_en')</label>
                            <textarea class="form-control" name="site_description_en">{{settings()->site_description_en}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_tags_ar')</label>
                            <textarea class="form-control" name="site_tags_ar">{{settings()->site_tags_ar}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_tags_en')</label>
                            <textarea class="form-control" name="site_tags_en">{{settings()->site_tags_en}}</textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="inputCity">@lang('site.status')</label>
                            <select id="inputState" class="form-control" name="site_status">
                            <option value="open" {{(settings()->site_status == 'open')? 'selected' : ''}}>Open</option>
                            <option value="close" {{(settings()->site_status == 'close')? 'selected' : ''}}>Close</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_text_close_msg_ar')</label>
                            <textarea class="form-control" name="site_close_msg_ar">{{settings()->site_close_msg_ar}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_text_close_msg_en')</label>
                            <textarea class="form-control" name="site_close_msg_en">{{settings()->site_close_msg_en}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_copyrights_ar')</label>
                            <textarea class="form-control" name="site_copyrights_ar">{{settings()->site_copyrights_ar}}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputAddress">@lang('site.site_copyrights_en')</label>
                            <textarea class="form-control" name="site_copyrights_en">{{settings()->site_copyrights_en}}</textarea>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Mail Driver</label>
                            <input type="text" class="form-control" name="mail_driver" value="{{settings()->mail_driver}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>Mail Host</label>
                            <input type="text" class="form-control" name="mail_host" value="{{settings()->mail_host}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>Mail Port</label>
                            <input type="text" class="form-control" name="mail_port" value="{{settings()->mail_port}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>Mail Username</label>
                            <input type="text" class="form-control" name="mail_username" value="{{settings()->mail_username}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>Mail Password</label>
                            <input type="password" class="form-control" name="mail_password" value="{{settings()->mail_password}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>Mail Encrypt Type</label>
                            <input type="text" class="form-control" name="mail_encrypt" value="{{settings()->mail_encrypt}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>SMS Server</label>
                            <input type="text" class="form-control" name="sms_server" value="{{settings()->sms_server}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>SMS Api Key</label>
                            <input type="text" class="form-control" name="sms_key" value="{{settings()->sms_key}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>SMS Secret Key</label>
                            <input type="password" class="form-control" name="sms_secret" value="{{settings()->sms_secret}}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>@lang('site.en.logo')</label>
                            <input type="file" id="logo_image" name="logo_en" class="form-control" />
                            <br>
                            <img src="{{(settings()->logo_en == 'no-img.png') ? '#' : url('uploads/images/'.settings()->logo_en)}}" id="logo-img-tag" width="119" height="50" class="img-responsive img-thumbnail" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>@lang('site.ar.logo')</label>
                            <input type="file" id="logo_image_ar" name="logo_ar" class="form-control" />
                            <br>
                            <img src="{{(settings()->logo_ar == 'no-img.png') ? '#' : url('uploads/images/'.settings()->logo_ar)}}" id="logo-img-tag_ar" width="119" height="50" class="img-responsive img-thumbnail" />
                        </div>
                        <div class="form-group col-md-4">
                            <label>@lang('site.watermark')</label>
                            <input type="file" id="watermark_image" name="watermark" class="form-control" />
                            <br>
                            <img src="{{(settings()->watermark == 'no-img.png') ? '#' : url('uploads/images/'.settings()->watermark)}}" id="watermark-img-tag" width="119" height="50" class="img-responsive img-thumbnail" />
                        </div>
                        <div class="clearfix"></div>
                        <button type="submit" class="btn btn-primary">@lang('site.update')</button>
                    </form>
                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection