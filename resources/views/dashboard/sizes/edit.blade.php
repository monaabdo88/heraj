@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.sizes')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.sizes')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('sizes.update',$size->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                           <div class="form-group col-md-6">
                                <label>@lang('site.title')</label>
                                <input type="text" name="title" class="form-control" value="{{$size->title }}">
                            </div>
                            
                        
                        
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary"> @lang('site.update')</button>
                        </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
