@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.users')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.users')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('users.update',$user->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                        <div class="form-group col-md-12">
                            <label>@lang('site.name')</label>
                            <input type="text" name="name" class="form-control" value="{{$user->name}}" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.email')</label>
                            <input type="email" name="email" class="form-control" value="{{$user->email}}" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.show_email')</label><br>
                            <input type="radio" name="show_email" value="0" {{($user->show_email == 0)? 'checked' :''}}> @lang('site.no')
                            <input type="radio" name="show_email" value="1" {{($user->show_email == 1)? 'checked' :''}}> @lang('site.yes')
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.password')</label>
                            <input type="password" name="password" class="form-control"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.password_confirm')</label>
                            <input type="password" name="password_confirmation" class="form-control" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.phone')</label>
                        <input type="text" name="phone" class="form-control" value="{{$user->phone}}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.show_phone')</label><br>
                            <input type="radio" name="show_phone" value="0" {{($user->show_phone == 0)? 'checked' :''}}> @lang('site.no')
                            <input type="radio" name="show_phone" value="1" {{($user->show_phone == 1)? 'checked' :''}}> @lang('site.yes')
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.gender')</label>
                            <select class="form-control" name="gender">
                                <option value="female" {{($user->gender == 'female')? 'selected' :''}}>@lang('site.female')</option>
                                <option value="male" {{($user->gender == 'male')? 'selected' :''}}>@lang('site.male')</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.country_name')</label>
                            <select name="country_id" class="form-control" id="country">
                                @foreach ($countries as $country)

                                    <option value="{{$country->id}}" {{($user->country_id == $country->id)? 'selected' :''}}>{{$country->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.city')</label>
                            <select class="form-control" name="city_id" id="city">
                                @foreach ($cities as $city)

                                    <option value="{{$city->id}}" {{($user->city_id == $city->id)? 'selected' :''}}>{{$city->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.facebook_link')</label>
                        <input type="text" name="facebook_link" class="form-control" value="{{$user->facbook_link}}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.twitter_link')</label>
                            <input type="text" name="twitter_link" class="form-control" value="{{$user->twitter_link}}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.insatgram_link')</label>
                            <input type="text" name="instagram_link" class="form-control" value="{{$user->instagram_link}}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.website_link')</label>
                            <input type="text" name="website_link" class="form-control" value="{{$user->website_link}}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.special')</label><br>
                            <input type="radio" name="special" value="0" {{($user->special == 0)? 'checked' :''}}> @lang('site.no')
                            <input type="radio" name="special" value="1" {{($user->special == 1)? 'checked' :''}}> @lang('site.yes')
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.status')</label>
                            <select class="form-control" name="status">
                                <option value="1" {{($user->status == '1')? 'selected' :''}}>@lang('site.active')</option>
                                <option value="0" {{($user->status == '0')? 'selected' :''}}>@lang('site.not-active')</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.image')</label>
                            <input type="file" id="logo_image" name="user_img" class="form-control"/>
                            <br>
                            <img src="{{url('uploads/users/'.$user->user_img)}}" id="logo-img-tag" class="img-responsive img-thumbnail" />
                        
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> @lang('site.update')</button>
                        </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
@push('scripts')
    <script>
        $(function(){
            $("#country").on('change',function(){
                var country_id = $(this).val();
                var url ='{{url('get_cities')}}'+'/'+country_id;
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(data){
                        $("#city").html(data)
                    }
                });
                
            });

        });
    </script>
@endpush    