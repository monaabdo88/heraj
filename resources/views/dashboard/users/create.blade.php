@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.users')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.users')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group col-md-12">
                            <label>@lang('site.name')</label>
                            <input type="text" name="name" class="form-control" value="{{old('name')}}" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.email')</label>
                            <input type="email" name="email" class="form-control" value="{{old('email')}}" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.show_email')</label><br>
                            <input type="radio" name="show_email" value="0"> @lang('site.no')
                            <input type="radio" name="show_email" value="1"> @lang('site.yes')
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.password')</label>
                            <input type="password" name="password" class="form-control"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.password_confirm')</label>
                            <input type="password" name="password_confirmation" class="form-control" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.phone')</label>
                            <input type="text" name="phone" class="form-control" value="{{old('phone')}}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.show_phone')</label><br>
                            <input type="radio" name="show_phone" value="0"> @lang('site.no')
                            <input type="radio" name="show_phone" value="1"> @lang('site.yes')
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.gender')</label>
                            <select class="form-control" name="gender">
                                <option value="female">@lang('site.female')</option>
                                <option value="male">@lang('site.male')</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.country_name')</label>
                            <select name="country_id" class="form-control" id="country">
                                    <option value="">@lang('site.selecte_country')</option>
                                @foreach ($countries as $country)
                                    <option value="{{$country->id}}">{{$country->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.city')</label>
                            <select class="form-control" name="city_id" id="city"></select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.facebook_link')</label>
                            <input type="text" name="facebook_link" class="form-control" value="{{old('facebook_link')}}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.twitter_link')</label>
                            <input type="text" name="twitter_link" class="form-control" value="{{old('twitter.link')}}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.insatgram_link')</label>
                            <input type="text" name="instagram_link" class="form-control" value="{{old('instagram.link')}}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.website_link')</label>
                            <input type="text" name="website_link" class="form-control" value="{{old('website.link')}}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.special')</label><br>
                            <input type="radio" name="special" value="0"> @lang('site.no')
                            <input type="radio" name="special" value="1"> @lang('site.yes')
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.status')</label>
                            <select class="form-control" name="status">
                                <option value="1">@lang('site.active')</option>
                                <option value="0">@lang('site.not-active')</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.image')</label>
                            <input type="file" id="logo_image" name="user_img" class="form-control"/>
                            <br>
                            <img src="#" id="logo-img-tag" class="img-responsive img-thumbnail" />
                        
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                        </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
@push('scripts')
    <script>
        $(function(){
            $("#country").on('change',function(){
                var country_id = $(this).val();
                var lang = '{{app()->getLocale()}}';
                var url ='{{url('get_cities')}}'+'/'+country_id+'/'+lang;
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(data){
                        $("#city").html(data);
                        
                    }
                });
                
            });

        });
    </script>
@endpush    