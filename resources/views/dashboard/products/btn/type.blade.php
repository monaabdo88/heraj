<input data-id="{{$id}}" class="toggle-class-type-{{$id}}" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="@lang('site.special')" data-off="@lang('site.not-special')" {{ $special== 1 ? 'checked' : '' }}>

<script>
    $(function() {
      $(".toggle-class-type-{{$id}}").change(function() {
          var type = $(this).prop('checked') == true ? 1 : 0; 
          var id = $(this).data('id'); 
          var url ='{{url('cp/products/changeType')}}';
          var baseUrl = url+'/'+id+'/'+type;
          $.ajax({
              type: "GET",
              url: url,
              data: {'type': type, 'id': id},
              success: function(data){
                toastr.success("", data.success);
              }
          });
      })
    })
  </script>