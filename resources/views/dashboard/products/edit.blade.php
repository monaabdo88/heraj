@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">
    <?php
    $lat = $product->lat;
    $lng = $product->lng;
    ?>
    <section class="content-header">

        <h1>@lang('site.products')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.products')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('products.update',$product->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                        @foreach (config('translatable.locales') as $locale)
                            <div class="form-group col-md-12">
                                <label>@lang('site.' . $locale . '.title')</label>
                                <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{ $product->translate($locale)->title }}">
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label>@lang('site.' . $locale .'.content')</label>
                            <textarea class="form-control" name="{{ $locale }}[description]">{!!$product->translate($locale)->description!!}</textarea>
                            </div>
                        @endforeach
                        <div class="form-group col-md-6">
                            <label>@lang('site.country_name')</label>
                            <select name="country_id" class="form-control" id="country">
                                @foreach ($countries as $country)
                                    <option value="{{$country->id}}" {{($product->country_id == $country->id)? 'selected' :''}}>{{$country->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.city')</label>
                            <select class="form-control" name="city_id" id="city">
                                @foreach ($cities as $city)

                                    <option value="{{$city->id}}" {{($product->city_id == $city->id)? 'selected' :''}}>{{$city->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.status')</label>
                            <select class="form-control" name="status">
                                <option value="1" {{($product->status == 1)? 'selected' :''}}>@lang('site.active')</option>
                                <option value="0" {{($product->status == 0)? 'selected' :''}}>@lang('site.not-active')</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.author')</label>
                            <select name="user_id" class="form-control">
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}" {{($product->user_id == $user->id)? 'selected' :''}}>{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.category')</label>
                            <div id="jstree"></div>
                            <input type="hidden" class="parent_id" value="{{old('category_id')}}" name="category_id">
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.map_address')</label>
                            <div id="us1" style="width: 100%; height: 400px;"></div>
                            <input type="hidden" value="{{ $lat }}" id="lat" name="lat">
                            <input type="hidden" value="{{ $lng }}" id="lng" name="lng">
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.special')</label><br>
                            <div class="col-md-6">
                                <input type="radio" name="special" value="0" {{($product->special == 0)? 'checked' :''}}> @lang('site.no')
                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="special" value="1" {{($product->special == 1)? 'checked' :''}}> @lang('site.yes')
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.ad_type')</label><br>
                            <div class="col-md-3">
                                <input type="radio" name="product_type" value="rent" {{($product->product_type == 'rent')? 'checked' :''}}/> @lang('site.rent')
                            
                            </div>
                            <div class="col-md-3">
                                <input type="radio" name="product_type" value="sale" {{($product->product_type == 'sale')? 'checked' :''}}/> @lang('site.sale')
                            </div>
                            <div class="col-md-3">
                                <input type="radio" name="product_type" value="exchange" {{($product->product_type == 'exchange')? 'checked' :''}}/> @lang('site.exchange')
                            
                            </div>
                            <div class="col-md-3">
                                <input type="radio" name="product_type" value="services" {{($product->product_type == 'services')? 'checked' :''}}/> @lang('site.exchange')
                        
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.phone')</label>
                            <input type="text" name="phone" class="form-control" value="{{$product->phone}}"/>
                        </div>
                        <div class="form-group col-md-6">
                            <label>@lang('site.price')</label>
                            <input type="text" name="price" class="form-control" value="{{$product->price}}" />
                        </div>
                        <div class="form-group col-md-12">
                            <label for="">@lang('site.colors')</label>
                            <div class="clearfix"></div>
                            @foreach ($colors as $item)
                            <div class="col-md-2">
                                <input type="checkbox" name="color_id[]" value="{{$item->id}}" 
                                @if(isset($product->colors))
                                    @foreach($product->colors as $t)
                                        @if($t->id == $item->id)
                                            checked
                                        @endif
                                    @endforeach
                                @endif
                                />
                                <div style="width:100%;height:15px;background:{{$item->title}}"></div>
                            </div>
                               
                            @endforeach
                        </div>
                        <div class="form-group col-md-12">
                            <label for="">@lang('site.sizes')</label>
                            <div class="clearfix"></div>
                            @foreach ($sizes as $item)
                            <div class="col-md-2">
                                <input type="checkbox" name="size_id[]" value="{{$item->id}}" 
                                @if(isset($product->sizes))
                                    @foreach($product->sizes as $t)
                                        @if($t->id == $item->id)
                                            checked
                                        @endif
                                    @endforeach
                                @endif
                                />
                                {{$item->title}} 
                            </div>
                            @endforeach
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.tags')</label><br>
                            @foreach($tags as $tag)
                            <div class="col-md-2">
                                <input type="checkbox" name="tag_id[]" value="{{$tag->id}}" 
                                @if(isset($product->tags))
                                     @foreach($product->tags as $t)
                                         @if($t->id == $tag->id)
                                             checked
                                         @endif
                                     @endforeach
                                 @endif
                             > {{$tag->title}} 
                            </div>
                            
                            @endforeach
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.main_image')</label>
                            <input type="file" id="logo_image" class="form-control" name="main_image" />
                            <br>
                            <img src="{{url('uploads/products/'.$product->main_image)}}" id="logo-img-tag" class="img-responsive img-thumbnail" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.media')</label>
                            <div class="needsclick dropzone" id="document-dropzone">

                            </div>
                           
                        </div>
                        
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> @lang('site.update')</button>
                        </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
@push('scripts')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script src="{{asset('js/locationpicker.jquery.js')}}"></script>   
<script>
        //Ckeditor
        CKEDITOR.replace( "ar[description]" ,{
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            
        });
        CKEDITOR.replace( "en[description]" ,{
            filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            
        });
        $(function(){
            $("#country").on('change',function(){
                var country_id = $(this).val();
                var lang = '{{app()->getLocale()}}';
                var url ='{{url('get_cities')}}'+'/'+country_id+'/'+lang;
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(data){
                        $("#city").html(data);
                        
                    }
                });
                
            });

        });
        // Categories
        $(document).ready(function(){
            $('#jstree').jstree({
                "core" : {
                    'data' : {!! load_cats($product->category_id) !!},
                    "themes" : {
                        "variant" : "large"
                    }
                },
                "checkbox" : {
                    "keep_selected_style" : false
                },
                "plugins" : [ "wholerow" ]
                });
        });
            $('#jstree').on('changed.jstree',function (e,data) {
                var i , j ,r = [];
                for(i=0,j = data.selected.length;i < j;i++){
                    r.push(data.instance.get_node(data.selected[i]).id);
                }
                $('.parent_id').val(r.join(', '));
            });
        //Location Map
        $('#us1').locationpicker({
        location: {
            latitude: {{ $lat }},
            longitude:{{ $lng }}
        },
        radius: 300,
            markerIcon: '{{ asset('img/map-marker-2-xl.png') }}',
            inputBinding: {
                latitudeInput: $('#lat'),
                longitudeInput: $('#lng'),
        }
        });
        //Dropzone
        
        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('products.storeMedia') }}',
            paramName:'file',
            uploadMultiple:false,
            maxFiles:15,
            parallelUploads: 15, //all images should upload same time
            maxFiles: 15, //number of images a user should upload at an instance
            addRemoveLinks: true,
            dictDefaultMessage:"{{trans('site.drag_drop')}}",
            dictRemoveFile:"{{ trans('site.delete') }} ",
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $.ajax({
                    headers:{
                        'X-CSRF-Token':$('input[name="_token"]').val()
                    }, //passes the current token of the page to image url
                    type: 'GET',
                    url: '{{ url("cp/products/deleteImag/") }}'+'/'+name,
                    dataType: 'json',
                    success: function (data){
                        console.log("File deleted successfully!! "+data);
                    },
                    error: function(e) {
                        console.log('this is '+e.data);
                    }});
                $('form').find('input[name="document[]"][value="' + name + '"]').remove()
            },
            init: function () {
                @if(isset($media) && $media)
                    @foreach($media as $item)
                        var image = {!! json_encode($item->getFullUrl()) !!}
                        var file = {!! json_encode($item) !!}
                        this.options.addedfile.call(this, file)
                        this.options.thumbnail.call(this, file, image)
                        file.previewElement.classList.add('dz-complete')
                        $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
                    
                    @endforeach
                @endif
                
            }
        }

    </script>
    <style type="text/css">
        .dz-image img {
            width: 120px;
            height: 120px;
    
        }
    
        </style>
@endpush    