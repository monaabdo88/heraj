<!-- Trigger the modal with a button -->
<a href="{{url('cp/socials/delSocial/'.$id)}}" class="btn btn-danger delete-confirm"><i class="fa fa-trash"></i></a>
<script type="text/javascript">
    // confirm delete
     $('.delete-confirm').on('click',function (event) {
         event.preventDefault();
         const url = $(this).attr('href');
         swal({
             title: '@lang('site.confirm_delete')',
             text: '@lang('site.delete_msg')',
             icon: 'warning',
             buttons: ["@lang('site.no')", "@lang('site.yes')"],
         }).then(function(value) {
             if (value) {
                 window.location.href = url;
             }
             
         });
     });
     
 </script>

                                