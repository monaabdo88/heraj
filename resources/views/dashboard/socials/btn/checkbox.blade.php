<input type="checkbox" name="item[]" class="checkItem" value="{{ $id }}">
<script type="text/javascript">
    $('.confirm_all').attr("disabled", true);
    $('.archive_all').attr("disabled", true);
    $('.restore_all').attr("disabled", true);

    // enable check all items
    $('.selectAll').click(function () {    
         $(':checkbox.checkItem').prop('checked', this.checked);    
     }); 
     
     $('input:checkbox').change(function () {
         $('.confirm_all').prop('disabled', $('input.checkItem:checked').length == 0)
         $('.archive_all').prop('disabled', $('input.checkItem:checked').length == 0)
         $('.restore_all').prop('disabled', $('input.checkItem:checked').length == 0)

     })
     //confirm before archive
     $('.archive_all').click(function () {
         event.preventDefault();
         var ids = [];
         swal({
             title: '@lang('site.confirm_archive')',
             text: '@lang('site.archive_msg')',
             icon: 'warning',
             buttons: ["@lang('site.no')", "@lang('site.yes')"],
         }).then(function(value) {
             if (value) {
                 $.each($('.checkItem:checked'),function(){ 
                     if($(this).val() != 0)
                     ids.push($(this).val()); 
                 });
                 window.location.href = "{{url('cp/socials/archiveAll')}}"+'/'+ids;
             }
         });
     });
     //confirm before restore All
    $('.restore_all').click(function () {
         event.preventDefault();
         var ids = [];
         swal({
             title: '@lang('site.confirm_restore')',
             text: '@lang('site.restore_msg')',
             icon: 'warning',
             buttons: ["@lang('site.no')", "@lang('site.yes')"],
         }).then(function(value) {
             if (value) {
                 $.each($('.checkItem:checked'),function(){ 
                     if($(this).val() != 0)
                     ids.push($(this).val()); 
                 });
                 window.location.href = "{{url('cp/socials/restoreAll')}}"+'/'+ids;
             }
         });
     }); 
     // confirm before delete All
     $('.confirm_all').click(function () {
         event.preventDefault();
         var ids = [];
         swal({
             title: '@lang('site.confirm_delete')',
             text: '@lang('site.delete_msg')',
             icon: 'warning',
             buttons: ["@lang('site.no')", "@lang('site.yes')"],
         }).then(function(value) {
             if (value) {
                 $.each($('.checkItem:checked'),function(){ 
                     if($(this).val() != 0)
                     ids.push($(this).val()); 
                 });
                 window.location.href = "{{url('cp/socials/delAll')}}"+'/'+ids;
             }
         });
     });

</script>
