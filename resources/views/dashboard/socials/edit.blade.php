@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

    <h1>@lang('site.edit') {{$social->title}}</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.socials')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('socials.update',$social->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                        @foreach (config('translatable.locales') as $locale)
                            <div class="form-group col-md-6">
                                <label>@lang('site.' . $locale . '.title')</label>
                                <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{$social->translate($locale)->title }}">
                            </div>
                       @endforeach
                        <div class="form-group col-md-6">
                            <label>@lang('site.link')</label>
                            <input type="text" name="link" class="form-control" value="{{ $social->link }}">
                        </div>
                         
                            <div class="form-group col-md-6">
                                <label>@lang('site.image')</label>
                                <input type="file" id="logo_image" name="icon" class="form-control"/>
                                <br>
                            <img src="{{url('uploads/icons/'.$social->icon)}}" id="logo-img-tag" class="img-responsive img-thumbnail" />
                            
                            </div>
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-pencil"></i> @lang('site.update')</button>
                    </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
