<input data-id="{{$id}}" class="toggle-class-{{$id}}" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="@lang('site.active')" data-off="@lang('site.not-active')" {{ $status== 1 ? 'checked' : '' }}>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
    $(function() {
      $(".toggle-class-{{$id}}").change(function() {
          var status = $(this).prop('checked') == true ? 1 : 0; 
          var id = $(this).data('id'); 
          var url ='{{url('cp/ads/changeStatus')}}';
          //var baseUrl = url+'/'+id+'/'+status;
          $.ajax({
            type: 'GET',
            url: url,
            data: { 'status': status, 'id': id},
            success: function(data){
              toastr.success("", data.success);
            }
        });
      })
    })
  </script>
  