@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.google_ads')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.google_ads')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                   {{$dataTable->table()}}

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
@push('scripts')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    {{$dataTable->scripts()}}
    
@endpush  