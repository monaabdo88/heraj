@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.google_ads')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.google_ads')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('ads.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                        @foreach (config('translatable.locales') as $locale)
                            <div class="form-group col-md-6">
                                <label>@lang('site.' . $locale . '.title')</label>
                                <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{ old($locale . '.title') }}">
                            </div>
                            
                        @endforeach
                            <div class="form-group col-md-6">
                                <label>@lang('site.ad_position')</label>
                                <select class="form-control" name="position">
                                    <option value="inside-home">@lang('site.inside-home')</option>
                                    <option value="sidebar-page">@lang('site.sidebar-page')</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>@lang('site.link')</label>
                                <input type="text" class="form-control" name="link" value="{{old('link')}}"/>
                            </div>
                            <div class="form-group col-md-12">
                                <label>@lang('site.ad_code')</label>
                                <textarea class="form-control" name="code">{{old('code')}}</textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label>@lang('site.status')</label>
                                <select class="form-control" name="status">
                                    <option value="0">@lang('site.not-active')</option>
                                    <option value="1">@lang('site.active')</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>@lang('site.image')</label>
                                <input type="file" id="logo_image" name="image" class="form-control"/>
                                <br>
                                <img src="#" id="logo-img-tag" class="img-responsive img-thumbnail" />
                            
                            </div>
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                    </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
