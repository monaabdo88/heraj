@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.operations')</h1>

        <ol class="breadcrumb">
            <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.operations')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
                    @endif  
                    <div class="clearfix"></div>
                    <table id="example" class="operations table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th><input type="checkbox" class="selectAll" /></th>
                                <th scope="col">#</th>
                                <th scope="col">@lang('site.section')</th>
                                <th>@lang('site.title')</th>
                                <th scope="col">@lang('site.type')</th>
                                <th scope="col">@lang('site.user')</th>
                                <th scope="col">@lang('site.created_at')</th>
                                <th>@lang('site.delete')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($operations as $item)
                                <tr>
                                    <td><input type="checkbox" name="item[]" class="checkItem" value="{{ $item->id }}"></td>
                                    <td>{{$item->id}}</td>
                                    <td> <a href="{{url('cp/showOperations/'.$item->section)}}">{{__('site.'.$item->section)}}</a></td>
                                    <td>
                                       {{($item->section == 'settings')? __('site.settings'): $item->title}}
                                    </td>
                                    <td><button class="btn 
                                        @if($item->type == 'add')
                                            btn-success
                                        @elseif($item->type == 'update')
                                            btn-primary
                                        @elseif($item->type == 'archive')
                                            btn-warning
                                        @elseif($item->type == 'delete')
                                            btn-danger
                                        @else
                                            btn-info
                                        @endif
                                        ">{{__('site.'.$item->type)}}</button></td>
                                    <td><a href="{{url('cp/showAdminOperations/'.$item->admin_id)}}">{{\App\Models\Admin::where('id',$item->admin_id)->first()->name}}</a></td>
                                    <td>{{$item->created_at}}</td>
                                    <td>
                                        <a href="{{url('cp/delOperation/'.$item->id)}}" class="btn btn-danger delete-confirm"><i class="fa fa-trash"></i></a>
                                        
                                        
                                    </td>
                                </tr>
                            @endforeach
                            
                          </tbody>
                        
                    </table>
                    <button class="btn btn-danger float-right confirm_all"><i class="fa fa-trash"></i></button>
                    
                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
@push('scripts')
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    } );
    

    </script>
    <script type="text/javascript">
        $('.confirm_all').attr("disabled", true);

                // enable check all items
                $('.selectAll').click(function () {    
                    $(':checkbox.checkItem').prop('checked', this.checked);    
                }); 
                
                $('input:checkbox').change(function () {
                    $('.confirm_all').prop('disabled', $('input.checkItem:checked').length == 0)
                })
                // confirm before delete All
                $('.confirm_all').click(function () {
                    event.preventDefault();
                    var ids = [];
                    swal({
                        title: '@lang('site.confirm_delete')',
                        text: '@lang('site.delete_msg')',
                        icon: 'warning',
                        buttons: ["@lang('site.no')", "@lang('site.yes')"],
                    }).then(function(value) {
                        if (value) {
                            $.each($('.checkItem:checked'),function(){ 
                                if($(this).val() != 0)
                                ids.push($(this).val()); 
                            });
                            window.location.href = "{{url('cp/delOperation')}}"+'/'+ids;
                        }
                    });
                });
            // confirm delete
             $('.delete-confirm').on('click',function (event) {
                 event.preventDefault();
                 const url = $(this).attr('href');
                 swal({
                     title: '@lang('site.confirm_delete')',
                     text: '@lang('site.delete_msg')',
                     icon: 'warning',
                     buttons: ["@lang('site.no')", "@lang('site.yes')"],
                 }).then(function(value) {
                     if (value) {
                         window.location.href = url;
                     }
                     
                 });
             });
             
         </script>
@endpush