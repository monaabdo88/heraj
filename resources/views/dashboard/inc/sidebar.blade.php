<aside class="main-sidebar">

    <section class="sidebar">

        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('uploads/admins/'.Auth::guard('admin')->user()->image)}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::guard('admin')->user()->name}} </p>
                <a href="#"><i class="fa fa-circle text-success"></i> @lang('site.online')</a>
            </div>
        </div>

        <ul class="sidebar-menu" data-widget="tree">
            <li class="{{active_link('')}}"><a href="{{ route('cp.index') }}"><i class="fa fa-th"></i><span>@lang('site.dashboard')</span></a></li>
            <li class="{{active_link('settings')}}"><a href="{{ route('cp.getSettings') }}"><i class="fa fa-cog"></i><span>@lang('site.settings')</span></a></li>
            <li class="{{active_link('countries')}}"><a href="{{ url('cp/countries') }}"><i class="fa fa-flag"></i><span>@lang('site.countries')</span></a></li>
            <li class="{{active_link('cities')}}"><a href="{{ url('cp/cities') }}"><i class="fa fa-th"></i><span>@lang('site.cities')</span></a></li>
            <li class="{{active_link('users')}}"><a href="{{ url('cp/users') }}"><i class="fa fa-users"></i><span>@lang('site.users')</span></a></li>
            <li class="{{active_link('sliders')}}"><a href="{{ url('cp/sliders') }}"><i class="fa fa-image"></i><span>@lang('site.sliders')</span></a></li>
            <li class="{{active_link('ads')}}"><a href="{{ url('cp/ads') }}"><i class="fa fa-adn"></i><span>@lang('site.google_ads')</span></a></li>
            <li class="{{active_link('socials')}}"><a href="{{ url('cp/socials') }}"><i class="fa fa-share-alt"></i><span>@lang('site.socials')</span></a></li>
            <li class="{{active_link('pages')}}"><a href="{{ url('cp/pages') }}"><i class="fa fa-file"></i><span>@lang('site.pages')</span></a></li>
            <li class="{{active_link('tags')}}"><a href="{{ url('cp/tags') }}"><i class="fa fa-cog"></i><span>@lang('site.tags')</span></a></li>
            <li class="{{active_link('colors')}}"><a href="{{ url('cp/colors') }}"><i class="fa fa-paint-brush"></i><span>@lang('site.colors')</span></a></li>
            <li class="{{active_link('sizes')}}"><a href="{{ url('cp/sizes') }}"><i class="fa fa-scissors"></i><span>@lang('site.sizes')</span></a></li>
            
            <li class="{{active_link('categories')}}"><a href="{{ url('cp/categories') }}"><i class="fa fa-cubes"></i><span>@lang('site.categories')</span></a></>
            <li class="{{active_link('products')}}"><a href="{{ url('cp/products') }}"><i class="fa fa-cart-plus"></i><span>@lang('site.products')</span></a></li>
            <li class="{{active_link('alerts')}}"><a href="{{ url('cp/alerts') }}"><i class="fa fa-exclamation-triangle"></i><span>@lang('site.alerts')</span></a></li>
            <li class="{{active_link('mail_list')}}"><a href="{{ url('cp/mail_list') }}"><i class="fa fa-envelope"></i><span>@lang('site.mail_list')</span></a></li>
            <li><a href=""><i class="fa fa-phone-square"></i>@lang('site.customer_support_msg')</a></li>
            <li class="{{active_link('admins')}}" style="height: auto;"><a href="{{url('cp/admins')}}"><i class="fa fa-lock"></i> <span>@lang('site.admins')</span></a></li>
            <li class="{{active_link('operations')}}"><a href="{{url('cp/operations')}}"><i class="fa fa-tasks"></i>@lang('site.operations')</a></li>
            <li class="{{active_link('archive')}} treeview" style="height: auto;">
                <a href="#"><i class="fa fa-archive"></i>@lang('site.archive')<span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span></a>
                  <ul class="treeview-menu" style="">
                    <li class="{{active_link('countries_archive')}}"><a href="{{ url('cp/countries_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.countries')</span></a></li>
                    <li class="{{active_link('cities_archive')}}"><a href="{{ url('cp/cities_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.cities')</span></a></li>
                    <li class="{{active_link('users_archive')}}"><a href="{{ url('cp/users_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.users')</span></a></li>
                    <li class="{{active_link('sliders_archive')}}"><a href="{{ url('cp/sliders_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.sliders')</span></a></li>
                    <li class="{{active_link('ads_archive')}}"><a href="{{ url('cp/ads_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.google_ads')</span></a></li>
                    <li class="{{active_link('socials_archive')}}"><a href="{{ url('cp/socials_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.socials')</span></a></li>
                    <li class="{{active_link('pages_archive')}}"><a href="{{ url('cp/pages_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.pages')</span></a></li>
                    <li class="{{active_link('tags_archive')}}"><a href="{{ url('cp/tags_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.tags')</span></a></li>
                    <li class="{{active_link('colors_archive')}}"><a href="{{ url('cp/colors_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.colors')</span></a></li>
                    <li class="{{active_link('sizes_archive')}}"><a href="{{ url('cp/sizes_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.sizes')</span></a></li>
                    <li class="{{active_link('categories_archive')}}"><a href="{{ url('cp/categories_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.categories')</span></a></li>
                    <li class="{{active_link('products_archive')}}"><a href="{{ url('cp/products_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.products')</span></a></li>
                    <li class="{{active_link('alerts_archive')}}"><a href="{{ url('cp/alerts_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.alerts')</span></a></li>
                    <li class="{{active_link('mail_list_archive')}}"><a href="{{ url('cp/mail_list_archive') }}"><i class="fa fa-circle-o"></i><span>@lang('site.mail_list')</span></a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i>@lang('site.customer_support_msg')</a></li>
                    <li class="{{active_link('admins_archive')}}" style="height: auto;"><a href="{{url('cp/admins_archive')}}"><i class="fa fa-circle-o"></i> <span>@lang('site.admins')</span></a></li>
                  </ul>
            </li>
        
        </ul>

    </section>

</aside>

