<footer class="main-footer">
    <p>
        <b>
            @if (app()->getLocale() == 'ar')
                {!!settings()->site_copyrights_ar!!}
            @else
                {!! settings()->site_copyrights_en !!}
            @endif
        </b>
    </p>
    
</footer>