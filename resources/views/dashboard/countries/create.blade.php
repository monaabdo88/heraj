@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.countries')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.countries')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('countries.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                        @foreach (config('translatable.locales') as $locale)
                            <div class="form-group col-md-6">
                                <label>@lang('site.' . $locale . '.title')</label>
                                <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{ old($locale . '.title') }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label>@lang('site.' . $locale . '.currency')</label>
                                <input type="text" name="{{ $locale }}[currency]" class="form-control" value="{{ old($locale . '.currency') }}">
                            </div>
                            
                        @endforeach
                        <div class="form-group col-md-6">
                            <label>@lang('site.country_code')</label>
                            <input type="text" name="code" class="form-control" value="{{ old( 'code') }}">
                        </div>
                            
                            <div class="form-group col-md-6">
                                <label>@lang('site.image')</label>
                                <input type="file" id="logo_image" name="flage" class="form-control"/>
                                <br>
                                <img src="#" id="logo-img-tag" class="img-responsive img-thumbnail" />
                            
                            </div>
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                    </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
