<!-- Trigger the modal with a button -->
<button class="btn btn-warning move_to_archive" id="{{$id}}"><i class="fa fa-archive"></i></button>
<script type="text/javascript">
    $('.move_to_archive').click(function () {
         event.preventDefault();
         var ids = $(this).attr('id');
         swal({
             title: '@lang('site.confirm_archive')',
             text: '@lang('site.archive_msg')',
             icon: 'warning',
             buttons: ["@lang('site.no')", "@lang('site.yes')"],
         }).then(function(value) {
             if (value) {
                
                 window.location.href = "{{url('cp/countries/archiveAll')}}"+'/'+ids;
             }
         });
     }); 
     
 </script>

                                