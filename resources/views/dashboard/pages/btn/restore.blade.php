<!-- Trigger the modal with a button -->
<button class="btn btn-warning restore" id="{{$id}}"><i class="fa fa-reply"></i></button>
<script type="text/javascript">
    $('.restore').click(function () {
         event.preventDefault();
         var ids = $(this).attr('id');
         swal({
             title: '@lang('site.confirm_restore')',
             text: '@lang('site.restore_msg')',
             icon: 'warning',
             buttons: ["@lang('site.no')", "@lang('site.yes')"],
         }).then(function(value) {
             if (value) {                
                 window.location.href = "{{url('cp/pages/restoreAll')}}"+'/'+ids;
             }
         });
     }); 
     
 </script>

                                