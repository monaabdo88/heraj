@extends('dashboard.layouts.app')
@section('content')
<div class="content-wrapper">

    <section class="content-header">

        <h1>@lang('site.pages')</h1>

        <ol class="breadcrumb">
        <li><a href="{{url('cp')}}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active"><i class="fa fa-cog"></i>@lang('site.pages')</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                <div class="box-body">
                    @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form id="file-upload-form" class="uploader" action="{{route('pages.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                        @foreach (config('translatable.locales') as $locale)
                            <div class="form-group col-md-12">
                                <label>@lang('site.' . $locale . '.title')</label>
                                <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{ old($locale . '.title') }}">
                            </div>
                            
                            <div class="form-group col-md-12">
                                <label>@lang('site.' . $locale .'.content')</label>
                                <textarea class="form-control" name="{{ $locale }}[content]"></textarea>
                            </div>
                            
                        @endforeach
                        <div class="form-group col-md-12">
                            <label>@lang('site.place')</label><br>
                            <input type="radio" name="place" value="nav" /> @lang('site.nav')
                            <input type="radio" name="place" value="footer" /> @lang('site.footer')
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.status')</label>
                        
                                <select class="form-control" name="status">
                                    <option value="0">@lang('site.not-active')</option>
                                    <option value="1">@lang('site.active')</option>
                                </select>
                            
                            
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                        </div>

                </form>

                </div>
            </div>
            </div>
        </div><!-- end of row -->

        
    </section><!-- end of content -->

</div><!-- end of content wrapper -->
  
@endsection
@push('scripts')
<script>
    CKEDITOR.replace( "ar[content]" ,{
        filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        
    });
    CKEDITOR.replace( "en[content]" ,{
        filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        
    });
</script>
@endpush
