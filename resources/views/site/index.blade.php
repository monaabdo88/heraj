@extends('site.layouts.app')
@section('content')
        <!-- Hero Section Begin -->
        <section class="hero">
            <div class="container">
                <div class="row">
                    @include('site.inc.search')
                    @include('site/inc/slider')
                </div>
            </div>
        </section>
        <!-- Hero Section End -->
    
        <!-- Categories Section Begin -->
        <section class="categories">
            <div class="container">
                <div class="row">
                    <div class="categories__slider owl-carousel">
                        @foreach ($all_cats as $item)
                            <div class="col-lg-3">
                                @if($item->icon == NULL)
                                    @php
                                        $cat_image = 'image.png';   
                                    @endphp
                                @else
                                    @php
                                        $cat_image = $item->icon;   
                                    @endphp
                                @endif
                                <div class="categories__item set-bg" data-setbg="{{asset('uploads/categories/'.$cat_image)}}">
                                    <h5><a href="{{route('categories.show',$item->id)}}">{{$item->title}}</a></h5>
                                </div>
                            </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- Categories Section End -->
    
        <!-- Featured Section Begin -->
        <section class="featured spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2>@lang('site.feature_products')</h2>
                        </div>
                        <div class="featured__controls">
                            <ul>
                                <li class="active" data-filter="*">@lang('site.all')</li>
                                <li data-filter=".sale">@lang('site.sale')</li>
                                <li data-filter=".rent">@lang('site.rent')</li>
                                <li data-filter=".exchange">@lang('site.exchange')</li>
                                <li data-filter=".services">@lang('site.services')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row featured__filter">
                    @foreach ($products_type as $item)
                        <div class="col-lg-3 col-md-4 col-sm-6 mix {{$item->product_type}}">
                            <div class="featured__item">
                            <div class="featured__item__pic set-bg" data-setbg="{{asset('uploads/products/'.$item->main_image)}}">
                                    <ul class="featured__item__pic__hover">
                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="featured__item__text">
                                    <h6><a href="{{url('showProduct/'.$item->translate(app()->getLocale())->slug)}}">{{$item->title}}</a></h6>
                                    <span>{{$item->price}} {{$item->country->currency}}</span>
                                </div>
                            </div>
                        </div>    
                    @endforeach
                    
                </div>
            </div>
        </section>
        <!-- Featured Section End -->
    
        <!-- Banner Begin -->
        <div class="banner">
            <div class="container">
                <div class="row">
                    @foreach (show_ads('inside-home',2) as $item)
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="banner__pic">
                            @if($item->code == NULL)
                                <a href="{{($item->link) ? $item->link : '#'}}">
                                    <img src="{{asset('uploads/googleImages/'.$item->image)}}" width="100%" title="{{$item->title}}" alt="{{$item->title}}"/>
                                </a>
                            @else
                                {!! $item->code !!}
                            @endif
                        </div>
                    </div>
                            
                    @endforeach
                    
                    
                </div>
            </div>
        </div>
        <!-- Banner End -->
    
        <!-- Latest Product Section Begin -->
        <section class="latest-product spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="latest-product__text">
                            <h4>@lang('site.latest_products')</h4>
                            <div class="latest-product__slider owl-carousel">
                                @foreach ($last_products as $item)
                                    <div class="latest-prdouct__slider__item">
                                        <a href="{{url('showProduct/'.$item->translate(app()->getLocale())->slug)}}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                            <img class="img-thumbnail" style="width:110px;" src="{{asset('uploads/products/'.$item->main_image)}}" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6 class="text-left"><b>{{$item->title}}</b></h6>
                                                <span class="text-left">{{$item->price}} {{$item->country->currency}}</span>
                                            </div>
                                        </a>
                                    
                                    </div>
                                @endforeach
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="latest-product__text">
                            <h4>@lang('site.commented_products')</h4>
                            <div class="latest-product__slider owl-carousel">
                                
                                @foreach ($commented_products as $item)
                                    <div class="latest-prdouct__slider__item">
                                        <a href="{{url('showProduct/'.$item->translate(app()->getLocale())->slug)}}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                            <img class="img-thumbnail" style="width:110px;" src="{{asset('uploads/products/'.$item->main_image)}}" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6 class="text-left"><b>{{$item->title}}</b></h6>
                                                <span class="text-left">{{$item->price}} {{$item->country->currency}}</span>
                                            </div>
                                        </a>
                                    
                                    </div>  
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="latest-product__text">
                            <h4>@lang('site.viewed_products')</h4>
                            <div class="latest-product__slider owl-carousel">
                                @foreach ($viewed_products as $item)
                                    <div class="latest-prdouct__slider__item">
                                        <a href="{{url('showProduct/'.$item->translate(app()->getLocale())->slug)}}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                            <img class="img-thumbnail" style="width:110px;" src="{{asset('uploads/products/'.$item->main_image)}}" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6 class="text-left"><b>{{$item->title}}</b></h6>
                                                <span class="text-left">{{$item->price}} {{$item->country->currency}}</span>
                                            </div>
                                        </a>
                                    
                                    </div>
                                @endforeach
    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>       
@endsection