@extends('site.layouts.app')
@section('content')
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>@lang('site.products')</h2>
                        <div class="breadcrumb__option">
                            <a href="{{url('/')}}">@lang('site.products')</a>
                            <span>{{ucwords($user->name)}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="card">
                        <div class="card-header">
                            @lang('site.products') ({{$user->name}})
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead class="thead-dark">
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">@lang('site.image')</th>
                                    <th scope="col">@lang('site.title')</th>
                                    <th scope="col">@lang('site.status')</th>
                                    <th>@lang('site.views')</th>
                                    <th>@lang('site.created_at')</th>
                                    @if($user->id == auth()->user()->id)
                                        <th>@lang('site.options')</th>
                                    @endif
                                  </tr>
                                </thead>
                                <tbody>
                                  @if(count($products) > 0)
                                        @php
                                            $i = 0;   
                                        @endphp  
                                        @foreach ($products as $item)
                                            @php
                                                $i++
                                            @endphp
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td><img width="75" height="75" class="img-responsive img-thumbnail" src="{{asset('uploads/products/'.$item->main_image)}}" /></td>
                                                <td><a href="{{url('showProduct/'.$item->translate(app()->getLocale())->slug)}}">{{$item->title}}</a></td>
                                                <td><i class="fa fa-{{($item->status == '0')? 'close text-danger rounded-circle border border-danger':'check text-success rounded-circle border border-success'}}"></i></td>
                                                <td>{{$item->views}}</td>
                                                <td>{{$item->created_at}}</td>
                                                @if($user->id == auth()->user()->id)
                                                    <td>
                                                        <a href="{{route('product.edit',$item->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                                        <a href="{{url('delProduct/'.$item->id)}}" class="btn btn-danger btn-sm delete-confirm"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                @endif
                                            </tr> 
                                        @endforeach     
                                    @else
                                            <tr>
                                                <td colspan="7"><p class="text-center text-danger">@lang('site.no_records')</p></td>
                                            </tr>
                                    @endif
                                </tbody>
                              </table>
                              {{ $products->links() }}

                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    // confirm delete
     $('.delete-confirm').on('click',function (event) {
         event.preventDefault();
         const url = $(this).attr('href');
         swal({
             title: '@lang('site.confirm_delete')',
             text: '@lang('site.delete_msg')',
             icon: 'warning',
             buttons: ["@lang('site.no')", "@lang('site.yes')"],
         }).then(function(value) {
             if (value) {
                 window.location.href = url;
             }
             
         });
     });
     
 </script>
@endpush