@extends('site.layouts.app')
@section('content')
<section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>@lang('site.edit_user')</h2>
                    <div class="breadcrumb__option">
                        <a href="{{url('/')}}">@lang('site.home')</a>
                        <span>@lang('site.edit_user')</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<!-- Contact Section Begin -->
<section class="contact spad">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center">
                        @lang('site.edit_user') ({{auth()->user()->name}})
                    </div>
                    <div class="card-body">
                        @if($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"><p class="text-center">{{$error}}</p></div>
                        @endforeach
					@endif  
                <form action="{{route('user.update',$user->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                        <div class="form-group col-md-12">
                            <label>@lang('site.name')</label>
                            <input type="text" name="name" class="form-control" value="{{$user->name}}" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.email')</label>
                            <input type="email" name="email" class="form-control" value="{{$user->email}}" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.show_email')</label><br>
                            <input type="radio" name="show_email" value="0" {{($user->show_email == 0)? 'checked' :''}}> @lang('site.no')
                            <input type="radio" name="show_email" value="1" {{($user->show_email == 1)? 'checked' :''}}> @lang('site.yes')
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.password')</label>
                            <input type="password" name="password" class="form-control"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.password_confirm')</label>
                            <input type="password" name="password_confirmation" class="form-control" />
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.phone')</label>
                        <input type="text" name="phone" class="form-control" value="{{$user->phone}}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.show_phone')</label><br>
                            <input type="radio" name="show_phone" value="0" {{($user->show_phone == 0)? 'checked' :''}}> @lang('site.no')
                            <input type="radio" name="show_phone" value="1" {{($user->show_phone == 1)? 'checked' :''}}> @lang('site.yes')
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.gender')</label>
                            <select class="form-control" name="gender">
                                <option value="female" {{($user->gender == 'female')? 'selected' :''}}>@lang('site.female')</option>
                                <option value="male" {{($user->gender == 'male')? 'selected' :''}}>@lang('site.male')</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.country_name')</label>
                            <select name="country_id" class="form-control" id="country">
                                @foreach ($countries as $country)

                                    <option value="{{$country->id}}" {{($user->country_id == $country->id)? 'selected' :''}}>{{$country->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.city')</label>
                            <select class="form-control" name="city_id" id="city">
                                @foreach ($cities as $city)

                                    <option value="{{$city->id}}" {{($user->city_id == $city->id)? 'selected' :''}}>{{$city->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 float-right">
                            <label>@lang('site.facebook_link')</label>
                        <input type="text" name="facebook_link" class="form-control" value="{{$user->facbook_link}}"/>
                        </div>
                        <div class="form-group col-md-6 float-left">
                            <label>@lang('site.twitter_link')</label>
                            <input type="text" name="twitter_link" class="form-control" value="{{$user->twitter_link}}"/>
                        </div>
                        <div class="form-group col-md-6 float-right">
                            <label>@lang('site.insatgram_link')</label>
                            <input type="text" name="instagram_link" class="form-control" value="{{$user->instagram_link}}"/>
                        </div>
                        <div class="form-group col-md-6 float-left">
                            <label>@lang('site.website_link')</label>
                            <input type="text" name="website_link" class="form-control" value="{{$user->website_link}}"/>
                        </div>
                        <div class="form-group col-md-12">
                            <label>@lang('site.image')</label>
                            <input type="file" id="logo_image" name="user_img" class="form-control"/>
                            <br>
                            <img src="{{url('uploads/users/'.$user->user_img)}}" id="logo-img-tag" class="img-responsive img-thumbnail" />
                        
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-pencil"></i> @lang('site.update')</button>
                        </div>

                </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection