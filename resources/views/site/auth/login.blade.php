<div class="modal" tabindex="-1" id="loginModal" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center">@lang('site.login')</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form class="form-login" method="post" action="{{ route('postLogin') }}">
              @csrf
                <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="@lang('site.email')">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control" placeholder="@lang('site.password')">
                </div>
                <div class="form-group">
                    <div class="col-md-6 offset-md-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                @lang('site.remember_me')
                            </label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-info btn-block" id='login-submit'>@lang('site.login')</button>
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="#" data-toggle="modal" data-target="#resetModal">
                                @lang('site.forget_password')
                            </a>
                        @endif
                   
            </form>
                <div class="text-center text-muted delimiter">@lang('site.social_login')</div>
                <br>
                <div class="d-flex justify-content-center social-buttons">
                    <a href="{{ url('/auth/redirect/twitter') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Twitter">
                      <i class="fa fa-twitter"></i>
                    </a>
                    <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Facebook">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a href="{{ url('/auth/redirect/linkedin') }}" class="btn btn-secondary social_button_links" data-toggle="tooltip" data-placement="top" title="Linkedin">
                      <i class="fa fa-linkedin"></i>
                    </a>
                </div>
        </div>
        <div class="modal-footer d-flex justify-content-center">
            <div class="signup-section">@lang('site.not_member') <a href="#a" data-toggle="modal" data-target="#signupModal" class="text-info">@lang('site.signup')</a>.</div>
          </div>
      </div>
    </div>
  </div>
  @push('scripts')
      <script>
        // Wait for the DOM to be ready
          $(function() {
              $("#login-submit").click(function(){
                    $(".form-login").validate({
                    rules: {
                      email: {
                        required: true,
                        email: true
                      },
                      password: {
                        required: true,
                        minlength: 5
                      }
                    },
                  messages: {
                      password: {
                        required: "<span class='text-danger'><?=__('site.password_field')?></span>",
                        minlength: "<span class='text-danger'><?=__('site.min_length_field')?></span>"
                      },
                      email: "<span class='text-danger'><?=__('site.email_field')?></span>"
                    },
                    submitHandler: function(form) {
                      form.submit();
                    }
                  });
              });
              //clear 
            $('#loginModal').on('hidden.bs.modal', function() {
                  var $alertas = $('.form-login');
                  $alertas.validate().resetForm();
                  $alertas.find('.error').removeClass('error');
            });
          });
          
      </script>
    @endpush