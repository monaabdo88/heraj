<div class="modal" tabindex="-1" id="resetModal" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center">@lang('site.reset_password')</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form class="form-reset" method="post" action="{{ route('password.email') }}">
              @csrf
                <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="@lang('site.email')" required autocomplete="email" autofocus>
                </div>
                
                <div class="form-group">
                    <a class="btn btn-link" href="#" data-toggle="modal" data-target="#loginModal">
                        <i class="fa fa-arrow-left"></i> @lang('site.login')
                    </a>
                    <button type="submit" id="reset-submit" class="btn btn-info btn-block">@lang('site.reset_password_link')</button>
                </div>
            </form>
                
        </div>
        
      </div>
    </div>
  </div>
  @push('scripts')
      <script>
        $(function() {
              $("#reset-submit").click(function(){
                    $(".form-reset").validate({
                    rules: {
                      email: {
                        required: true,
                        email: true
                      }
                    },
                  messages: {
                      email: "<span class='text-danger'><?=__('site.email_field')?></span>"
                    },
                    submitHandler: function(form) {
                      form.submit();
                    }
                  });
              });
                //clear 
            $('#resetModal').on('hidden.bs.modal', function() {
                  var $alertas = $('.form-reset');
                  $alertas.validate().resetForm();
                  $alertas.find('.error').removeClass('error');
            });
          });
      </script>
  @endpush