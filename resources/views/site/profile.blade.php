@extends('site.layouts.app')
@section('content')
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>@lang('site.profile')</h2>
                        <div class="breadcrumb__option">
                            <a href="{{url('/')}}">@lang('site.profile')</a>
                            <span>{{ucwords($user->name)}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="col-sm-6 col-md-4 float-left">
                        <img src="{{asset('uploads/users/'.$user->user_img)}}" width="100%" alt="{{$user->name}}" class="img-rounded img-responsive" />
                        <br><br>
                        <div class="col-md-8 offset-md-2">
                            <a href="{{($user->website_link != NULL) ? $user->website_link : '#'}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-globe"></i></a>
                            <a href="{{($user->instagram_link != NULL) ? $user->instagram_link : '#'}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-instagram"></i></a>
                            <a href="{{($user->twitter_link != NULL) ? $user->twitter_link : '#'}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-twitter"></i></a>
                            <a href="{{($user->facebook_link != NULL) ? $user->facebook_link : "#"}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-facebook"></i></a>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        @if(auth()->user()->id != $user->id)
                            @if(auth()->user()->isFollowing($user))
                                <a href="#" class="btn btn-block btn-primary">@lang('site.unfollow')</a>
                            @else
                                <a href="#" class="btn btn-block btn-primary">@lang('site.follow')</a>
                            @endif
                        @endif
                        @if ($user->id == auth()->user()->id)
                            <a href="{{route('user.edit',$user->id)}}" class="btn btn-info btn-block">@lang('site.update_profile')</a>
                        @endif
                    </div>
                    <div class="col-sm-6 col-md-8 float-right">
                        <h4>
                            {{ucwords($user->name)}}</h4>
                        <p>
                            <small>
                                <cite title="{{$user->city->title}}, {{$user->country->title}}"><i class="fa fa-map-marker"></i> {{$user->city->title}}, {{$user->country->title}} 
                                    
                                </cite>
                            </small>
                        </p>
                            <p>
                                <i class="fa fa-envelope"></i> 
                                @if(auth()->user()->id == $user->id || $user->show_email == 'yes') 
                                    {{$user->email}}
                                @else
                                    @lang('site.hidden')
                                @endif
                            </p>
                        <p>
                            <i class="fa fa-phone"></i> 
                            @if(auth()->user()->id == $user->id || $user->show_phone == 'yes') 
                                {{$user->phone}}
    
                            @else
                                @lang('site.hidden')
                            @endif
    
                            @if(auth()->user()->id == $user->id && $user->status == 0)
                                <a class="btn btn-info" href="{{url('verifyPhone/'.$user->id)}}">@lang('site.verifyPhone')</a>
                            @endif
                            
                        </p>
                        <p>
                            <i class="fa fa-{{($user->gender == 'female') ? 'female' : 'male'}}"></i> @lang('site.'.$user->gender)
                        </p>
                        <p><i class="fa fa-product-hunt"></i> {{$user->products->count()}}</p>
                        <p>
                            @if($user->status == 1)
                                <i class="fa fa-check text-success"></i> @lang('site.reliable')
                            @else
                                <i class="fa fa-times text-danger"></i> @lang('site.wait_reliable')
                            @endif
                        </p>
                        <p>
                            <i class="fa fa-calendar"></i> {{$user->created_at}}
                        </p>
                        <p>
                            <a href="{{url('userProducts/'.$user->id)}}" class="btn btn-success">@lang('site.products')</a>
                            @if(auth()->user()->id == $user->id)
                                <a href="{{url('myFavs/'.$user->id)}}" class="btn btn-primary">@lang('site.myFavs')</a>
                                <a href="{{url('myFollowers/'.$user->id)}}" class="btn btn-dark">@lang('site.followers')</a>
                                <a href="{{url('followUp/'.$user->id)}}" class="btn btn-light">@lang('site.followback')</a>
                                <a href="{{url('myMsgs/'.$user->id)}}" class="btn btn-info">@lang('site.myMsg')</a>
                            @endif
                        </p>
                        
                    </div>
                </div>
                @include('site.inc.sideAd')
            </div>
        </div>
    </section>
@endsection