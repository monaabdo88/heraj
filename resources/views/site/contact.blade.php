@extends('site.layouts.app')
@section('content')

    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>@lang('site.contact')</h2>
                        <div class="breadcrumb__option">
                            <a href="{{url('/')}}">@lang('site.home')</a>
                            <span>@lang('site.contact')</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                    <div class="contact__widget">
                        <span class="icon_phone"></span>
                        <h4>@lang('site.phone')</h4>
                        <p>{{settings()->site_phone}}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                    <div class="contact__widget">
                        <span class="icon_pin_alt"></span>
                        <h4>@lang('site.address')</h4>
                        <p>{{(app()->getLocale() == 'ar') ? settings()->site_address_ar : settings()->site_address_en}}</p>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                    <div class="contact__widget">
                        <span class="icon_mail_alt"></span>
                        <h4>@lang('site.email')</h4>
                        <p>{{settings()->site_email}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Section End -->
    <!-- Contact Form Begin -->
    <div class="contact-form spad" style="background: #c6c9f1;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="contact__form__title">
                        <h2>@lang('site.leave_msg')</h2>
                    </div>
                </div>
            </div>
            <form id="contact_form" action="{{route('contactMessage')}}" method="post" role="form">
                    
                <div class="row">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif   
                        @csrf
                        <div class="col-lg-6 col-md-6">
                            <input type="text" name="name" placeholder="@lang('site.name')" />
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <input type="text" name="email" placeholder="@lang('site.email')" />
                        </div>
                        <div class="col-md-12">
                            <input type="text" name="subject" placeholder="@lang('site.subject')" />
                        </div>
                        <div class="col-lg-12 text-center">
                            <textarea name="message" placeholder="@lang('site.message')"></textarea>
                        </div>
                        <div class="col-md-4 offset-md-4">
                            <div class="g-recaptcha" data-sitekey="6Lf2kgQaAAAAADChUMt-QCjqL161FETtkG22DLoM" data-callback="enableBtn"></div>
                        </div>
                        <div class="col-md-12 text-center">
                            <br>
                            <button class="site-btn" id="send_message" type="submit" disabled="disabled">@lang('site.send_msg')</button>
                        
                        </div>
                    
                </div>
            
        </div>
    </div>
</form>
                    
    <!-- Contact Form End -->

@endsection
@push('scripts')
    <script>
        $(function() {
              $("#send_message").click(function(){
                    $("#contact_form").validate({
                    rules: {
                      name:'required',
                      subject:'required',
                      msg:'required',
                      email: {
                        required: true,
                        email: true
                      }
                    },
                  messages: {
                      name:"<span class='text-danger'><?=__('site.name')?> <?=__('site.required')?></span>",
                      subject:"<span class='text-danger'><?=__('site.subject')?> <?=__('site.required')?></span>",
                      msg:"<span class='text-danger'><?=__('site.message')?> <?=__('site.required')?></span>",
                      email: "<span class='text-danger'><?=__('site.email_field')?> <?=__('site.required')?></span>"
                    },
                    submitHandler: function(form) {
                      form.submit();
                    }
                  });
              });
            });
            function enableBtn(){
                document.getElementById("send_message").disabled = false;
            }         
    </script>
@endpush