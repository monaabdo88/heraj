<nav class="humberger__menu__nav mobile-menu">
    <ul>
        <li class="active"><a href="{{url('/')}}">@lang('site.home')</a></li>
        <li><a href="#">@lang('site.categories')</a></li>
        <li><a href="">@lang('site.products')</a></li>
        @foreach (get_pages('nav',2) as $item)
            <li><a href="#">{{$item->title}}</a></li>   
        @endforeach
        <li><a href="{{url('/contact')}}">@lang('site.contact')</a></li>
    </ul>
</nav>