<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        <ul>
                        <li><i class="fa fa-envelope"></i> {{settings()->site_email}}</li>
                            
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                        <div class="header__top__right__social">
                            @foreach (get_socials() as $social)
                                <a href="{{$social->link}}"><img src="{{asset('uploads/icons/'.$social->icon)}}"/></a>
                            @endforeach
                        </div>
                        <div class="header__top__right__language">
                            <div><i class="fa fa-flag"></i></div>
                            <span class="arrow_carrot-down"></span>
                            <ul>
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li>
                                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                            {{ $properties['native'] }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="header__top__right__auth">
                            @guest
                                <a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-user"></i> @lang('site.login')</a>
                            @else

                               <div class="header__top__right__language">
                                    <div>{{Auth::guard('web')->user()->name}}</div>
                                    <span class="arrow_carrot-down"></span>
                                    <ul>
                                        <li><a class="dropdown-item" href="{{url('product/create')}}">@lang('site.new_ad')</a></li>
                                        <li><a class="dropdown-item" href="{{route('user.show',auth()->user()->id)}}">@lang('site.profile')</a></li>
                                        <li><a class="dropdown-item" href="{{url('userProducts/'.auth()->user()->id)}}">@lang('site.myAds')</a></li>
                                        <li><a class="dropdown-item" href="{{url('myFavs/'.auth()->user()->id)}}">@lang('site.myFavs')</a></li>
                                        <li><a class="dropdown-item" href="{{url('myMsg/'.auth()->user()->id)}}">@lang('site.myMsg')</a></li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('userLogout') }}"
                                            onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();">
                                             @lang('site.logout')
                                            </a>
                                            <form id="logout-form" action="{{ route('userLogout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                                
                            @endguest
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="header__logo">
                <a href="{{url('/')}}">
                    @if(app()->getLocale() == 'ar')
                        <img src="{{asset('uploads/images/'.settings()->logo_ar)}}" alt="">
                    @else
                        <img src="{{asset('uploads/images/'.settings()->logo_en)}}" alt="">
                    @endif
                </a>
                </div>
            </div>
            <div class="col-lg-9">
                <nav class="header__menu">
                    <ul>
                        <li class="active"><a href="{{url('/')}}">@lang('site.home')</a></li>
                        @if(app()->getLocale() == 'ar')
                            @php
                                $arrow = 'left'; 
                            @endphp
                        
                        @else
                            @php
                                $arrow = 'right';
                            @endphp
                        @endif
                        <li><a class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#">@lang('site.categories')</a>
                            <ul class="header__menu__dropdown categories_menu">
                                @foreach(cats_nav() as $category)
                                    <li class="child">
                                        <a href="{{route('categories.show',$category->id)}}" role="button" aria-haspopup="true" aria-expanded="true">{{{ $category->title }}} 
                                            @if(count($category->childs) > 0)
                                                <i class="arrow_icon fa fa-angle-{{$arrow}}"></i>
                                            @endif
                                        </a>
                                        @if(count($category->childs))
                                            @include('site.inc.category_child',['childs' => $category->childs])
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </li>
            
                        <li><a href="{{url('allProducts')}}">@lang('site.products')</a></li>
                        @foreach (get_pages('nav',2) as $item)
                            <li><a href="{{url('getPage/'.$item->id)}}">{{$item->title}}</a></li>   
                        @endforeach
                        <li><a href="{{url('/contact')}}">@lang('site.contact')</a></li>
                    </ul>
                    
                </nav>
            </div>
            <!--<div class="col-lg-3">
                <div class="header__cart">
                    <ul>
                        <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                        <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
                    </ul>
                    <div class="header__cart__price">item: <span>$150.00</span></div>
                </div>
            </div>-->
        </div>
        <div class="humberger__open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>
@guest
    <!--------------------------------------Login Model ------------------------------------->
    @include('site.auth.login')
    <!--------------------------------------End Login Model --------------------------------->
    <!--------------------------------------Signup Model ------------------------------------>
    @include('site.auth.signup')
    <!-----------------------------------End Signup Model ----------------------------------->
    <!-----------------------------------Reset Password Model ------------------------------->
    @include('site.auth.reset')
    <!----------------------------------End Reset Password Model ---------------------------->
@endguest