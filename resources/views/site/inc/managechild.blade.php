<ul class="header__submenu__dropdown">
    @foreach($childs as $child)
        <li class="child">
        <a role="button" aria-haspopup="true" aria-expanded="true" href="{{route('categories.show',$child->id)}}">{{{ $child->title }}} 
            @if(count($child->childs) > 0)
                <i class="arrow_icon fa fa-angle-{{$arrow}}"></i>
            @endif
        </a>
     @if(count($child->childs))
        @include('site.inc.managechild',['childs' => $child->childs])
     @endif
      </li>
    @endforeach
</ul>