<div class="col-lg-3">
    <div class="hero__categories mb-10">
        <div class="hero__categories__all">
            <i class="fa fa-bars"></i>
            <span>@lang('site.last_categories')</span>
        </div>
        <ul>
            @foreach ($main_cats as $cat)
                <li><a href="{{route('categories.show',$cat->id)}}"><i class="fa fa-arrow-{{(app()->getLocale() == 'ar')? 'left':'right'}}"></i> {{ucwords($cat->title)}}</a></li> 
            @endforeach
        </ul>
    </div>
    <div class="search-box text-center">
        <div class="search-header text-center">
            <span>
                @lang('site.search')
            </span>  
        </div>
            
        <div class="search-body">
            <form>
                <div class="form-group">
                    <select class="form-control">
                        <option selected value="0">
                            @lang('site.all_categories')
                        </option>
                        @foreach ($main_cats as $cat)
                            <option value="{{$cat->id}}">{{ucwords($cat->title)}}</option> 
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                <input type="text" placeholder="@lang('site.search_word')" class="form-control" />
                </div>
                
                <button type="submit" class="site-btn">@lang('site.search')</button> 
            </form>
        </div>
        
    </div>
</div>