<div class="col-lg-9">
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            @foreach ($sliders as $item)
                <li><img src="{{asset('uploads/sliders/'.$item->image)}}" title="{{ucwords($item->title)}}" id="{{$item->id}}"></li>
            @endforeach
        </ul>
    </div>
        <div class="ws_bullets"><div>
            @foreach ($sliders as $item)
                <a href="{{($item->link) ? $item->link : '#'}}" title="{{ucwords($item->title)}}"><span><img src="{{asset('uploads/sliders/'.$item->image)}}" alt="{{ucwords($item->title)}}"/>{{$item->id}}</span></a>
            @endforeach
        </div>
    </div>
    <div class="ws_shadow"></div>
    </div>	
    <!-- End WOWSlider.com BODY section -->
</div>