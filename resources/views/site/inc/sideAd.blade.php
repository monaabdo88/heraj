@if(count(show_ads('sidebar-page',2)) > 0)
    @foreach (show_ads('sidebar-page',2) as $item)
        <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="banner__pic">
                    @if($item->code == NULL)
                        <a href="{{($item->link) ? $item->link : '#'}}">
                            <img src="{{asset('uploads/googleImages/'.$item->image)}}" width="100%" title="{{$item->title}}" alt="{{$item->title}}"/>
                        </a>
                    @else
                        {!! $item->code !!}
                    @endif
                
            </div>
        </div>
                                
    @endforeach
@else
    <div class="col-md-3">
        @if(app()->getLocale() == 'ar')
            <img src="{{asset('uploads/googleImages/no-ad-ar.png')}}" width="100%" title="no-ad-ar" alt="no-ad-ar"/>
            <img src="{{asset('uploads/googleImages/no-ad-ar.png')}}" width="100%" title="no-ad-ar" alt="no-ad-ar"/>
        @else
            <img src="{{asset('uploads/googleImages/no-ad-en.png')}}" width="100%" title="no-ad-en" alt="no-ad-en"/>
            <img src="{{asset('uploads/googleImages/no-ad-en.png')}}" width="100%" title="no-ad-en" alt="no-ad-en"/>
        
        @endif
    </div> 
@endif