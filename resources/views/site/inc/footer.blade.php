<footer class="footer spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="footer__about">
                    <div class="footer__about__logo">
                    @if(app()->getLocale() == 'ar')
                        @php
                            $logo = settings()->logo_ar;   
                        @endphp
                    @else
                        @php
                            $logo = settings()->logo_en;
                        @endphp
                    @endif
                    <a href="{{url('/')}}"><img width="200" height="50" src="{{asset('uploads/images/'.$logo)}}"></a>
                    </div>
                    <ul>
                        <li><i class="fa fa-map-marker"></i> {{(app()->getLocale() == 'ar') ? settings()->site_address_ar : settings()->site_address_en}}</li>
                        <li><i class="fa fa-phone"></i> {{settings()->site_phone}}</li>
                        <li><i class="fa fa-envelope"></i>  {{settings()->site_email}}</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="footer__widget">
                    <h6>@lang('site.useful_links')</h6>
                    
                    <ul>
                        <li><a href="{{url('/')}}"><i class="fa fa-arrow-{{(app()->getLocale() == 'ar')? 'left':'right'}}"></i> @lang('site.home')</a></li>
                        <li><a href=""><i class="fa fa-arrow-{{(app()->getLocale() == 'ar')? 'left':'right'}}"></i> @lang('site.categories')</a></li>
                        <li><a href=""><i class="fa fa-arrow-{{(app()->getLocale() == 'ar')? 'left':'right'}}"></i> @lang('site.products')</a></li>
                        <li><a href="{{url('/contact')}}"><i class="fa fa-arrow-{{(app()->getLocale() == 'ar')? 'left':'right'}}"></i> @lang('site.contact')</a></li>
                    </ul>
                    <ul>
        
                        @foreach (get_pages('footer',5) as $item)
                            <li><a href="{{url('getPage/'.$item->id)}}"><i class="fa fa-arrow-{{(app()->getLocale() == 'ar')? 'left':'right'}}"></i> {{$item->title}}</a></li> 
                        @endforeach
                    </ul>
                    
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="footer__widget">
                    <h6>@lang('site.join_newsletter')</h6>
                    <p>@lang('site.get_new')</p>
                    <form  action="{{route('subscribe')}}" method="POST">
                        @csrf
                        <input type="email" name="email" placeholder="@lang('site.email')" required>
                        <button type="submit" class="site-btn">@lang('site.subscribe')</button>
                    </form>
                    <div class="footer__widget__social">
                        @foreach (get_socials() as $social)
                            <a href="{{$social->link}}"><img src="{{asset('uploads/icons/'.$social->icon)}}"/></a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="footer__copyright">
                    <div class="footer__copyright__text"><p>{{(app()->getLocale() == 'ar') ? settings()->site_copyrights_ar : settings()->site_copyrights_en}}</p></div>
                </div>
            </div>
        </div>
    </div>
</footer>