@extends('site.layouts.app')
@section('content')
    
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>{{$category->title}}</h2>
                        <div class="breadcrumb__option">
                            <a href="{{url('/')}}">@lang('site.home')</a>
                            <span>{{$category->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('site.home')</a></li>
                            @if ($category->getParentsNames() !== $category->title)
                                @foreach ($category->getParentsNames()->reverse() as $item)
                                    @if ($item->parent_id == 0)
                                        <li class="breadcrumb-item"><a href="{{ route('categories.show',$item->id) }}">{{ $item->title }}</a></li>
                                            @else
                                        <li class="breadcrumb-item"><a href="{{ route('categories.show',$item->id) }}">{{ $item->title }}</a></li>
                                    @endif
                                @endforeach  
                            @endif
                            <li class="breadcrumb-item active">{{$category->title}}</li>
                        </ol>
                      </nav>
                    
                    @foreach ($childs as $item)
                        <div class="col-lg-3 col-md-4 float-left col-sm-6 mix {{$item->product_type}}">
                            <div class="featured__item">
                                <div class="featured__item__pic set-bg" data-setbg="{{asset('uploads/products/'.$item->main_image)}}">
                                    <ul class="featured__item__pic__hover">
                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                    </ul>
                                </div>
                                <div class="featured__item__text">
                                    <h6><a href="{{url('showProduct/'.$item->translate(app()->getLocale())->slug)}}">{{$item->title}}</a></h6>
                                    <span>{{$item->price}} {{$item->country->currency}}</span>
                                </div>
                            </div>
                        </div>    
                           
                    @endforeach
                </div>
                @include('site.inc.sideAd')
            </div>
        </div>
    </section>
@endsection