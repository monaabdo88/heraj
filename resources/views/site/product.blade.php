@extends('site.layouts.app')
@section('content')
     <!-- Breadcrumb Section Begin -->
     <section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>{{$product->title}}</h2><br>
                        <div class="breadcrumb__option">
                            <a href="{{url('/')}}">@lang('site.home')</a>
                            <span>{{$product->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->
     <section class="product-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__pic">
                        <div class="product__details__pic__item">
                            <img class="product__details__pic__item--large"
                                src="{{asset('uploads/products/'.$product->main_image)}}" alt="">
                        </div>
                        <div class="product__details__pic__slider owl-carousel">
                            
                            @foreach ($media as $item)
                            <img data-imgbigurl="{{asset('storage/'.$item->id.'/'.$item->file_name)}}"
                                src="{{asset('storage/'.$item->id.'/'.$item->file_name)}}" alt="">    
                            @endforeach
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__text">
                        <h3>{{$product->title}}</h3>
                        
                        <div class="product__details__price">{{$product->price}} {{$product->country->currency}}</div>
                        <div class="product__details__rating">
                        
                        <form action="{{ route('product.rate') }}" method="POST">
                            {{ csrf_field() }}   
                                        <div class="details col-md-6">
                                            <div class="rating">
                                                <input id="input-1" name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="{{ $product->averageRating }}" data-size="xs">
                                                @if(isset(auth()->user()->id))
                                                    @if(auth()->user()->id != $product->user_id)
                                                        <input type="hidden" name="id" required="" value="{{ $product->id }}">
                                                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
                                                        <br/>
                                                        <button class="btn btn-success">@lang('site.submit_review')</button>
                                                    @endif
                                                @endif
                                            </div>
                                            
                                        </div>                                   
                        </form>
                        
                        </div>
                        <!--<a href="#" class="heart-icon"><span class="icon_heart_alt"></span></a>-->
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2 float-left"><b>@lang('site.address')</b></div>
                                        <div class="col-md-10 float-right">   <span>{{$product->country->title}} - {{$product->city->title}}</span>
                                        </div>
                                    </div>
                                </div>
                                
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2 float-left"><b>@lang('site.category')</b></div>
                                        <div class="col-md-10 float-right">
                                            @if($product->categories->parent_id != 0)
                                                @foreach ($product->categories->getParentsNames()->reverse() as $item)
                                                    @if ($item->parent_id == 0)
                                                        <a href="{{ route('categories.show',$item->id) }}">{{ $item->title }}</a> >
                                                    @else
                                                        > <a href="{{ route('categories.show',$item->id) }}">{{ $item->title }}</a> >
                                                    @endif
                                                @endforeach  
                                            @endif    
                                         <a href="{{ route('categories.show',$product->category_id) }}">{{$product->categories->title}}</a>

                                        </div>
                                    </div> 
                                </div>                                                                                                                          
                            </li>
                            
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2 float-left"> <b>@lang('site.ad_type')</b></div>
                                        <div class="col-md-10 float-right"><span>{{$product->product_type}}</span></div>
                                    </div>           
                                </div>                       
                            </li>
                            @if(count($product->sizes)>0)
                                <li> 
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-2 float-left"><b>@lang('site.sizes')</b>  </div>
                                            <div class="col-md-10 float-right">

                                                @foreach ($product->sizes as $item)
                                                    <span class="border border-info" style="padding:0 5px;">{{$item->title}}</span>
                                                @endforeach    
                                            </div>
                                        </div> 
                                    </div>                                                                                      
                                </li>
                            @endif
                            @if(count($product->colors) >0)
                                <li>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-2 float-left"><b>@lang('site.colors')</b></div>
                                            <div class="col-md-10 float-right">
                                                @foreach ($product->colors as $item)
                                                <div class="border float-left" style="background-color:{{$item->title}};width:20px;height:20px;margin:5px 0;"></div>
                                                
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>                                
                                </li>
                            @endif
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2 float-left"><b>@lang('site.views')</b></div>
                                        <div class="col-md-10 float-right"><span>{{$product->views}}</span> </div>
                                    </div>
                                </div>
                                
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2 float-left"><b>@lang('site.rating_count')</b></div>
                                        <div class="col-md-10 float-right"><span>{{count($product->ratings)}}</span></div>
                                    </div>
                                </div>
                                
                            </li>
                            
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2 float-left"><b>@lang('site.tags')</b></div>
                                        <div class="col-md-10 float-right"> 
                                            @foreach ($product->tags as $item)
                                                <a href="{{url('tagProducts/'.$item->id)}}" class="border border-info" style="padding:0 5px">{{$item->title}}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>                                         
                            </li>
                            
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2 float-left"><b>@lang('site.author')</b></div>
                                        <div class="col-md-10 float-right"><a href="{{url('showUserProducts/'.$product->user_id)}}">{{$product->user->name}}</a></div>
                                    </div>
                                </div>
                            </li>
                            @if(isset(auth()->user->id))
                                @if(auth()->user()->id != $product->user_id)
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-2 float-left"><b>@lang('site.favourite')</b></div>
                                                <div class="col-md-10 float-right"><a href="#" class="btn btn-success">@lang('site.add_to_fav') <i class="fa fa-heart-o"></i></a></div>
                                            </div>
                                        </div>
                                    </li>
                                @endif
                            @endif
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2 float-left"><b>@lang('site.share')</b></div>
                                        <div class="col-md-10 float-right">
                                            <div class="share">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-instagram"></i></a>
                                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                    aria-selected="true">@lang('site.details')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                    aria-selected="false">@lang('site.location_in_map')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"
                                    aria-selected="false">@lang('site.comments') <span>{{count($product->comments)}}</span></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>@lang('site.description')</h6>
                                    {!! $product->translate(app()->getLocale())->description !!}
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>@lang('site.location_in_map')</h6>
                                    <div id="us1" style="width: 100%; height: 400px;"></div>
                                        <input type="hidden" value="{{ $product->lat }}" id="lat" name="lat">
                                        <input type="hidden" value="{{ $product->lng }}" id="lng" name="lng">
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Products Infomation</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                        Proin eget tortor risus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->

    <!-- Related Product Section Begin -->
    <section class="related-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title related__product__title">
                        <h2>@lang('site.related_products')</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($related_products as $item)
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="{{asset('uploads/products/'.$item->main_image)}}">
                            <!--<ul class="product__item__pic__hover">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                            </ul>-->
                        </div>
                        <div class="product__item__text">
                            <h6><a href="{{url('showProduct/'.$item->translate(app()->getLocale())->slug)}}">{{$item->title}}</a></h6>
                            <h5>{{$item->price}} {{$product->country->currency}}</h5>
                        </div>
                    </div>
                </div>
                @endforeach
                
               
            </div>
        </div>
    </section>
    <!-- Related Product Section End -->
@endsection
@push('scripts')
    <script>
        $("#input-id").rating();
        //Location Map
        $('#us1').locationpicker({
        location: {
            latitude: {{ $product->lat }},
            longitude:{{ $product->lng }}
        },
        radius: 300,
            markerIcon: '{{ asset('img/map-marker-2-xl.png') }}',
            inputBinding: {
                latitudeInput: $('#lat'),
                longitudeInput: $('#lng'),
        }
        });  
    </script>
@endpush