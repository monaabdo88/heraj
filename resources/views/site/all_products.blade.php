    @extends('site.layouts.app')
@section('content')
    
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>@lang('site.products')</h2>
                        <div class="breadcrumb__option">
                            <a href="{{url('/')}}">@lang('site.home')</a>
                            <span>@lang('site.products')</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    @foreach ($products as $item)
                    <div class="col-lg-3 col-md-4 col-sm-6 mix {{$item->product_type}} float-left">
                        <div class="featured__item">
                        <div class="featured__item__pic set-bg" data-setbg="{{asset('uploads/products/'.$item->main_image)}}">
                                <ul class="featured__item__pic__hover">
                                    <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                </ul>
                            </div>
                            <div class="featured__item__text">
                                <h6><a href="{{url('showProduct/'.$item->translate(app()->getLocale())->slug)}}">{{$item->title}}</a></h6>
                                <span>{{$item->price}} {{$item->country->currency}}</span>
                            </div>
                        </div>
                    </div>    
                    @endforeach
                </div>
                @include('site.inc.sideAd')
            </div>
        </div>
    </section>
@endsection