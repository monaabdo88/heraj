<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{(app()->getLocale() == 'ar') ? settings()->site_name_ar : settings()->site_name_en}}</title>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
    <link href="{{asset('css/site.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    @if (app()->getLocale() == 'ar')        
        <link rel="stylesheet" href="{{asset('site_files/css/rtlbootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('site_files/css/style_ar.css')}}" type="text/css">
    @else
        <link rel="stylesheet" href="{{asset('site_files/css/bootstrap.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('site_files/css/style.css')}}" type="text/css">
    @endif
    @toastr_css 
    

</head>

<body>
    @include('notify::messages')         
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Humberger Begin -->
    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="{{url('/')}}">
                @if(app()->getLocale() == 'ar')
                    <img src="{{asset('uploads/images/'.settings()->logo_ar)}}" alt="">
                @else
                    <img src="{{asset('uploads/images/'.settings()->logo_en)}}" alt="">
                @endif
            </a>
        </div>
        
        <div class="humberger__menu__widget">
            <div class="header__top__right__language">
                <div><i class="fa fa-flag"></i></div>
                <span class="arrow_carrot-down"></span>
                
                <ul>
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li>
                            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ $properties['native'] }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="header__top__right__auth">
                <a href="#"><i class="fa fa-user"></i> @lang('site.login')</a>
            </div>
        </div>
        @include('site.inc.nav')
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            @foreach (get_socials() as $social)
                <a href="{{$social->link}}"><img src="{{asset('uploads/icons/'.$social->icon)}}"/></a>
            @endforeach
        </div>
        <div class="humberger__menu__contact">
            <ul>
                <li><i class="fa fa-envelope"></i> {{settings()->site_email}}</li>
            </ul>
        </div>
    </div>
    <!-- Humberger End -->

    <!-- Header Section Begin -->
    @include('site.inc.header')
    <!-- Header Section End -->

    @yield('content')
    <!-- Footer Section Begin -->
    @include('site.inc.footer')
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('site_files/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('site_files/js/popper.min.js')}}"></script>
    <script src="{{asset('site_files/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>

    <script src="{{asset('site_files/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('site_files/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('site_files/js/mixitup.min.js')}}"></script>
    <script src="{{asset('site_files/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('site_files/js/main.js')}}"></script>
    <script src="{{asset('site_files/sliders/engine1/wowslider.js')}}"></script>
    <script src="{{asset('site_files/sliders/engine1/script.js')}}"></script>
    <script src="{{asset('js/locationpicker.jquery.js')}}"></script>   
    <script src="{{asset('dashboard_files/jstree/jstree.js')}}"></script>
    <script src="{{asset('dashboard_files/jstree/jstree.wholerow.js')}}"></script>
    <script src="{{asset('dashboard_files/jstree/jstree.checkbox.js')}}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.selectboxit/3.8.0/jquery.selectBoxIt.min.js"></script>
    @toastr_js
    @toastr_render
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script type="text/javascript" src="http://bootstrapessentials.com/dist/js/bootstrap-essentials.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>
    @stack('scripts')
    <script>
        $('body').on('show.bs.modal', '.modal', function () {
            $('.modal:visible').removeClass('fade').modal('hide').addClass('fade');
        });
        
        $(function(){
            $("#country").on('change',function(){
                var country_id = $(this).val();
                var lang = '{{app()->getLocale()}}';
                var url ='{{url('get_cities')}}'+'/'+country_id+'/'+lang;
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(data){
                        $("#city").html(data);
                        
                    }
                });
                
            });
            
        });
        // preview image before upload
        function readURL(input) {
                //show image after upload
                    $('#logo-img-tag').css('display','block');
                // upload image and preview
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#logo-img-tag').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            // change and upload new image
            $("#logo_image").change(function(){
                readURL(this);
            });
               
        $(document).ready(function() {
            toastr.options.timeOut = 3500; // 3.5s
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    toastr.error("{{$error}}");  
                @endforeach 
            @endif
            @if (session('status'))
                toastr.success("{{session('status')}}");
            @endif
            
        });
    </script>
</body>

</html>