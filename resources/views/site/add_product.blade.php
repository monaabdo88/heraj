@extends('site.layouts.app')
@section('content')
    <?php
        $lat = !empty(old('lat')) ? old('lat') : '30.034024628931657';
        $lng = !empty(old('lng')) ? old('lng') : '31.24238681793213';
    ?>
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>@lang('site.new_ad')</h2>
                        <div class="breadcrumb__option">
                            <a href="{{url('/')}}">@lang('site.home')</a>
                            <span>@lang('site.new_ad')</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-center">
                            @lang('site.new_ad')
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif   
                            <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data" role="form">
                                @csrf
                                    @foreach (config('translatable.locales') as $locale)
                                        <div class="form-group col-md-12">
                                            <label>@lang('site.' . $locale . '.title')</label>
                                            <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{ old($locale . '.title') }}">
                                        </div>
                                        
                                        <div class="form-group col-md-12">
                                            <label>@lang('site.' . $locale .'.content')</label>
                                        <textarea class="form-control" name="{{ $locale }}[description]">{{old('description')}}</textarea>
                                        </div>
                                        
                                    @endforeach
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6 float-left">
                                                <label>@lang('site.country_name')</label>
                                                <select name="country_id" class="form-control" id="country_id">
                                                        <option value="">@lang('site.selecte_country')</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{$country->id}}">{{$country->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 float-right">
                                                <label>@lang('site.city')</label>
                                                <select class="form-control" name="city_id" id="city_id"></select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group col-md-12">
                                        <label>@lang('site.category')</label>
                                        <div id="jstree"></div>
                                        <input type="hidden" class="parent_id" value="{{old('category_id')}}" name="category_id">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>@lang('site.map_address')</label>
                                        <div id="us1" style="width: 100%; height: 400px;"></div>
                                        <input type="hidden" value="{{ $lat }}" id="lat" name="lat">
                                        <input type="hidden" value="{{ $lng }}" id="lng" name="lng">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>@lang('site.ad_type')</label><br>
                                        <div class="col-md-3">
                                            <input type="radio" name="product_type" value="rent" /> @lang('site.rent')
                                        
                                        </div>
                                        <div class="col-md-3">
                                            <input type="radio" name="product_type" value="sale" /> @lang('site.sale')
                                        
                                        </div>
                                        <div class="col-md-3">
                                            <input type="radio" name="product_type" value="exchange" /> @lang('site.exchange')
                                        
                                        </div>
                                        <div class="col-md-3">
                                            <input type="radio" name="product_type" value="services"> @lang('site.services')
                                   
                                        </div>
                                     </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6 float-right">
                                                <label>@lang('site.phone')</label>
                                                <input type="text" name="phone" class="form-control" value="{{old('phone')}}"/>
                                            </div>
                                            <div class="form-group col-md-6 float-left">
                                                <label>@lang('site.price')</label>
                                                <input type="text" name="price" class="form-control" value="{{old('price')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="">@lang('site.colors')</label>
                                        <div class="clearfix"></div>
                                        @foreach ($colors as $item)
                                        <div class="col-md-2 float-left">
                                            <input type="checkbox" name="color_id[]" value="{{$item->id}}" />
                                            <div style="width:100%;height:15px;background:{{$item->title}}"></div>
                                        </div>
                                           
                                        @endforeach
                                    </div>
                                    <div class="clearfix"></div><br>
                                    <div class="form-group col-md-12">
                                        <label for="">@lang('site.sizes')</label>
                                        <div class="clearfix"></div>
                                        @foreach ($sizes as $item)
                                        <div class="col-md-2  float-left">
                                            <input type="checkbox" name="size_id[]" value="{{$item->id}}" />
                                            {{$item->title}} 
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="clearfix"></div><br>
                                    <div class="form-group col-md-12">
                                        <label>@lang('site.tags')</label><br>
                                        @foreach($tags as $tag)
                                        <div class="col-md-3  float-left">
                                            <input type="checkbox" name="tag_id[]" value="{{$tag->id}}"/> {{$tag->title}} <br>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="form-group col-md-12">
                                        <label>@lang('site.main_image')</label>
                                        <input type="file" id="logo_image" class="form-control" name="main_image" />
                                        <br>
                                        <img src="#" id="logo-img-tag" class="img-responsive img-thumbnail" />
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.media')</label>
                                        <div class="needsclick dropzone" id="document-dropzone">           
                                        </div>
                                    </div>
                                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}" />
                                    
                                    <div class="form-group col-md-12">
                                        <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> @lang('site.add')</button>
                                    </div>
            
                            </form>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')

<script>
            // preview image before upload
            function readURL(input) {
                //show image after upload
                $('#logo-img-tag').css('display','block');
                // upload image and preview
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#logo-img-tag').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            // change and upload new image
            $("#logo_image").change(function(){
                readURL(this);
            });
            // hide image before upload
            var src = $('#logo-img-tag').attr('src');
            if(src==="#"){
                $('#logo-img-tag').css('display','none');
            }
        //Ckeditor
        CKEDITOR.replace( "ar[description]" ,{
            filebrowserUploadUrl: "{{route('ckeditor.uploadProduct', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            
        });
        CKEDITOR.replace( "en[description]" ,{
            filebrowserUploadUrl: "{{route('ckeditor.uploadProduct', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
            
        });
        $(function(){
            $("#country_id").on('change',function(){
                var country_id = $(this).val();
                var lang = '{{app()->getLocale()}}';
                var url ='{{url('get_cities')}}'+'/'+country_id+'/'+lang;
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(data){
                        $("#city_id").html(data);
                        
                    }
                });
                
            });

        });
        // Categories
        $(document).ready(function(){
            $('#jstree').jstree({
                "core" : {
                    'data' : {!! load_cats(old('category_id')) !!},
                    "themes" : {
                        "variant" : "large"
                    }
                },
                "checkbox" : {
                    "keep_selected_style" : false
                },
                "plugins" : [ "wholerow" ]
                });
        });
            $('#jstree').on('changed.jstree',function (e,data) {
                var i , j ,r = [];
                for(i=0,j = data.selected.length;i < j;i++){
                    r.push(data.instance.get_node(data.selected[i]).id);
                }
                $('.parent_id').val(r.join(', '));
            });
        //Location Map
        $('#us1').locationpicker({
        location: {
            latitude: {{ $lat }},
            longitude:{{ $lng }}
        },
        radius: 300,
            markerIcon: '{{ asset('img/map-marker-2-xl.png') }}',
            inputBinding: {
                latitudeInput: $('#lat'),
                longitudeInput: $('#lng'),
        }
        });
        //Dropzone
        
        var uploadedDocumentMap = {}
        Dropzone.options.documentDropzone = {
            url: '{{ route('products.storeMedia') }}',
            paramName:'file',
            uploadMultiple:false,
            maxFiles:15,
            maxFilessaze:2,
            addRemoveLinks: true,
            dictDefaultMessage:"{{trans('site.drag_drop')}}",
            dictRemoveFile:"{{ trans('site.delete') }} ",
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
                uploadedDocumentMap[file.name] = response.name
            },
            removedfile: function(file) 
            {
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedDocumentMap[file.name]
                }
                $.ajax({
                    headers:{
                        'X-CSRF-Token':$('input[name="_token"]').val()
                    }, //passes the current token of the page to image url
                    type: 'GET',
                    url: '{{ url("productImg/deleteImag/") }}'+'/'+name,
                    dataType: 'json',
                    success: function (data){
                        console.log("File deleted successfully!! "+data);
                    },
                    error: function(e) {
                        console.log('this is '+e.data);
                    }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ? 
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            init: function () {
                @if(isset($project) && $project->document)
                    var files =
                    {!! json_encode($project->document) !!}
                    for (var i in files) {
                    var file = files[i]
                    this.options.addedfile.call(this, file)
                    file.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
                    }
                @endif
            }
        }

    </script>
@endpush    