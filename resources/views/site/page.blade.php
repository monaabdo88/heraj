@extends('site.layouts.app')
@section('content')
    <!-- Breadcrumb Section Begin -->
    <section class="blog-details-hero set-bg" data-setbg="{{asset('site_files/img/banner/banner.jpeg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog__details__hero__text">
                        <h2>{{$page->title}}</h2>
                        <div class="breadcrumb__option">
                            <a href="{{url('/')}}">@lang('site.home')</a>
                            <span> {{$page->title}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->
        <!-- Blog Details Hero End -->

    <!-- Blog Details Section Begin -->
    <section class="blog-details spad">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="blog__details__text">
                       {!!$page->content!!}
                    </div>
                    
                </div>
                @include('site.inc.sideAd')
            </div>
        </div>
    </section>
    <!-- Blog Details Section End -->

@endsection